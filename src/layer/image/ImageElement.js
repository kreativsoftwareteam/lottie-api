var LayerBase = require('../LayerBase');

function Image(element) {
	var state = {
		element: element,
		properties: []
	}

	var methods = {
	}

	return Object.assign({}, LayerBase(state), methods);
}

module.exports = Image;