(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.lottie_api = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var Renderer = require('../renderer/Renderer');
var layer_api = require('../helpers/layerAPIBuilder');

function AnimationItemFactory(animation) {

	var state = {
		animation: animation,
		elements: animation.renderer.elements.map(function (item) {
			return layer_api(item, animation);
		}),
		boundingRect: null,
		scaleData: null
	};

	function getCurrentFrame() {
		return animation.currentFrame;
	}

	function getCurrentTime() {
		return animation.currentFrame / animation.frameRate;
	}

	function addValueCallback(properties, value) {
		var i,
		    len = properties.length;
		for (i = 0; i < len; i += 1) {
			properties.getPropertyAtIndex(i).setValue(value);
		}
	}

	function toKeypathLayerPoint(properties, point) {
		var i,
		    len = properties.length;
		var points = [];
		for (i = 0; i < len; i += 1) {
			points.push(properties.getPropertyAtIndex(i).toKeypathLayerPoint(point));
		}
		if (points.length === 1) {
			return points[0];
		}
		return points;
	}

	function fromKeypathLayerPoint(properties, point) {
		var i,
		    len = properties.length;
		var points = [];
		for (i = 0; i < len; i += 1) {
			points.push(properties.getPropertyAtIndex(i).fromKeypathLayerPoint(point));
		}
		if (points.length === 1) {
			return points[0];
		}
		return points;
	}

	function calculateScaleData(boundingRect) {
		var compWidth = animation.animationData.w;
		var compHeight = animation.animationData.h;
		var compRel = compWidth / compHeight;
		var elementWidth = boundingRect.width;
		var elementHeight = boundingRect.height;
		var elementRel = elementWidth / elementHeight;
		var scale, scaleXOffset, scaleYOffset;
		var xAlignment, yAlignment, scaleMode;
		var aspectRatio = animation.renderer.renderConfig.preserveAspectRatio.split(' ');
		if (aspectRatio[1] === 'meet') {
			scale = elementRel > compRel ? elementHeight / compHeight : elementWidth / compWidth;
		} else {
			scale = elementRel > compRel ? elementWidth / compWidth : elementHeight / compHeight;
		}
		xAlignment = aspectRatio[0].substr(0, 4);
		yAlignment = aspectRatio[0].substr(4);
		if (xAlignment === 'xMin') {
			scaleXOffset = 0;
		} else if (xAlignment === 'xMid') {
			scaleXOffset = (elementWidth - compWidth * scale) / 2;
		} else {
			scaleXOffset = elementWidth - compWidth * scale;
		}

		if (yAlignment === 'YMin') {
			scaleYOffset = 0;
		} else if (yAlignment === 'YMid') {
			scaleYOffset = (elementHeight - compHeight * scale) / 2;
		} else {
			scaleYOffset = elementHeight - compHeight * scale;
		}
		return {
			scaleYOffset: scaleYOffset,
			scaleXOffset: scaleXOffset,
			scale: scale
		};
	}

	function recalculateSize(container) {
		var container = animation.wrapper;
		state.boundingRect = container.getBoundingClientRect();
		state.scaleData = calculateScaleData(state.boundingRect);
	}

	function toContainerPoint(point) {
		if (!animation.wrapper || !animation.wrapper.getBoundingClientRect) {
			return point;
		}
		if (!state.boundingRect) {
			recalculateSize();
		}

		var boundingRect = state.boundingRect;
		var newPoint = [point[0] - boundingRect.left, point[1] - boundingRect.top];
		var scaleData = state.scaleData;

		newPoint[0] = (newPoint[0] - scaleData.scaleXOffset) / scaleData.scale;
		newPoint[1] = (newPoint[1] - scaleData.scaleYOffset) / scaleData.scale;

		return newPoint;
	}

	function fromContainerPoint(point) {
		if (!animation.wrapper || !animation.wrapper.getBoundingClientRect) {
			return point;
		}
		if (!state.boundingRect) {
			recalculateSize();
		}
		var boundingRect = state.boundingRect;
		var scaleData = state.scaleData;

		var newPoint = [point[0] * scaleData.scale + scaleData.scaleXOffset, point[1] * scaleData.scale + scaleData.scaleYOffset];

		var newPoint = [newPoint[0] + boundingRect.left, newPoint[1] + boundingRect.top];
		return newPoint;
	}

	function getScaleData() {
		return state.scaleData;
	}

	var methods = {
		recalculateSize: recalculateSize,
		getScaleData: getScaleData,
		toContainerPoint: toContainerPoint,
		fromContainerPoint: fromContainerPoint,
		getCurrentFrame: getCurrentFrame,
		getCurrentTime: getCurrentTime,
		addValueCallback: addValueCallback,
		toKeypathLayerPoint: toKeypathLayerPoint,
		fromKeypathLayerPoint: fromKeypathLayerPoint
	};

	return Object.assign({}, Renderer(state), methods);
}

module.exports = AnimationItemFactory;

},{"../helpers/layerAPIBuilder":6,"../renderer/Renderer":42}],2:[function(require,module,exports){
'use strict';

module.exports = ',';

},{}],3:[function(require,module,exports){
'use strict';

module.exports = {
	0: 0,
	1: 1,
	2: 2,
	3: 3,
	4: 4,
	5: 5,
	13: 13,
	'comp': 0,
	'composition': 0,
	'solid': 1,
	'image': 2,
	'null': 3,
	'shape': 4,
	'text': 5,
	'camera': 13
};

},{}],4:[function(require,module,exports){
'use strict';

module.exports = {
	LAYER_TRANSFORM: 'transform'
};

},{}],5:[function(require,module,exports){
'use strict';

var key_path_separator = require('../enums/key_path_separator');
var sanitizeString = require('./stringSanitizer');

module.exports = function (propertyPath) {
	var keyPathSplit = propertyPath.split(key_path_separator);
	var selector = keyPathSplit.shift();
	return {
		selector: sanitizeString(selector),
		propertyPath: keyPathSplit.join(key_path_separator)
	};
};

},{"../enums/key_path_separator":2,"./stringSanitizer":7}],6:[function(require,module,exports){
'use strict';

var TextElement = require('../layer/text/TextElement');
var ShapeElement = require('../layer/shape/Shape');
var NullElement = require('../layer/null_element/NullElement');
var SolidElement = require('../layer/solid/SolidElement');
var ImageElement = require('../layer/image/ImageElement');
var CameraElement = require('../layer/camera/Camera');
var LayerBase = require('../layer/LayerBase');

module.exports = function getLayerApi(element, parent) {
	var layerType = element.data.ty;
	var Composition = require('../layer/composition/Composition');
	switch (layerType) {
		case 0:
			return Composition(element, parent);
		case 1:
			return SolidElement(element, parent);
		case 2:
			return ImageElement(element, parent);
		case 3:
			return NullElement(element, parent);
		case 4:
			return ShapeElement(element, parent, element.data.shapes, element.itemsData);
		case 5:
			return TextElement(element, parent);
		case 13:
			return CameraElement(element, parent);
		default:
			return LayerBase(element, parent);
	}
};

},{"../layer/LayerBase":13,"../layer/camera/Camera":15,"../layer/composition/Composition":16,"../layer/image/ImageElement":20,"../layer/null_element/NullElement":21,"../layer/shape/Shape":22,"../layer/solid/SolidElement":35,"../layer/text/TextElement":38}],7:[function(require,module,exports){
"use strict";

function sanitizeString(string) {
	return string.trim();
}

module.exports = sanitizeString;

},{}],8:[function(require,module,exports){
'use strict';

var createTypedArray = require('./typedArrays');

/*!
 Transformation Matrix v2.0
 (c) Epistemex 2014-2015
 www.epistemex.com
 By Ken Fyrstenberg
 Contributions by leeoniya.
 License: MIT, header required.
 */

/**
 * 2D transformation matrix object initialized with identity matrix.
 *
 * The matrix can synchronize a canvas context by supplying the context
 * as an argument, or later apply current absolute transform to an
 * existing context.
 *
 * All values are handled as floating point values.
 *
 * @param {CanvasRenderingContext2D} [context] - Optional context to sync with Matrix
 * @prop {number} a - scale x
 * @prop {number} b - shear y
 * @prop {number} c - shear x
 * @prop {number} d - scale y
 * @prop {number} e - translate x
 * @prop {number} f - translate y
 * @prop {CanvasRenderingContext2D|null} [context=null] - set or get current canvas context
 * @constructor
 */

var Matrix = function () {

    var _cos = Math.cos;
    var _sin = Math.sin;
    var _tan = Math.tan;
    var _rnd = Math.round;

    function reset() {
        this.props[0] = 1;
        this.props[1] = 0;
        this.props[2] = 0;
        this.props[3] = 0;
        this.props[4] = 0;
        this.props[5] = 1;
        this.props[6] = 0;
        this.props[7] = 0;
        this.props[8] = 0;
        this.props[9] = 0;
        this.props[10] = 1;
        this.props[11] = 0;
        this.props[12] = 0;
        this.props[13] = 0;
        this.props[14] = 0;
        this.props[15] = 1;
        return this;
    }

    function rotate(angle) {
        if (angle === 0) {
            return this;
        }
        var mCos = _cos(angle);
        var mSin = _sin(angle);
        return this._t(mCos, -mSin, 0, 0, mSin, mCos, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
    }

    function rotateX(angle) {
        if (angle === 0) {
            return this;
        }
        var mCos = _cos(angle);
        var mSin = _sin(angle);
        return this._t(1, 0, 0, 0, 0, mCos, -mSin, 0, 0, mSin, mCos, 0, 0, 0, 0, 1);
    }

    function rotateY(angle) {
        if (angle === 0) {
            return this;
        }
        var mCos = _cos(angle);
        var mSin = _sin(angle);
        return this._t(mCos, 0, mSin, 0, 0, 1, 0, 0, -mSin, 0, mCos, 0, 0, 0, 0, 1);
    }

    function rotateZ(angle) {
        if (angle === 0) {
            return this;
        }
        var mCos = _cos(angle);
        var mSin = _sin(angle);
        return this._t(mCos, -mSin, 0, 0, mSin, mCos, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
    }

    function shear(sx, sy) {
        return this._t(1, sy, sx, 1, 0, 0);
    }

    function skew(ax, ay) {
        return this.shear(_tan(ax), _tan(ay));
    }

    function skewFromAxis(ax, angle) {
        var mCos = _cos(angle);
        var mSin = _sin(angle);
        return this._t(mCos, mSin, 0, 0, -mSin, mCos, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)._t(1, 0, 0, 0, _tan(ax), 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)._t(mCos, -mSin, 0, 0, mSin, mCos, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
        //return this._t(mCos, mSin, -mSin, mCos, 0, 0)._t(1, 0, _tan(ax), 1, 0, 0)._t(mCos, -mSin, mSin, mCos, 0, 0);
    }

    function scale(sx, sy, sz) {
        sz = isNaN(sz) ? 1 : sz;
        if (sx == 1 && sy == 1 && sz == 1) {
            return this;
        }
        return this._t(sx, 0, 0, 0, 0, sy, 0, 0, 0, 0, sz, 0, 0, 0, 0, 1);
    }

    function setTransform(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p) {
        this.props[0] = a;
        this.props[1] = b;
        this.props[2] = c;
        this.props[3] = d;
        this.props[4] = e;
        this.props[5] = f;
        this.props[6] = g;
        this.props[7] = h;
        this.props[8] = i;
        this.props[9] = j;
        this.props[10] = k;
        this.props[11] = l;
        this.props[12] = m;
        this.props[13] = n;
        this.props[14] = o;
        this.props[15] = p;
        return this;
    }

    function translate(tx, ty, tz) {
        tz = tz || 0;
        if (tx !== 0 || ty !== 0 || tz !== 0) {
            return this._t(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, tx, ty, tz, 1);
        }
        return this;
    }

    function transform(a2, b2, c2, d2, e2, f2, g2, h2, i2, j2, k2, l2, m2, n2, o2, p2) {

        var _p = this.props;

        if (a2 === 1 && b2 === 0 && c2 === 0 && d2 === 0 && e2 === 0 && f2 === 1 && g2 === 0 && h2 === 0 && i2 === 0 && j2 === 0 && k2 === 1 && l2 === 0) {
            //NOTE: commenting this condition because TurboFan deoptimizes code when present
            //if(m2 !== 0 || n2 !== 0 || o2 !== 0){
            _p[12] = _p[12] * a2 + _p[15] * m2;
            _p[13] = _p[13] * f2 + _p[15] * n2;
            _p[14] = _p[14] * k2 + _p[15] * o2;
            _p[15] = _p[15] * p2;
            //}
            this._identityCalculated = false;
            return this;
        }

        var a1 = _p[0];
        var b1 = _p[1];
        var c1 = _p[2];
        var d1 = _p[3];
        var e1 = _p[4];
        var f1 = _p[5];
        var g1 = _p[6];
        var h1 = _p[7];
        var i1 = _p[8];
        var j1 = _p[9];
        var k1 = _p[10];
        var l1 = _p[11];
        var m1 = _p[12];
        var n1 = _p[13];
        var o1 = _p[14];
        var p1 = _p[15];

        /* matrix order (canvas compatible):
         * ace
         * bdf
         * 001
         */
        _p[0] = a1 * a2 + b1 * e2 + c1 * i2 + d1 * m2;
        _p[1] = a1 * b2 + b1 * f2 + c1 * j2 + d1 * n2;
        _p[2] = a1 * c2 + b1 * g2 + c1 * k2 + d1 * o2;
        _p[3] = a1 * d2 + b1 * h2 + c1 * l2 + d1 * p2;

        _p[4] = e1 * a2 + f1 * e2 + g1 * i2 + h1 * m2;
        _p[5] = e1 * b2 + f1 * f2 + g1 * j2 + h1 * n2;
        _p[6] = e1 * c2 + f1 * g2 + g1 * k2 + h1 * o2;
        _p[7] = e1 * d2 + f1 * h2 + g1 * l2 + h1 * p2;

        _p[8] = i1 * a2 + j1 * e2 + k1 * i2 + l1 * m2;
        _p[9] = i1 * b2 + j1 * f2 + k1 * j2 + l1 * n2;
        _p[10] = i1 * c2 + j1 * g2 + k1 * k2 + l1 * o2;
        _p[11] = i1 * d2 + j1 * h2 + k1 * l2 + l1 * p2;

        _p[12] = m1 * a2 + n1 * e2 + o1 * i2 + p1 * m2;
        _p[13] = m1 * b2 + n1 * f2 + o1 * j2 + p1 * n2;
        _p[14] = m1 * c2 + n1 * g2 + o1 * k2 + p1 * o2;
        _p[15] = m1 * d2 + n1 * h2 + o1 * l2 + p1 * p2;

        this._identityCalculated = false;
        return this;
    }

    function isIdentity() {
        if (!this._identityCalculated) {
            this._identity = !(this.props[0] !== 1 || this.props[1] !== 0 || this.props[2] !== 0 || this.props[3] !== 0 || this.props[4] !== 0 || this.props[5] !== 1 || this.props[6] !== 0 || this.props[7] !== 0 || this.props[8] !== 0 || this.props[9] !== 0 || this.props[10] !== 1 || this.props[11] !== 0 || this.props[12] !== 0 || this.props[13] !== 0 || this.props[14] !== 0 || this.props[15] !== 1);
            this._identityCalculated = true;
        }
        return this._identity;
    }

    function equals(matr) {
        var i = 0;
        while (i < 16) {
            if (matr.props[i] !== this.props[i]) {
                return false;
            }
            i += 1;
        }
        return true;
    }

    function clone(matr) {
        var i;
        for (i = 0; i < 16; i += 1) {
            matr.props[i] = this.props[i];
        }
    }

    function cloneFromProps(props) {
        var i;
        for (i = 0; i < 16; i += 1) {
            this.props[i] = props[i];
        }
    }

    function applyToPoint(x, y, z) {

        return {
            x: x * this.props[0] + y * this.props[4] + z * this.props[8] + this.props[12],
            y: x * this.props[1] + y * this.props[5] + z * this.props[9] + this.props[13],
            z: x * this.props[2] + y * this.props[6] + z * this.props[10] + this.props[14]
        };
        /*return {
         x: x * me.a + y * me.c + me.e,
         y: x * me.b + y * me.d + me.f
         };*/
    }
    function applyToX(x, y, z) {
        return x * this.props[0] + y * this.props[4] + z * this.props[8] + this.props[12];
    }
    function applyToY(x, y, z) {
        return x * this.props[1] + y * this.props[5] + z * this.props[9] + this.props[13];
    }
    function applyToZ(x, y, z) {
        return x * this.props[2] + y * this.props[6] + z * this.props[10] + this.props[14];
    }

    function inversePoint(pt) {
        var determinant = this.props[0] * this.props[5] - this.props[1] * this.props[4];
        var a = this.props[5] / determinant;
        var b = -this.props[1] / determinant;
        var c = -this.props[4] / determinant;
        var d = this.props[0] / determinant;
        var e = (this.props[4] * this.props[13] - this.props[5] * this.props[12]) / determinant;
        var f = -(this.props[0] * this.props[13] - this.props[1] * this.props[12]) / determinant;
        return [pt[0] * a + pt[1] * c + e, pt[0] * b + pt[1] * d + f, 0];
    }

    function inversePoints(pts) {
        var i,
            len = pts.length,
            retPts = [];
        for (i = 0; i < len; i += 1) {
            retPts[i] = inversePoint(pts[i]);
        }
        return retPts;
    }

    function applyToTriplePoints(pt1, pt2, pt3) {
        var arr = createTypedArray('float32', 6);
        if (this.isIdentity()) {
            arr[0] = pt1[0];
            arr[1] = pt1[1];
            arr[2] = pt2[0];
            arr[3] = pt2[1];
            arr[4] = pt3[0];
            arr[5] = pt3[1];
        } else {
            var p0 = this.props[0],
                p1 = this.props[1],
                p4 = this.props[4],
                p5 = this.props[5],
                p12 = this.props[12],
                p13 = this.props[13];
            arr[0] = pt1[0] * p0 + pt1[1] * p4 + p12;
            arr[1] = pt1[0] * p1 + pt1[1] * p5 + p13;
            arr[2] = pt2[0] * p0 + pt2[1] * p4 + p12;
            arr[3] = pt2[0] * p1 + pt2[1] * p5 + p13;
            arr[4] = pt3[0] * p0 + pt3[1] * p4 + p12;
            arr[5] = pt3[0] * p1 + pt3[1] * p5 + p13;
        }
        return arr;
    }

    function applyToPointArray(x, y, z) {
        var arr;
        if (this.isIdentity()) {
            arr = [x, y, z];
        } else {
            arr = [x * this.props[0] + y * this.props[4] + z * this.props[8] + this.props[12], x * this.props[1] + y * this.props[5] + z * this.props[9] + this.props[13], x * this.props[2] + y * this.props[6] + z * this.props[10] + this.props[14]];
        }
        return arr;
    }

    function applyToPointStringified(x, y) {
        if (this.isIdentity()) {
            return x + ',' + y;
        }
        return x * this.props[0] + y * this.props[4] + this.props[12] + ',' + (x * this.props[1] + y * this.props[5] + this.props[13]);
    }

    function toCSS() {
        //Doesn't make much sense to add this optimization. If it is an identity matrix, it's very likely this will get called only once since it won't be keyframed.
        /*if(this.isIdentity()) {
            return '';
        }*/
        var i = 0;
        var props = this.props;
        var cssValue = 'matrix3d(';
        var v = 10000;
        while (i < 16) {
            cssValue += _rnd(props[i] * v) / v;
            cssValue += i === 15 ? ')' : ',';
            i += 1;
        }
        return cssValue;
    }

    function to2dCSS() {
        //Doesn't make much sense to add this optimization. If it is an identity matrix, it's very likely this will get called only once since it won't be keyframed.
        /*if(this.isIdentity()) {
            return '';
        }*/
        var v = 10000;
        var props = this.props;
        return "matrix(" + _rnd(props[0] * v) / v + ',' + _rnd(props[1] * v) / v + ',' + _rnd(props[4] * v) / v + ',' + _rnd(props[5] * v) / v + ',' + _rnd(props[12] * v) / v + ',' + _rnd(props[13] * v) / v + ")";
    }

    function MatrixInstance() {
        this.reset = reset;
        this.rotate = rotate;
        this.rotateX = rotateX;
        this.rotateY = rotateY;
        this.rotateZ = rotateZ;
        this.skew = skew;
        this.skewFromAxis = skewFromAxis;
        this.shear = shear;
        this.scale = scale;
        this.setTransform = setTransform;
        this.translate = translate;
        this.transform = transform;
        this.applyToPoint = applyToPoint;
        this.applyToX = applyToX;
        this.applyToY = applyToY;
        this.applyToZ = applyToZ;
        this.applyToPointArray = applyToPointArray;
        this.applyToTriplePoints = applyToTriplePoints;
        this.applyToPointStringified = applyToPointStringified;
        this.toCSS = toCSS;
        this.to2dCSS = to2dCSS;
        this.clone = clone;
        this.cloneFromProps = cloneFromProps;
        this.equals = equals;
        this.inversePoints = inversePoints;
        this.inversePoint = inversePoint;
        this._t = this.transform;
        this.isIdentity = isIdentity;
        this._identity = true;
        this._identityCalculated = false;

        this.props = createTypedArray('float32', 16);
        this.reset();
    };

    return function () {
        return new MatrixInstance();
    };
}();

module.exports = Matrix;

},{"./typedArrays":9}],9:[function(require,module,exports){
'use strict';

var createTypedArray = function () {
	function createRegularArray(type, len) {
		var i = 0,
		    arr = [],
		    value;
		switch (type) {
			case 'int16':
			case 'uint8c':
				value = 1;
				break;
			default:
				value = 1.1;
				break;
		}
		for (i = 0; i < len; i += 1) {
			arr.push(value);
		}
		return arr;
	}
	function createTypedArray(type, len) {
		if (type === 'float32') {
			return new Float32Array(len);
		} else if (type === 'int16') {
			return new Int16Array(len);
		} else if (type === 'uint8c') {
			return new Uint8ClampedArray(len);
		}
	}
	if (typeof Uint8ClampedArray === 'function' && typeof Float32Array === 'function') {
		return createTypedArray;
	} else {
		return createRegularArray;
	}
}();

module.exports = createTypedArray;

},{}],10:[function(require,module,exports){
'use strict';

var AnimationItem = require('./animation/AnimationItem');

function createAnimationApi(anim) {
	return Object.assign({}, AnimationItem(anim));
}

module.exports = {
	createAnimationApi: createAnimationApi
};

},{"./animation/AnimationItem":1}],11:[function(require,module,exports){
'use strict';

var keyPathBuilder = require('../helpers/keyPathBuilder');
var layer_types = require('../enums/layer_types');

function KeyPathList(elements, node_type) {

	function _getLength() {
		return elements.length;
	}

	function _filterLayerByType(elements, type) {
		return elements.filter(function (element) {
			return element.getTargetLayer().data.ty === layer_types[type];
		});
	}

	function _filterLayerByName(elements, name) {
		return elements.filter(function (element) {
			return element.getTargetLayer().data.nm === name;
		});
	}

	function _filterLayerByProperty(elements, name) {
		return elements.filter(function (element) {
			if (element.hasProperty(name)) {
				return element.getProperty(name);
			}
			return false;
		});
	}

	function getLayersByType(selector) {
		return KeyPathList(_filterLayerByType(elements, selector), 'layer');
	}

	function getLayersByName(selector) {
		return KeyPathList(_filterLayerByName(elements, selector), 'layer');
	}

	function getPropertiesBySelector(selector) {
		return KeyPathList(elements.filter(function (element) {
			return element.hasProperty(selector);
		}).map(function (element) {
			return element.getProperty(selector);
		}), 'property');
	}

	function getLayerProperty(selector) {
		var layers = _filterLayerByProperty(elements, selector);
		var properties = layers.map(function (element) {
			return element.getProperty(selector);
		});
		return KeyPathList(properties, 'property');
	}

	function getKeyPath(propertyPath) {
		var keyPathData = keyPathBuilder(propertyPath);
		var selector = keyPathData.selector;
		var nodesByName, nodesByType, selectedNodes;
		if (node_type === 'renderer' || node_type === 'layer') {
			nodesByName = getLayersByName(selector);
			nodesByType = getLayersByType(selector);
			if (nodesByName.length === 0 && nodesByType.length === 0) {
				selectedNodes = getLayerProperty(selector);
			} else {
				selectedNodes = nodesByName.concat(nodesByType);
			}
			if (keyPathData.propertyPath) {
				return selectedNodes.getKeyPath(keyPathData.propertyPath);
			} else {
				return selectedNodes;
			}
		} else if (node_type === 'property') {
			selectedNodes = getPropertiesBySelector(selector);
			if (keyPathData.propertyPath) {
				return selectedNodes.getKeyPath(keyPathData.propertyPath);
			} else {
				return selectedNodes;
			}
		}
	}

	function concat(nodes) {
		var nodesElements = nodes.getElements();
		return KeyPathList(elements.concat(nodesElements), node_type);
	}

	function getElements() {
		return elements;
	}

	function getPropertyAtIndex(index) {
		return elements[index];
	}

	var methods = {
		getKeyPath: getKeyPath,
		concat: concat,
		getElements: getElements,
		getPropertyAtIndex: getPropertyAtIndex
	};

	Object.defineProperty(methods, 'length', {
		get: _getLength
	});

	return methods;
}

module.exports = KeyPathList;

},{"../enums/layer_types":3,"../helpers/keyPathBuilder":5}],12:[function(require,module,exports){
'use strict';

var key_path_separator = require('../enums/key_path_separator');
var property_names = require('../enums/property_names');

function KeyPathNode(state) {

	function getPropertyByPath(selector, propertyPath) {
		var instanceProperties = state.properties || [];
		var i = 0,
		    len = instanceProperties.length;
		while (i < len) {
			if (instanceProperties[i].name === selector) {
				return instanceProperties[i].value;
			}
			i += 1;
		}
		return null;
	}

	function hasProperty(selector) {
		return !!getPropertyByPath(selector);
	}

	function getProperty(selector) {
		return getPropertyByPath(selector);
	}

	function fromKeypathLayerPoint(point) {
		return state.parent.fromKeypathLayerPoint(point);
	}

	function toKeypathLayerPoint(point) {
		return state.parent.toKeypathLayerPoint(point);
	}

	var methods = {
		hasProperty: hasProperty,
		getProperty: getProperty,
		fromKeypathLayerPoint: fromKeypathLayerPoint,
		toKeypathLayerPoint: toKeypathLayerPoint
	};
	return methods;
}

module.exports = KeyPathNode;

},{"../enums/key_path_separator":2,"../enums/property_names":4}],13:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../key_path/KeyPathNode');
var Transform = require('./transform/Transform');
var Effects = require('./effects/Effects');
var Matrix = require('../helpers/transformationMatrix');

function LayerBase(state) {

	var transform = Transform(state.element.finalTransform.mProp, state);
	var effects = Effects(state.element.effectsManager.effectElements || [], state);

	function _buildPropertyMap() {
		state.properties.push({
			name: 'transform',
			value: transform
		}, {
			name: 'Transform',
			value: transform
		}, {
			name: 'Effects',
			value: effects
		}, {
			name: 'effects',
			value: effects
		});
	}

	function getElementToPoint(point) {}

	function toKeypathLayerPoint(point) {
		var element = state.element;
		if (state.parent.toKeypathLayerPoint) {
			point = state.parent.toKeypathLayerPoint(point);
		}
		var toWorldMat = Matrix();
		var transformMat = state.getProperty('Transform').getTargetTransform();
		transformMat.applyToMatrix(toWorldMat);
		if (element.hierarchy && element.hierarchy.length) {
			var i,
			    len = element.hierarchy.length;
			for (i = 0; i < len; i += 1) {
				element.hierarchy[i].finalTransform.mProp.applyToMatrix(toWorldMat);
			}
		}
		return toWorldMat.inversePoint(point);
	}

	function fromKeypathLayerPoint(point) {
		var element = state.element;
		var toWorldMat = Matrix();
		var transformMat = state.getProperty('Transform').getTargetTransform();
		transformMat.applyToMatrix(toWorldMat);
		if (element.hierarchy && element.hierarchy.length) {
			var i,
			    len = element.hierarchy.length;
			for (i = 0; i < len; i += 1) {
				element.hierarchy[i].finalTransform.mProp.applyToMatrix(toWorldMat);
			}
		}
		point = toWorldMat.applyToPointArray(point[0], point[1], point[2] || 0);
		if (state.parent.fromKeypathLayerPoint) {
			return state.parent.fromKeypathLayerPoint(point);
		} else {
			return point;
		}
	}

	function getTargetLayer() {
		return state.element;
	}

	var methods = {
		getTargetLayer: getTargetLayer,
		toKeypathLayerPoint: toKeypathLayerPoint,
		fromKeypathLayerPoint: fromKeypathLayerPoint
	};

	_buildPropertyMap();

	return Object.assign(state, KeyPathNode(state), methods);
}

module.exports = LayerBase;

},{"../helpers/transformationMatrix":8,"../key_path/KeyPathNode":12,"./effects/Effects":19,"./transform/Transform":39}],14:[function(require,module,exports){
'use strict';

var layer_types = require('../enums/layer_types');
var layer_api = require('../helpers/layerAPIBuilder');

function LayerList(elements) {

	function _getLength() {
		return elements.length;
	}

	function _filterLayerByType(elements, type) {
		return elements.filter(function (element) {
			return element.data.ty === layer_types[type];
		});
	}

	function _filterLayerByName(elements, name) {
		return elements.filter(function (element) {
			return element.data.nm === name;
		});
	}

	function getLayers() {
		return LayerList(elements);
	}

	function getLayersByType(type) {
		var elementsList = _filterLayerByType(elements, type);
		return LayerList(elementsList);
	}

	function getLayersByName(type) {
		var elementsList = _filterLayerByName(elements, type);
		return LayerList(elementsList);
	}

	function layer(index) {
		if (index >= elements.length) {
			return [];
		}
		return layer_api(elements[parseInt(index)]);
	}

	function addIteratableMethods(iteratableMethods, list) {
		iteratableMethods.reduce(function (accumulator, value) {
			var _value = value;
			accumulator[value] = function () {
				var _arguments = arguments;
				return elements.map(function (element) {
					var layer = layer_api(element);
					if (layer[_value]) {
						return layer[_value].apply(null, _arguments);
					}
					return null;
				});
			};
			return accumulator;
		}, methods);
	}

	function getTargetElements() {
		return elements;
	}

	function concat(list) {
		return elements.concat(list.getTargetElements());
	}

	var methods = {
		getLayers: getLayers,
		getLayersByType: getLayersByType,
		getLayersByName: getLayersByName,
		layer: layer,
		concat: concat,
		getTargetElements: getTargetElements
	};

	addIteratableMethods(['setTranslate', 'getType', 'getDuration']);
	addIteratableMethods(['setText', 'getText', 'setDocumentData', 'canResizeFont', 'setMinimumFontSize']);

	Object.defineProperty(methods, 'length', {
		get: _getLength
	});
	return methods;
}

module.exports = LayerList;

},{"../enums/layer_types":3,"../helpers/layerAPIBuilder":6}],15:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../../key_path/KeyPathNode');
var Property = require('../../property/Property');

function Camera(element, parent) {

	var instance = {};

	var state = {
		element: element,
		parent: parent,
		properties: _buildPropertyMap()
	};

	function _buildPropertyMap() {
		return [{
			name: 'Point of Interest',
			value: Property(element.a, parent)
		}, {
			name: 'Zoom',
			value: Property(element.pe, parent)
		}, {
			name: 'Position',
			value: Property(element.p, parent)
		}, {
			name: 'X Rotation',
			value: Property(element.rx, parent)
		}, {
			name: 'Y Rotation',
			value: Property(element.ry, parent)
		}, {
			name: 'Z Rotation',
			value: Property(element.rz, parent)
		}, {
			name: 'Orientation',
			value: Property(element.or, parent)
		}];
	}

	function getTargetLayer() {
		return state.element;
	}

	var methods = {
		getTargetLayer: getTargetLayer
	};

	return Object.assign(instance, KeyPathNode(state), methods);
}

module.exports = Camera;

},{"../../key_path/KeyPathNode":12,"../../property/Property":40}],16:[function(require,module,exports){
'use strict';

var KeyPathList = require('../../key_path/KeyPathList');
var LayerBase = require('../LayerBase');
var layer_api = require('../../helpers/layerAPIBuilder');
var Property = require('../../property/Property');
var TimeRemap = require('./TimeRemap');

function Composition(element, parent) {

	var instance = {};

	var state = {
		element: element,
		parent: parent,
		properties: _buildPropertyMap()
	};

	function buildLayerApi(layer, index) {
		var _layerApi = null;
		var ob = {
			name: layer.nm
		};

		function getLayerApi() {
			if (!_layerApi) {
				_layerApi = layer_api(element.elements[index], state);
			}
			return _layerApi;
		}

		Object.defineProperty(ob, 'value', {
			get: getLayerApi
		});
		return ob;
	}

	function _buildPropertyMap() {
		var compositionLayers = element.layers.map(buildLayerApi);
		return [{
			name: 'Time Remap',
			value: TimeRemap(element.tm)
		}].concat(compositionLayers);
	}

	var methods = {};

	return Object.assign(instance, LayerBase(state), KeyPathList(state.elements, 'layer'), methods);
}

module.exports = Composition;

},{"../../helpers/layerAPIBuilder":6,"../../key_path/KeyPathList":11,"../../property/Property":40,"../LayerBase":13,"./TimeRemap":17}],17:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../../key_path/KeyPathNode');
var ValueProperty = require('../../property/ValueProperty');

function TimeRemap(property, parent) {
	var state = {
		property: property,
		parent: parent
	};

	var _isCallbackAdded = false;
	var currentSegmentInit = 0;
	var currentSegmentEnd = 0;
	var previousTime = 0,
	    currentTime = 0;
	var initTime = 0;
	var _loop = true;
	var _loopCount = 0;
	var _speed = 1;
	var _paused = false;
	var _isDebugging = false;
	var queuedSegments = [];
	var _destroyFunction;
	var enterFrameCallback = null;
	var enterFrameEvent = {
		time: -1
	};

	function playSegment(init, end, clear) {
		_paused = false;
		if (clear) {
			clearQueue();
			currentTime = init;
		}
		if (_isDebugging) {
			console.log(init, end);
		}
		_loopCount = 0;
		previousTime = Date.now();
		currentSegmentInit = init;
		currentSegmentEnd = end;
		addCallback();
	}

	function playQueuedSegment() {
		var newSegment = queuedSegments.shift();
		playSegment(newSegment[0], newSegment[1]);
	}

	function queueSegment(init, end) {
		queuedSegments.push([init, end]);
	}

	function clearQueue() {
		queuedSegments.length = 0;
	}

	function _segmentPlayer(currentValue) {
		if (currentSegmentInit === currentSegmentEnd) {
			currentTime = currentSegmentInit;
		} else if (!_paused) {
			var nowTime = Date.now();
			var elapsedTime = _speed * (nowTime - previousTime) / 1000;
			previousTime = nowTime;
			if (currentSegmentInit < currentSegmentEnd) {
				currentTime += elapsedTime;
				if (currentTime > currentSegmentEnd) {
					_loopCount += 1;
					if (queuedSegments.length) {
						playQueuedSegment();
					} else if (!_loop) {
						currentTime = currentSegmentEnd;
					} else {
						/*currentTime -= Math.floor(currentTime / (currentSegmentEnd - currentSegmentInit)) * (currentSegmentEnd - currentSegmentInit);
      currentTime = currentSegmentInit + currentTime;*/
						currentTime = currentTime % (currentSegmentEnd - currentSegmentInit);
						//currentTime = currentSegmentInit + (currentTime);
						//currentTime = currentSegmentInit + (currentTime - currentSegmentEnd);
						//console.log('CT: ', currentTime) 
					}
				}
			} else {
				currentTime -= elapsedTime;
				if (currentTime < currentSegmentEnd) {
					_loopCount += 1;
					if (queuedSegments.length) {
						playQueuedSegment();
					} else if (!_loop) {
						currentTime = currentSegmentEnd;
					} else {
						currentTime = currentSegmentInit - (currentSegmentEnd - currentTime);
					}
				}
			}
			if (_isDebugging) {
				console.log(currentTime);
			}
		}
		if (instance.onEnterFrame && enterFrameEvent.time !== currentTime) {
			enterFrameEvent.time = currentTime;
			instance.onEnterFrame(enterFrameEvent);
		}
		return currentTime;
	}

	function addCallback() {
		if (!_isCallbackAdded) {
			_isCallbackAdded = true;
			_destroyFunction = instance.setValue(_segmentPlayer, _isDebugging);
		}
	}

	function playTo(end, clear) {
		_paused = false;
		if (clear) {
			clearQueue();
		}
		addCallback();
		currentSegmentEnd = end;
	}

	function getCurrentTime() {
		if (_isCallbackAdded) {
			return currentTime;
		} else {
			return property.v / property.mult;
		}
	}

	function setLoop(flag) {
		_loop = flag;
	}

	function setSpeed(value) {
		_speed = value;
	}

	function setDebugging(flag) {
		_isDebugging = flag;
	}

	function pause() {
		_paused = true;
	}

	function destroy() {
		if (_destroyFunction) {
			_destroyFunction();
			state.property = null;
			state.parent = null;
		}
	}

	var methods = {
		playSegment: playSegment,
		playTo: playTo,
		queueSegment: queueSegment,
		clearQueue: clearQueue,
		setLoop: setLoop,
		setSpeed: setSpeed,
		pause: pause,
		setDebugging: setDebugging,
		getCurrentTime: getCurrentTime,
		onEnterFrame: null,
		destroy: destroy
	};

	var instance = {};

	return Object.assign(instance, methods, ValueProperty(state), KeyPathNode(state));
}

module.exports = TimeRemap;

},{"../../key_path/KeyPathNode":12,"../../property/ValueProperty":41}],18:[function(require,module,exports){
'use strict';

var Property = require('../../property/Property');

function EffectElement(effect, parent) {

	return Property(effect.p, parent);
}

module.exports = EffectElement;

},{"../../property/Property":40}],19:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../../key_path/KeyPathNode');
var Property = require('../../property/Property');
var EffectElement = require('./EffectElement');

function Effects(effects, parent) {

	var state = {
		parent: parent,
		properties: buildProperties()
	};

	function getValue(effectData, index) {
		var nm = effectData.data ? effectData.data.nm : index.toString();
		var effectElement = effectData.data ? Effects(effectData.effectElements, parent) : Property(effectData.p, parent);
		return {
			name: nm,
			value: effectElement
		};
	}

	function buildProperties() {
		var i,
		    len = effects.length;
		var arr = [];
		for (i = 0; i < len; i += 1) {
			arr.push(getValue(effects[i], i));
		}
		return arr;
	}

	var methods = {};

	return Object.assign(methods, KeyPathNode(state));
}

module.exports = Effects;

},{"../../key_path/KeyPathNode":12,"../../property/Property":40,"./EffectElement":18}],20:[function(require,module,exports){
'use strict';

var LayerBase = require('../LayerBase');

function Image(element) {
	var state = {
		element: element,
		properties: []
	};

	var methods = {};

	return Object.assign({}, LayerBase(state), methods);
}

module.exports = Image;

},{"../LayerBase":13}],21:[function(require,module,exports){
'use strict';

var LayerBase = require('../LayerBase');

function NullElement(element, parent) {

	var instance = {};

	var state = {
		element: element,
		parent: parent,
		properties: _buildPropertyMap()
	};

	function _buildPropertyMap() {
		return [];
	}

	var methods = {};

	return Object.assign(instance, LayerBase(state), methods);
}

module.exports = NullElement;

},{"../LayerBase":13}],22:[function(require,module,exports){
'use strict';

var LayerBase = require('../LayerBase');
var ShapeContents = require('./ShapeContents');

function Shape(element, parent) {

	var state = {
		properties: [],
		parent: parent,
		element: element
	};
	var shapeContents = ShapeContents(element.data.shapes, element.itemsData, state);

	function _buildPropertyMap() {
		state.properties.push({
			name: 'Contents',
			value: shapeContents
		});
	}

	var methods = {};

	_buildPropertyMap();

	return Object.assign(state, LayerBase(state), methods);
}

module.exports = Shape;

},{"../LayerBase":13,"./ShapeContents":23}],23:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../../key_path/KeyPathNode');
var Property = require('../../property/Property');
var ShapeRectangle = require('./ShapeRectangle');
var ShapeFill = require('./ShapeFill');
var ShapeStroke = require('./ShapeStroke');
var ShapeEllipse = require('./ShapeEllipse');
var ShapeGradientFill = require('./ShapeGradientFill');
var ShapeGradientStroke = require('./ShapeGradientStroke');
var ShapeTrimPaths = require('./ShapeTrimPaths');
var ShapeRepeater = require('./ShapeRepeater');
var ShapePolystar = require('./ShapePolystar');
var ShapeRoundCorners = require('./ShapeRoundCorners');
var ShapePath = require('./ShapePath');
var Transform = require('../transform/Transform');
var Matrix = require('../../helpers/transformationMatrix');

function ShapeContents(shapesData, shapes, parent) {
	var state = {
		properties: _buildPropertyMap(),
		parent: parent
	};

	var cachedShapeProperties = [];

	function buildShapeObject(shape, index) {
		var ob = {
			name: shape.nm
		};
		Object.defineProperty(ob, 'value', {
			get: function get() {
				if (cachedShapeProperties[index]) {
					return cachedShapeProperties[index];
				} else {
					var property;
				}
				if (shape.ty === 'gr') {
					property = ShapeContents(shapesData[index].it, shapes[index].it, state);
				} else if (shape.ty === 'rc') {
					property = ShapeRectangle(shapes[index], state);
				} else if (shape.ty === 'el') {
					property = ShapeEllipse(shapes[index], state);
				} else if (shape.ty === 'fl') {
					property = ShapeFill(shapes[index], state);
				} else if (shape.ty === 'st') {
					property = ShapeStroke(shapes[index], state);
				} else if (shape.ty === 'gf') {
					property = ShapeGradientFill(shapes[index], state);
				} else if (shape.ty === 'gs') {
					property = ShapeGradientStroke(shapes[index], state);
				} else if (shape.ty === 'tm') {
					property = ShapeTrimPaths(shapes[index], state);
				} else if (shape.ty === 'rp') {
					property = ShapeRepeater(shapes[index], state);
				} else if (shape.ty === 'sr') {
					property = ShapePolystar(shapes[index], state);
				} else if (shape.ty === 'rd') {
					property = ShapeRoundCorners(shapes[index], state);
				} else if (shape.ty === 'sh') {
					property = ShapePath(shapes[index], state);
				} else if (shape.ty === 'tr') {
					property = Transform(shapes[index].transform.mProps, state);
				} else {
					console.log(shape.ty);
				}
				cachedShapeProperties[index] = property;
				return property;
			}
		});
		return ob;
	}

	function _buildPropertyMap() {
		return shapesData.map(function (shape, index) {
			return buildShapeObject(shape, index);
		});
	}

	function fromKeypathLayerPoint(point) {
		if (state.hasProperty('Transform')) {
			var toWorldMat = Matrix();
			var transformMat = state.getProperty('Transform').getTargetTransform();
			transformMat.applyToMatrix(toWorldMat);
			point = toWorldMat.applyToPointArray(point[0], point[1], point[2] || 0);
		}
		return state.parent.fromKeypathLayerPoint(point);
	}

	function toKeypathLayerPoint(point) {
		point = state.parent.toKeypathLayerPoint(point);
		if (state.hasProperty('Transform')) {
			var toWorldMat = Matrix();
			var transformMat = state.getProperty('Transform').getTargetTransform();
			transformMat.applyToMatrix(toWorldMat);
			point = toWorldMat.inversePoint(point);
		}
		return point;
	}

	var methods = {
		fromKeypathLayerPoint: fromKeypathLayerPoint,
		toKeypathLayerPoint: toKeypathLayerPoint

		//state.properties = _buildPropertyMap();

	};return Object.assign(state, KeyPathNode(state), methods);
}

module.exports = ShapeContents;

},{"../../helpers/transformationMatrix":8,"../../key_path/KeyPathNode":12,"../../property/Property":40,"../transform/Transform":39,"./ShapeEllipse":24,"./ShapeFill":25,"./ShapeGradientFill":26,"./ShapeGradientStroke":27,"./ShapePath":28,"./ShapePolystar":29,"./ShapeRectangle":30,"./ShapeRepeater":31,"./ShapeRoundCorners":32,"./ShapeStroke":33,"./ShapeTrimPaths":34}],24:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../../key_path/KeyPathNode');
var Property = require('../../property/Property');

function ShapeEllipse(element, parent) {

	var state = {
		parent: parent,
		properties: _buildPropertyMap()
	};

	function _buildPropertyMap() {
		return [{
			name: 'Size',
			value: Property(element.sh.s, parent)
		}, {
			name: 'Position',
			value: Property(element.sh.p, parent)
		}];
	}

	var methods = {};

	return Object.assign(methods, KeyPathNode(state));
}

module.exports = ShapeEllipse;

},{"../../key_path/KeyPathNode":12,"../../property/Property":40}],25:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../../key_path/KeyPathNode');
var Property = require('../../property/Property');

function ShapeFill(element, parent) {

	var state = {
		parent: parent,
		properties: _buildPropertyMap()
	};

	function _buildPropertyMap() {
		return [{
			name: 'Color',
			value: Property(element.c, parent)
		}, {
			name: 'Opacity',
			value: Property(element.o, parent)
		}];
	}

	var methods = {};

	return Object.assign(methods, KeyPathNode(state));
}

module.exports = ShapeFill;

},{"../../key_path/KeyPathNode":12,"../../property/Property":40}],26:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../../key_path/KeyPathNode');
var Property = require('../../property/Property');

function ShapeGradientFill(element, parent) {

	var state = {
		parent: parent,
		properties: _buildPropertyMap()
	};

	function _buildPropertyMap() {
		return [{
			name: 'Start Point',
			value: Property(element.s, parent)
		}, {
			name: 'End Point',
			value: Property(element.s, parent)
		}, {
			name: 'Opacity',
			value: Property(element.o, parent)
		}, {
			name: 'Highlight Length',
			value: Property(element.h, parent)
		}, {
			name: 'Highlight Angle',
			value: Property(element.a, parent)
		}, {
			name: 'Colors',
			value: Property(element.g.prop, parent)
		}];
	}

	var methods = {};

	return Object.assign(methods, KeyPathNode(state));
}

module.exports = ShapeGradientFill;

},{"../../key_path/KeyPathNode":12,"../../property/Property":40}],27:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../../key_path/KeyPathNode');
var Property = require('../../property/Property');

function ShapeGradientStroke(element, parent) {

	var state = {
		parent: parent,
		properties: _buildPropertyMap()
	};

	function _buildPropertyMap() {
		return [{
			name: 'Start Point',
			value: Property(element.s, parent)
		}, {
			name: 'End Point',
			value: Property(element.e, parent)
		}, {
			name: 'Opacity',
			value: Property(element.o, parent)
		}, {
			name: 'Highlight Length',
			value: Property(element.h, parent)
		}, {
			name: 'Highlight Angle',
			value: Property(element.a, parent)
		}, {
			name: 'Colors',
			value: Property(element.g.prop, parent)
		}, {
			name: 'Stroke Width',
			value: Property(element.w, parent)
		}];
	}

	var methods = {};

	return Object.assign(methods, KeyPathNode(state));
}

module.exports = ShapeGradientStroke;

},{"../../key_path/KeyPathNode":12,"../../property/Property":40}],28:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../../key_path/KeyPathNode');
var Property = require('../../property/Property');

function ShapePath(element, parent) {

	var state = {
		parent: parent,
		properties: _buildPropertyMap()
	};

	function setPath(value) {
		Property(element.sh).setValue(value);
	}

	function _buildPropertyMap() {
		return [{
			name: 'path',
			value: Property(element.sh, parent)
		}];
	}

	var methods = {};

	return Object.assign(methods, KeyPathNode(state));
}

module.exports = ShapePath;

},{"../../key_path/KeyPathNode":12,"../../property/Property":40}],29:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../../key_path/KeyPathNode');
var Property = require('../../property/Property');

function ShapePolystar(element, parent) {

	var state = {
		parent: parent,
		properties: _buildPropertyMap()
	};

	function _buildPropertyMap() {
		return [{
			name: 'Points',
			value: Property(element.sh.pt, parent)
		}, {
			name: 'Position',
			value: Property(element.sh.p, parent)
		}, {
			name: 'Rotation',
			value: Property(element.sh.r, parent)
		}, {
			name: 'Inner Radius',
			value: Property(element.sh.ir, parent)
		}, {
			name: 'Outer Radius',
			value: Property(element.sh.or, parent)
		}, {
			name: 'Inner Roundness',
			value: Property(element.sh.is, parent)
		}, {
			name: 'Outer Roundness',
			value: Property(element.sh.os, parent)
		}];
	}

	var methods = {};

	return Object.assign(methods, KeyPathNode(state));
}

module.exports = ShapePolystar;

},{"../../key_path/KeyPathNode":12,"../../property/Property":40}],30:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../../key_path/KeyPathNode');
var Property = require('../../property/Property');

function ShapeRectangle(element, parent) {

	var state = {
		parent: parent,
		properties: _buildPropertyMap()
	};

	function _buildPropertyMap() {
		return [{
			name: 'Size',
			value: Property(element.sh.s, parent)
		}, {
			name: 'Position',
			value: Property(element.sh.p, parent)
		}, {
			name: 'Roundness',
			value: Property(element.sh.r, parent)
		}];
	}

	var methods = {};

	return Object.assign(methods, KeyPathNode(state));
}

module.exports = ShapeRectangle;

},{"../../key_path/KeyPathNode":12,"../../property/Property":40}],31:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../../key_path/KeyPathNode');
var Property = require('../../property/Property');
var Transform = require('../transform/Transform');

function ShapeRepeater(element, parent) {

	var state = {
		parent: parent,
		properties: _buildPropertyMap()
	};

	function _buildPropertyMap() {
		return [{
			name: 'Copies',
			value: Property(element.c, parent)
		}, {
			name: 'Offset',
			value: Property(element.o, parent)
		}, {
			name: 'Transform',
			value: Transform(element.tr, parent)
		}];
	}

	var methods = {};

	return Object.assign(methods, KeyPathNode(state));
}

module.exports = ShapeRepeater;

},{"../../key_path/KeyPathNode":12,"../../property/Property":40,"../transform/Transform":39}],32:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../../key_path/KeyPathNode');
var Property = require('../../property/Property');

function ShapeRoundCorners(element, parent) {

	var state = {
		parent: parent,
		properties: _buildPropertyMap()
	};

	function _buildPropertyMap() {
		return [{
			name: 'Radius',
			value: Property(element.rd, parent)
		}];
	}

	var methods = {};

	return Object.assign(methods, KeyPathNode(state));
}

module.exports = ShapeRoundCorners;

},{"../../key_path/KeyPathNode":12,"../../property/Property":40}],33:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../../key_path/KeyPathNode');
var Property = require('../../property/Property');

function ShapeStroke(element, parent) {
	var state = {
		parent: parent,
		properties: _buildPropertyMap()
	};

	function _buildPropertyMap() {
		return [{
			name: 'Color',
			value: Property(element.c, parent)
		}, {
			name: 'Stroke Width',
			value: Property(element.w, parent)
		}, {
			name: 'Opacity',
			value: Property(element.o, parent)
		}];
	}

	var methods = {};

	return Object.assign(methods, KeyPathNode(state));
}

module.exports = ShapeStroke;

},{"../../key_path/KeyPathNode":12,"../../property/Property":40}],34:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../../key_path/KeyPathNode');
var Property = require('../../property/Property');

function ShapeTrimPaths(element, parent) {

	var state = {
		parent: parent,
		properties: _buildPropertyMap()
	};

	function _buildPropertyMap() {
		return [{
			name: 'Start',
			value: Property(element.s, parent)
		}, {
			name: 'End',
			value: Property(element.e, parent)
		}, {
			name: 'Offset',
			value: Property(element.o, parent)
		}];
	}

	var methods = {};

	return Object.assign(methods, KeyPathNode(state));
}

module.exports = ShapeTrimPaths;

},{"../../key_path/KeyPathNode":12,"../../property/Property":40}],35:[function(require,module,exports){
'use strict';

var LayerBase = require('../LayerBase');

function Solid(element, parent) {

	var state = {
		element: element,
		parent: parent,
		properties: _buildPropertyMap()
	};

	function _buildPropertyMap() {
		return [];
	}

	var methods = {};

	return Object.assign({}, LayerBase(state), methods);
}

module.exports = Solid;

},{"../LayerBase":13}],36:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../../key_path/KeyPathNode');
var Property = require('../../property/Property');
var TextAnimator = require('./TextAnimator');

function Text(element, parent) {

	var instance = {};

	var state = {
		element: element,
		parent: parent,
		properties: _buildPropertyMap()
	};

	function setDocumentData(_function) {
		var previousValue;
		setInterval(function () {
			var newValue = _function(element.textProperty.currentData);
			if (previousValue !== newValue) {
				element.updateDocumentData(newValue);
			}
		}, 500);
		console.log(element);
	}

	function addAnimators() {
		var animatorProperties = [];
		var animators = element.textAnimator._animatorsData;
		var i,
		    len = animators.length;
		var textAnimator;
		for (i = 0; i < len; i += 1) {
			textAnimator = TextAnimator(animators[i]);
			animatorProperties.push({
				name: element.textAnimator._textData.a[i].nm || 'Animator ' + (i + 1), //Fallback for old animations
				value: textAnimator
			});
		}
		return animatorProperties;
	}

	function _buildPropertyMap() {
		return [{
			name: 'Source',
			value: {
				setValue: setDocumentData
			}
		}].concat(addAnimators());
	}

	var methods = {};

	return Object.assign(instance, methods, KeyPathNode(state));
}

module.exports = Text;

},{"../../key_path/KeyPathNode":12,"../../property/Property":40,"./TextAnimator":37}],37:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../../key_path/KeyPathNode');
var Property = require('../../property/Property');

function TextAnimator(animator) {

	var instance = {};

	var state = {
		properties: _buildPropertyMap()
	};

	function setAnchorPoint(value) {
		Property(animator.a.a).setValue(value);
	}

	function setFillBrightness(value) {
		Property(animator.a.fb).setValue(value);
	}

	function setFillColor(value) {
		Property(animator.a.fc).setValue(value);
	}

	function setFillHue(value) {
		Property(animator.a.fh).setValue(value);
	}

	function setFillSaturation(value) {
		Property(animator.a.fs).setValue(value);
	}

	function setFillOpacity(value) {
		Property(animator.a.fo).setValue(value);
	}

	function setOpacity(value) {
		Property(animator.a.o).setValue(value);
	}

	function setPosition(value) {
		Property(animator.a.p).setValue(value);
	}

	function setRotation(value) {
		Property(animator.a.r).setValue(value);
	}

	function setRotationX(value) {
		Property(animator.a.rx).setValue(value);
	}

	function setRotationY(value) {
		Property(animator.a.ry).setValue(value);
	}

	function setScale(value) {
		Property(animator.a.s).setValue(value);
	}

	function setSkewAxis(value) {
		Property(animator.a.sa).setValue(value);
	}

	function setStrokeColor(value) {
		Property(animator.a.sc).setValue(value);
	}

	function setSkew(value) {
		Property(animator.a.sk).setValue(value);
	}

	function setStrokeOpacity(value) {
		Property(animator.a.so).setValue(value);
	}

	function setStrokeWidth(value) {
		Property(animator.a.sw).setValue(value);
	}

	function setStrokeBrightness(value) {
		Property(animator.a.sb).setValue(value);
	}

	function setStrokeHue(value) {
		Property(animator.a.sh).setValue(value);
	}

	function setStrokeSaturation(value) {
		Property(animator.a.ss).setValue(value);
	}

	function setTrackingAmount(value) {
		Property(animator.a.t).setValue(value);
	}

	function _buildPropertyMap() {
		return [{
			name: 'Anchor Point',
			value: {
				setValue: setAnchorPoint
			}
		}, {
			name: 'Fill Brightness',
			value: {
				setValue: setFillBrightness
			}
		}, {
			name: 'Fill Color',
			value: {
				setValue: setFillColor
			}
		}, {
			name: 'Fill Hue',
			value: {
				setValue: setFillHue
			}
		}, {
			name: 'Fill Saturation',
			value: {
				setValue: setFillSaturation
			}
		}, {
			name: 'Fill Opacity',
			value: {
				setValue: setFillOpacity
			}
		}, {
			name: 'Opacity',
			value: {
				setValue: setOpacity
			}
		}, {
			name: 'Position',
			value: {
				setValue: setPosition
			}
		}, {
			name: 'Rotation X',
			value: {
				setValue: setRotationX
			}
		}, {
			name: 'Rotation Y',
			value: {
				setValue: setRotationY
			}
		}, {
			name: 'Scale',
			value: {
				setValue: setScale
			}
		}, {
			name: 'Skew Axis',
			value: {
				setValue: setSkewAxis
			}
		}, {
			name: 'Stroke Color',
			value: {
				setValue: setStrokeColor
			}
		}, {
			name: 'Skew',
			value: {
				setValue: setSkew
			}
		}, {
			name: 'Stroke Width',
			value: {
				setValue: setStrokeWidth
			}
		}, {
			name: 'Tracking Amount',
			value: {
				setValue: setTrackingAmount
			}
		}, {
			name: 'Stroke Opacity',
			value: {
				setValue: setStrokeOpacity
			}
		}, {
			name: 'Stroke Brightness',
			value: {
				setValue: setStrokeBrightness
			}
		}, {
			name: 'Stroke Saturation',
			value: {
				setValue: setStrokeSaturation
			}
		}, {
			name: 'Stroke Hue',
			value: {
				setValue: setStrokeHue
			}
		}];
	}

	var methods = {};

	return Object.assign(instance, methods, KeyPathNode(state));
}

module.exports = TextAnimator;

},{"../../key_path/KeyPathNode":12,"../../property/Property":40}],38:[function(require,module,exports){
'use strict';

var LayerBase = require('../LayerBase');
var Text = require('./Text');

function TextElement(element) {

	var instance = {};

	var TextProperty = Text(element);
	var state = {
		element: element,
		properties: _buildPropertyMap()
	};

	function _buildPropertyMap() {
		return [{
			name: 'text',
			value: TextProperty
		}, {
			name: 'Text',
			value: TextProperty
		}];
	}

	function getText() {
		return element.textProperty.currentData.t;
	}

	function setText(value, index) {
		setDocumentData({ t: value }, index);
	}

	function setDocumentData(data, index) {
		return element.updateDocumentData(data, index);
	}

	function canResizeFont(_canResize) {
		return element.canResizeFont(_canResize);
	}

	function setMinimumFontSize(_fontSize) {
		return element.setMinimumFontSize(_fontSize);
	}

	var methods = {
		getText: getText,
		setText: setText,
		canResizeFont: canResizeFont,
		setDocumentData: setDocumentData,
		setMinimumFontSize: setMinimumFontSize
	};

	return Object.assign(instance, LayerBase(state), methods);
}

module.exports = TextElement;

},{"../LayerBase":13,"./Text":36}],39:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../../key_path/KeyPathNode');
var Property = require('../../property/Property');

function Transform(props, parent) {
	var state = {
		properties: _buildPropertyMap()
	};

	function _buildPropertyMap() {
		return [{
			name: 'Anchor Point',
			value: Property(props.a, parent)
		}, {
			name: 'Point of Interest',
			value: Property(props.a, parent)
		}, {
			name: 'Position',
			value: Property(props.p, parent)
		}, {
			name: 'Scale',
			value: Property(props.s, parent)
		}, {
			name: 'Rotation',
			value: Property(props.r, parent)
		}, {
			name: 'X Position',
			value: Property(props.px, parent)
		}, {
			name: 'Y Position',
			value: Property(props.py, parent)
		}, {
			name: 'Z Position',
			value: Property(props.pz, parent)
		}, {
			name: 'X Rotation',
			value: Property(props.rx, parent)
		}, {
			name: 'Y Rotation',
			value: Property(props.ry, parent)
		}, {
			name: 'Z Rotation',
			value: Property(props.rz, parent)
		}, {
			name: 'Opacity',
			value: Property(props.o, parent)
		}];
	}

	function getTargetTransform() {
		return props;
	}

	var methods = {
		getTargetTransform: getTargetTransform
	};

	return Object.assign(methods, KeyPathNode(state));
}

module.exports = Transform;

},{"../../key_path/KeyPathNode":12,"../../property/Property":40}],40:[function(require,module,exports){
'use strict';

var KeyPathNode = require('../key_path/KeyPathNode');
var ValueProperty = require('./ValueProperty');

function Property(property, parent) {
	var state = {
		property: property,
		parent: parent
	};

	function destroy() {
		state.property = null;
		state.parent = null;
	}

	var methods = {
		destroy: destroy
	};

	return Object.assign({}, methods, ValueProperty(state), KeyPathNode(state));
}

module.exports = Property;

},{"../key_path/KeyPathNode":12,"./ValueProperty":41}],41:[function(require,module,exports){
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function ValueProperty(state) {

	function setValue(value) {
		var property = state.property;
		if (!property || !property.addEffect) {
			return;
		}
		if (typeof value === 'function') {
			return property.addEffect(value);
		} else if (property.propType === 'multidimensional' && (typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object' && value.length === 2) {
			return property.addEffect(function () {
				return value;
			});
		} else if (property.propType === 'unidimensional' && typeof value === 'number') {
			return property.addEffect(function () {
				return value;
			});
		}
	}

	function getValue() {
		return state.property.v;
	}

	var methods = {
		setValue: setValue,
		getValue: getValue
	};

	return methods;
}

module.exports = ValueProperty;

},{}],42:[function(require,module,exports){
'use strict';

var LayerList = require('../layer/LayerList');
var KeyPathList = require('../key_path/KeyPathList');

function Renderer(state) {

	state._type = 'renderer';

	function getRendererType() {
		return state.animation.animType;
	}

	return Object.assign({
		getRendererType: getRendererType
	}, LayerList(state.elements), KeyPathList(state.elements, 'renderer'));
}

module.exports = Renderer;

},{"../key_path/KeyPathList":11,"../layer/LayerList":14}]},{},[10])(10)
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvYW5pbWF0aW9uL0FuaW1hdGlvbkl0ZW0uanMiLCJzcmMvZW51bXMva2V5X3BhdGhfc2VwYXJhdG9yLmpzIiwic3JjL2VudW1zL2xheWVyX3R5cGVzLmpzIiwic3JjL2VudW1zL3Byb3BlcnR5X25hbWVzLmpzIiwic3JjL2hlbHBlcnMva2V5UGF0aEJ1aWxkZXIuanMiLCJzcmMvaGVscGVycy9sYXllckFQSUJ1aWxkZXIuanMiLCJzcmMvaGVscGVycy9zdHJpbmdTYW5pdGl6ZXIuanMiLCJzcmMvaGVscGVycy90cmFuc2Zvcm1hdGlvbk1hdHJpeC5qcyIsInNyYy9oZWxwZXJzL3R5cGVkQXJyYXlzLmpzIiwic3JjL2luZGV4LmpzIiwic3JjL2tleV9wYXRoL0tleVBhdGhMaXN0LmpzIiwic3JjL2tleV9wYXRoL0tleVBhdGhOb2RlLmpzIiwic3JjL2xheWVyL0xheWVyQmFzZS5qcyIsInNyYy9sYXllci9MYXllckxpc3QuanMiLCJzcmMvbGF5ZXIvY2FtZXJhL0NhbWVyYS5qcyIsInNyYy9sYXllci9jb21wb3NpdGlvbi9Db21wb3NpdGlvbi5qcyIsInNyYy9sYXllci9jb21wb3NpdGlvbi9UaW1lUmVtYXAuanMiLCJzcmMvbGF5ZXIvZWZmZWN0cy9FZmZlY3RFbGVtZW50LmpzIiwic3JjL2xheWVyL2VmZmVjdHMvRWZmZWN0cy5qcyIsInNyYy9sYXllci9pbWFnZS9JbWFnZUVsZW1lbnQuanMiLCJzcmMvbGF5ZXIvbnVsbF9lbGVtZW50L051bGxFbGVtZW50LmpzIiwic3JjL2xheWVyL3NoYXBlL1NoYXBlLmpzIiwic3JjL2xheWVyL3NoYXBlL1NoYXBlQ29udGVudHMuanMiLCJzcmMvbGF5ZXIvc2hhcGUvU2hhcGVFbGxpcHNlLmpzIiwic3JjL2xheWVyL3NoYXBlL1NoYXBlRmlsbC5qcyIsInNyYy9sYXllci9zaGFwZS9TaGFwZUdyYWRpZW50RmlsbC5qcyIsInNyYy9sYXllci9zaGFwZS9TaGFwZUdyYWRpZW50U3Ryb2tlLmpzIiwic3JjL2xheWVyL3NoYXBlL1NoYXBlUGF0aC5qcyIsInNyYy9sYXllci9zaGFwZS9TaGFwZVBvbHlzdGFyLmpzIiwic3JjL2xheWVyL3NoYXBlL1NoYXBlUmVjdGFuZ2xlLmpzIiwic3JjL2xheWVyL3NoYXBlL1NoYXBlUmVwZWF0ZXIuanMiLCJzcmMvbGF5ZXIvc2hhcGUvU2hhcGVSb3VuZENvcm5lcnMuanMiLCJzcmMvbGF5ZXIvc2hhcGUvU2hhcGVTdHJva2UuanMiLCJzcmMvbGF5ZXIvc2hhcGUvU2hhcGVUcmltUGF0aHMuanMiLCJzcmMvbGF5ZXIvc29saWQvU29saWRFbGVtZW50LmpzIiwic3JjL2xheWVyL3RleHQvVGV4dC5qcyIsInNyYy9sYXllci90ZXh0L1RleHRBbmltYXRvci5qcyIsInNyYy9sYXllci90ZXh0L1RleHRFbGVtZW50LmpzIiwic3JjL2xheWVyL3RyYW5zZm9ybS9UcmFuc2Zvcm0uanMiLCJzcmMvcHJvcGVydHkvUHJvcGVydHkuanMiLCJzcmMvcHJvcGVydHkvVmFsdWVQcm9wZXJ0eS5qcyIsInNyYy9yZW5kZXJlci9SZW5kZXJlci5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7O0FDQUEsSUFBSSxXQUFXLFFBQVEsc0JBQVIsQ0FBZjtBQUNBLElBQUksWUFBWSxRQUFRLDRCQUFSLENBQWhCOztBQUVBLFNBQVMsb0JBQVQsQ0FBOEIsU0FBOUIsRUFBeUM7O0FBRXhDLEtBQUksUUFBUTtBQUNYLGFBQVcsU0FEQTtBQUVYLFlBQVUsVUFBVSxRQUFWLENBQW1CLFFBQW5CLENBQTRCLEdBQTVCLENBQWdDLFVBQUMsSUFBRDtBQUFBLFVBQVUsVUFBVSxJQUFWLEVBQWdCLFNBQWhCLENBQVY7QUFBQSxHQUFoQyxDQUZDO0FBR1gsZ0JBQWMsSUFISDtBQUlYLGFBQVc7QUFKQSxFQUFaOztBQU9BLFVBQVMsZUFBVCxHQUEyQjtBQUMxQixTQUFPLFVBQVUsWUFBakI7QUFDQTs7QUFFRCxVQUFTLGNBQVQsR0FBMEI7QUFDekIsU0FBTyxVQUFVLFlBQVYsR0FBeUIsVUFBVSxTQUExQztBQUNBOztBQUVELFVBQVMsZ0JBQVQsQ0FBMEIsVUFBMUIsRUFBc0MsS0FBdEMsRUFBNkM7QUFDNUMsTUFBSSxDQUFKO0FBQUEsTUFBTyxNQUFNLFdBQVcsTUFBeEI7QUFDQSxPQUFLLElBQUksQ0FBVCxFQUFZLElBQUksR0FBaEIsRUFBcUIsS0FBSyxDQUExQixFQUE2QjtBQUM1QixjQUFXLGtCQUFYLENBQThCLENBQTlCLEVBQWlDLFFBQWpDLENBQTBDLEtBQTFDO0FBQ0E7QUFDRDs7QUFFRCxVQUFTLG1CQUFULENBQTZCLFVBQTdCLEVBQXlDLEtBQXpDLEVBQWdEO0FBQy9DLE1BQUksQ0FBSjtBQUFBLE1BQU8sTUFBTSxXQUFXLE1BQXhCO0FBQ0EsTUFBSSxTQUFTLEVBQWI7QUFDQSxPQUFLLElBQUksQ0FBVCxFQUFZLElBQUksR0FBaEIsRUFBcUIsS0FBSyxDQUExQixFQUE2QjtBQUM1QixVQUFPLElBQVAsQ0FBWSxXQUFXLGtCQUFYLENBQThCLENBQTlCLEVBQWlDLG1CQUFqQyxDQUFxRCxLQUFyRCxDQUFaO0FBQ0E7QUFDRCxNQUFHLE9BQU8sTUFBUCxLQUFrQixDQUFyQixFQUF3QjtBQUN2QixVQUFPLE9BQU8sQ0FBUCxDQUFQO0FBQ0E7QUFDRCxTQUFPLE1BQVA7QUFDQTs7QUFFRCxVQUFTLHFCQUFULENBQStCLFVBQS9CLEVBQTJDLEtBQTNDLEVBQWtEO0FBQ2pELE1BQUksQ0FBSjtBQUFBLE1BQU8sTUFBTSxXQUFXLE1BQXhCO0FBQ0EsTUFBSSxTQUFTLEVBQWI7QUFDQSxPQUFLLElBQUksQ0FBVCxFQUFZLElBQUksR0FBaEIsRUFBcUIsS0FBSyxDQUExQixFQUE2QjtBQUM1QixVQUFPLElBQVAsQ0FBWSxXQUFXLGtCQUFYLENBQThCLENBQTlCLEVBQWlDLHFCQUFqQyxDQUF1RCxLQUF2RCxDQUFaO0FBQ0E7QUFDRCxNQUFHLE9BQU8sTUFBUCxLQUFrQixDQUFyQixFQUF3QjtBQUN2QixVQUFPLE9BQU8sQ0FBUCxDQUFQO0FBQ0E7QUFDRCxTQUFPLE1BQVA7QUFDQTs7QUFFRCxVQUFTLGtCQUFULENBQTRCLFlBQTVCLEVBQTBDO0FBQ3pDLE1BQUksWUFBWSxVQUFVLGFBQVYsQ0FBd0IsQ0FBeEM7QUFDTSxNQUFJLGFBQWEsVUFBVSxhQUFWLENBQXdCLENBQXpDO0FBQ04sTUFBSSxVQUFVLFlBQVksVUFBMUI7QUFDTSxNQUFJLGVBQWUsYUFBYSxLQUFoQztBQUNBLE1BQUksZ0JBQWdCLGFBQWEsTUFBakM7QUFDQSxNQUFJLGFBQWEsZUFBZSxhQUFoQztBQUNBLE1BQUksS0FBSixFQUFVLFlBQVYsRUFBdUIsWUFBdkI7QUFDQSxNQUFJLFVBQUosRUFBZ0IsVUFBaEIsRUFBNEIsU0FBNUI7QUFDQSxNQUFJLGNBQWMsVUFBVSxRQUFWLENBQW1CLFlBQW5CLENBQWdDLG1CQUFoQyxDQUFvRCxLQUFwRCxDQUEwRCxHQUExRCxDQUFsQjtBQUNBLE1BQUcsWUFBWSxDQUFaLE1BQW1CLE1BQXRCLEVBQThCO0FBQzdCLFdBQVEsYUFBYSxPQUFiLEdBQXVCLGdCQUFnQixVQUF2QyxHQUFvRCxlQUFlLFNBQTNFO0FBQ0EsR0FGRCxNQUVPO0FBQ04sV0FBUSxhQUFhLE9BQWIsR0FBdUIsZUFBZSxTQUF0QyxHQUFrRCxnQkFBZ0IsVUFBMUU7QUFDQTtBQUNELGVBQWEsWUFBWSxDQUFaLEVBQWUsTUFBZixDQUFzQixDQUF0QixFQUF3QixDQUF4QixDQUFiO0FBQ0EsZUFBYSxZQUFZLENBQVosRUFBZSxNQUFmLENBQXNCLENBQXRCLENBQWI7QUFDQSxNQUFHLGVBQWUsTUFBbEIsRUFBMEI7QUFDekIsa0JBQWUsQ0FBZjtBQUNBLEdBRkQsTUFFTyxJQUFHLGVBQWUsTUFBbEIsRUFBMEI7QUFDaEMsa0JBQWUsQ0FBQyxlQUFlLFlBQVksS0FBNUIsSUFBcUMsQ0FBcEQ7QUFDQSxHQUZNLE1BRUE7QUFDTixrQkFBZ0IsZUFBZSxZQUFZLEtBQTNDO0FBQ0E7O0FBRUQsTUFBRyxlQUFlLE1BQWxCLEVBQTBCO0FBQ3pCLGtCQUFlLENBQWY7QUFDQSxHQUZELE1BRU8sSUFBRyxlQUFlLE1BQWxCLEVBQTBCO0FBQ2hDLGtCQUFlLENBQUMsZ0JBQWdCLGFBQWEsS0FBOUIsSUFBdUMsQ0FBdEQ7QUFDQSxHQUZNLE1BRUE7QUFDTixrQkFBZ0IsZ0JBQWdCLGFBQWEsS0FBN0M7QUFDQTtBQUNELFNBQU87QUFDTixpQkFBYyxZQURSO0FBRU4saUJBQWMsWUFGUjtBQUdOLFVBQU87QUFIRCxHQUFQO0FBS047O0FBRUQsVUFBUyxlQUFULENBQXlCLFNBQXpCLEVBQW9DO0FBQ25DLE1BQUksWUFBWSxVQUFVLE9BQTFCO0FBQ0EsUUFBTSxZQUFOLEdBQXFCLFVBQVUscUJBQVYsRUFBckI7QUFDQSxRQUFNLFNBQU4sR0FBa0IsbUJBQW1CLE1BQU0sWUFBekIsQ0FBbEI7QUFDQTs7QUFFRCxVQUFTLGdCQUFULENBQTBCLEtBQTFCLEVBQWlDO0FBQ2hDLE1BQUcsQ0FBQyxVQUFVLE9BQVgsSUFBc0IsQ0FBQyxVQUFVLE9BQVYsQ0FBa0IscUJBQTVDLEVBQW1FO0FBQ2xFLFVBQU8sS0FBUDtBQUNBO0FBQ0QsTUFBRyxDQUFDLE1BQU0sWUFBVixFQUF3QjtBQUN2QjtBQUNBOztBQUVELE1BQUksZUFBZSxNQUFNLFlBQXpCO0FBQ0EsTUFBSSxXQUFXLENBQUMsTUFBTSxDQUFOLElBQVcsYUFBYSxJQUF6QixFQUErQixNQUFNLENBQU4sSUFBVyxhQUFhLEdBQXZELENBQWY7QUFDQSxNQUFJLFlBQVksTUFBTSxTQUF0Qjs7QUFFTSxXQUFTLENBQVQsSUFBYyxDQUFDLFNBQVMsQ0FBVCxJQUFjLFVBQVUsWUFBekIsSUFBeUMsVUFBVSxLQUFqRTtBQUNBLFdBQVMsQ0FBVCxJQUFjLENBQUMsU0FBUyxDQUFULElBQWMsVUFBVSxZQUF6QixJQUF5QyxVQUFVLEtBQWpFOztBQUVOLFNBQU8sUUFBUDtBQUNBOztBQUVELFVBQVMsa0JBQVQsQ0FBNEIsS0FBNUIsRUFBbUM7QUFDbEMsTUFBRyxDQUFDLFVBQVUsT0FBWCxJQUFzQixDQUFDLFVBQVUsT0FBVixDQUFrQixxQkFBNUMsRUFBbUU7QUFDbEUsVUFBTyxLQUFQO0FBQ0E7QUFDRCxNQUFHLENBQUMsTUFBTSxZQUFWLEVBQXdCO0FBQ3ZCO0FBQ0E7QUFDRCxNQUFJLGVBQWUsTUFBTSxZQUF6QjtBQUNBLE1BQUksWUFBWSxNQUFNLFNBQXRCOztBQUVBLE1BQUksV0FBVyxDQUFDLE1BQU0sQ0FBTixJQUFXLFVBQVUsS0FBckIsR0FBNkIsVUFBVSxZQUF4QyxFQUFzRCxNQUFNLENBQU4sSUFBVyxVQUFVLEtBQXJCLEdBQTZCLFVBQVUsWUFBN0YsQ0FBZjs7QUFFQSxNQUFJLFdBQVcsQ0FBQyxTQUFTLENBQVQsSUFBYyxhQUFhLElBQTVCLEVBQWtDLFNBQVMsQ0FBVCxJQUFjLGFBQWEsR0FBN0QsQ0FBZjtBQUNBLFNBQU8sUUFBUDtBQUNBOztBQUVELFVBQVMsWUFBVCxHQUF3QjtBQUN2QixTQUFPLE1BQU0sU0FBYjtBQUNBOztBQUVELEtBQUksVUFBVTtBQUNiLG1CQUFpQixlQURKO0FBRWIsZ0JBQWMsWUFGRDtBQUdiLG9CQUFrQixnQkFITDtBQUliLHNCQUFvQixrQkFKUDtBQUtiLG1CQUFpQixlQUxKO0FBTWIsa0JBQWdCLGNBTkg7QUFPYixvQkFBa0IsZ0JBUEw7QUFRYix1QkFBcUIsbUJBUlI7QUFTYix5QkFBdUI7QUFUVixFQUFkOztBQVlBLFFBQU8sT0FBTyxNQUFQLENBQWMsRUFBZCxFQUFrQixTQUFTLEtBQVQsQ0FBbEIsRUFBbUMsT0FBbkMsQ0FBUDtBQUNBOztBQUVELE9BQU8sT0FBUCxHQUFpQixvQkFBakI7Ozs7O0FDckpBLE9BQU8sT0FBUCxHQUFpQixHQUFqQjs7Ozs7QUNBQSxPQUFPLE9BQVAsR0FBaUI7QUFDZixJQUFHLENBRFk7QUFFZixJQUFHLENBRlk7QUFHZixJQUFHLENBSFk7QUFJZixJQUFHLENBSlk7QUFLZixJQUFHLENBTFk7QUFNZixJQUFHLENBTlk7QUFPZixLQUFJLEVBUFc7QUFRaEIsU0FBUSxDQVJRO0FBU2hCLGdCQUFlLENBVEM7QUFVaEIsVUFBUyxDQVZPO0FBV2hCLFVBQVMsQ0FYTztBQVloQixTQUFRLENBWlE7QUFhaEIsVUFBUyxDQWJPO0FBY2hCLFNBQVEsQ0FkUTtBQWVoQixXQUFVO0FBZk0sQ0FBakI7Ozs7O0FDQUEsT0FBTyxPQUFQLEdBQWlCO0FBQ2hCLGtCQUFpQjtBQURELENBQWpCOzs7OztBQ0FBLElBQUkscUJBQXFCLFFBQVEsNkJBQVIsQ0FBekI7QUFDQSxJQUFJLGlCQUFpQixRQUFRLG1CQUFSLENBQXJCOztBQUVBLE9BQU8sT0FBUCxHQUFpQixVQUFTLFlBQVQsRUFBdUI7QUFDdkMsS0FBSSxlQUFlLGFBQWEsS0FBYixDQUFtQixrQkFBbkIsQ0FBbkI7QUFDQSxLQUFJLFdBQVcsYUFBYSxLQUFiLEVBQWY7QUFDQSxRQUFPO0FBQ04sWUFBVSxlQUFlLFFBQWYsQ0FESjtBQUVOLGdCQUFjLGFBQWEsSUFBYixDQUFrQixrQkFBbEI7QUFGUixFQUFQO0FBSUEsQ0FQRDs7Ozs7QUNIQSxJQUFJLGNBQWMsUUFBUSwyQkFBUixDQUFsQjtBQUNBLElBQUksZUFBZSxRQUFRLHNCQUFSLENBQW5CO0FBQ0EsSUFBSSxjQUFjLFFBQVEsbUNBQVIsQ0FBbEI7QUFDQSxJQUFJLGVBQWUsUUFBUSw2QkFBUixDQUFuQjtBQUNBLElBQUksZUFBZSxRQUFRLDZCQUFSLENBQW5CO0FBQ0EsSUFBSSxnQkFBZ0IsUUFBUSx3QkFBUixDQUFwQjtBQUNBLElBQUksWUFBWSxRQUFRLG9CQUFSLENBQWhCOztBQUdBLE9BQU8sT0FBUCxHQUFpQixTQUFTLFdBQVQsQ0FBcUIsT0FBckIsRUFBOEIsTUFBOUIsRUFBc0M7QUFDdEQsS0FBSSxZQUFZLFFBQVEsSUFBUixDQUFhLEVBQTdCO0FBQ0EsS0FBSSxjQUFjLFFBQVEsa0NBQVIsQ0FBbEI7QUFDQSxTQUFPLFNBQVA7QUFDQyxPQUFLLENBQUw7QUFDQSxVQUFPLFlBQVksT0FBWixFQUFxQixNQUFyQixDQUFQO0FBQ0EsT0FBSyxDQUFMO0FBQ0EsVUFBTyxhQUFhLE9BQWIsRUFBc0IsTUFBdEIsQ0FBUDtBQUNBLE9BQUssQ0FBTDtBQUNBLFVBQU8sYUFBYSxPQUFiLEVBQXNCLE1BQXRCLENBQVA7QUFDQSxPQUFLLENBQUw7QUFDQSxVQUFPLFlBQVksT0FBWixFQUFxQixNQUFyQixDQUFQO0FBQ0EsT0FBSyxDQUFMO0FBQ0EsVUFBTyxhQUFhLE9BQWIsRUFBc0IsTUFBdEIsRUFBOEIsUUFBUSxJQUFSLENBQWEsTUFBM0MsRUFBbUQsUUFBUSxTQUEzRCxDQUFQO0FBQ0EsT0FBSyxDQUFMO0FBQ0EsVUFBTyxZQUFZLE9BQVosRUFBcUIsTUFBckIsQ0FBUDtBQUNBLE9BQUssRUFBTDtBQUNBLFVBQU8sY0FBYyxPQUFkLEVBQXVCLE1BQXZCLENBQVA7QUFDQTtBQUNBLFVBQU8sVUFBVSxPQUFWLEVBQW1CLE1BQW5CLENBQVA7QUFoQkQ7QUFrQkEsQ0FyQkQ7Ozs7O0FDVEEsU0FBUyxjQUFULENBQXdCLE1BQXhCLEVBQWdDO0FBQy9CLFFBQU8sT0FBTyxJQUFQLEVBQVA7QUFDQTs7QUFFRCxPQUFPLE9BQVAsR0FBaUIsY0FBakI7Ozs7O0FDSkEsSUFBSSxtQkFBbUIsUUFBUSxlQUFSLENBQXZCOztBQUVBOzs7Ozs7Ozs7QUFTQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQkEsSUFBSSxTQUFVLFlBQVU7O0FBRXBCLFFBQUksT0FBTyxLQUFLLEdBQWhCO0FBQ0EsUUFBSSxPQUFPLEtBQUssR0FBaEI7QUFDQSxRQUFJLE9BQU8sS0FBSyxHQUFoQjtBQUNBLFFBQUksT0FBTyxLQUFLLEtBQWhCOztBQUVBLGFBQVMsS0FBVCxHQUFnQjtBQUNaLGFBQUssS0FBTCxDQUFXLENBQVgsSUFBZ0IsQ0FBaEI7QUFDQSxhQUFLLEtBQUwsQ0FBVyxDQUFYLElBQWdCLENBQWhCO0FBQ0EsYUFBSyxLQUFMLENBQVcsQ0FBWCxJQUFnQixDQUFoQjtBQUNBLGFBQUssS0FBTCxDQUFXLENBQVgsSUFBZ0IsQ0FBaEI7QUFDQSxhQUFLLEtBQUwsQ0FBVyxDQUFYLElBQWdCLENBQWhCO0FBQ0EsYUFBSyxLQUFMLENBQVcsQ0FBWCxJQUFnQixDQUFoQjtBQUNBLGFBQUssS0FBTCxDQUFXLENBQVgsSUFBZ0IsQ0FBaEI7QUFDQSxhQUFLLEtBQUwsQ0FBVyxDQUFYLElBQWdCLENBQWhCO0FBQ0EsYUFBSyxLQUFMLENBQVcsQ0FBWCxJQUFnQixDQUFoQjtBQUNBLGFBQUssS0FBTCxDQUFXLENBQVgsSUFBZ0IsQ0FBaEI7QUFDQSxhQUFLLEtBQUwsQ0FBVyxFQUFYLElBQWlCLENBQWpCO0FBQ0EsYUFBSyxLQUFMLENBQVcsRUFBWCxJQUFpQixDQUFqQjtBQUNBLGFBQUssS0FBTCxDQUFXLEVBQVgsSUFBaUIsQ0FBakI7QUFDQSxhQUFLLEtBQUwsQ0FBVyxFQUFYLElBQWlCLENBQWpCO0FBQ0EsYUFBSyxLQUFMLENBQVcsRUFBWCxJQUFpQixDQUFqQjtBQUNBLGFBQUssS0FBTCxDQUFXLEVBQVgsSUFBaUIsQ0FBakI7QUFDQSxlQUFPLElBQVA7QUFDSDs7QUFFRCxhQUFTLE1BQVQsQ0FBZ0IsS0FBaEIsRUFBdUI7QUFDbkIsWUFBRyxVQUFVLENBQWIsRUFBZTtBQUNYLG1CQUFPLElBQVA7QUFDSDtBQUNELFlBQUksT0FBTyxLQUFLLEtBQUwsQ0FBWDtBQUNBLFlBQUksT0FBTyxLQUFLLEtBQUwsQ0FBWDtBQUNBLGVBQU8sS0FBSyxFQUFMLENBQVEsSUFBUixFQUFjLENBQUMsSUFBZixFQUFzQixDQUF0QixFQUF5QixDQUF6QixFQUE0QixJQUE1QixFQUFtQyxJQUFuQyxFQUF5QyxDQUF6QyxFQUE0QyxDQUE1QyxFQUErQyxDQUEvQyxFQUFtRCxDQUFuRCxFQUF1RCxDQUF2RCxFQUEwRCxDQUExRCxFQUE2RCxDQUE3RCxFQUFnRSxDQUFoRSxFQUFtRSxDQUFuRSxFQUFzRSxDQUF0RSxDQUFQO0FBQ0g7O0FBRUQsYUFBUyxPQUFULENBQWlCLEtBQWpCLEVBQXVCO0FBQ25CLFlBQUcsVUFBVSxDQUFiLEVBQWU7QUFDWCxtQkFBTyxJQUFQO0FBQ0g7QUFDRCxZQUFJLE9BQU8sS0FBSyxLQUFMLENBQVg7QUFDQSxZQUFJLE9BQU8sS0FBSyxLQUFMLENBQVg7QUFDQSxlQUFPLEtBQUssRUFBTCxDQUFRLENBQVIsRUFBVyxDQUFYLEVBQWMsQ0FBZCxFQUFpQixDQUFqQixFQUFvQixDQUFwQixFQUF1QixJQUF2QixFQUE2QixDQUFDLElBQTlCLEVBQW9DLENBQXBDLEVBQXVDLENBQXZDLEVBQTBDLElBQTFDLEVBQWlELElBQWpELEVBQXVELENBQXZELEVBQTBELENBQTFELEVBQTZELENBQTdELEVBQWdFLENBQWhFLEVBQW1FLENBQW5FLENBQVA7QUFDSDs7QUFFRCxhQUFTLE9BQVQsQ0FBaUIsS0FBakIsRUFBdUI7QUFDbkIsWUFBRyxVQUFVLENBQWIsRUFBZTtBQUNYLG1CQUFPLElBQVA7QUFDSDtBQUNELFlBQUksT0FBTyxLQUFLLEtBQUwsQ0FBWDtBQUNBLFlBQUksT0FBTyxLQUFLLEtBQUwsQ0FBWDtBQUNBLGVBQU8sS0FBSyxFQUFMLENBQVEsSUFBUixFQUFlLENBQWYsRUFBbUIsSUFBbkIsRUFBeUIsQ0FBekIsRUFBNEIsQ0FBNUIsRUFBK0IsQ0FBL0IsRUFBa0MsQ0FBbEMsRUFBcUMsQ0FBckMsRUFBd0MsQ0FBQyxJQUF6QyxFQUFnRCxDQUFoRCxFQUFvRCxJQUFwRCxFQUEwRCxDQUExRCxFQUE2RCxDQUE3RCxFQUFnRSxDQUFoRSxFQUFtRSxDQUFuRSxFQUFzRSxDQUF0RSxDQUFQO0FBQ0g7O0FBRUQsYUFBUyxPQUFULENBQWlCLEtBQWpCLEVBQXVCO0FBQ25CLFlBQUcsVUFBVSxDQUFiLEVBQWU7QUFDWCxtQkFBTyxJQUFQO0FBQ0g7QUFDRCxZQUFJLE9BQU8sS0FBSyxLQUFMLENBQVg7QUFDQSxZQUFJLE9BQU8sS0FBSyxLQUFMLENBQVg7QUFDQSxlQUFPLEtBQUssRUFBTCxDQUFRLElBQVIsRUFBYyxDQUFDLElBQWYsRUFBc0IsQ0FBdEIsRUFBeUIsQ0FBekIsRUFBNEIsSUFBNUIsRUFBbUMsSUFBbkMsRUFBeUMsQ0FBekMsRUFBNEMsQ0FBNUMsRUFBK0MsQ0FBL0MsRUFBbUQsQ0FBbkQsRUFBdUQsQ0FBdkQsRUFBMEQsQ0FBMUQsRUFBNkQsQ0FBN0QsRUFBZ0UsQ0FBaEUsRUFBbUUsQ0FBbkUsRUFBc0UsQ0FBdEUsQ0FBUDtBQUNIOztBQUVELGFBQVMsS0FBVCxDQUFlLEVBQWYsRUFBa0IsRUFBbEIsRUFBcUI7QUFDakIsZUFBTyxLQUFLLEVBQUwsQ0FBUSxDQUFSLEVBQVcsRUFBWCxFQUFlLEVBQWYsRUFBbUIsQ0FBbkIsRUFBc0IsQ0FBdEIsRUFBeUIsQ0FBekIsQ0FBUDtBQUNIOztBQUVELGFBQVMsSUFBVCxDQUFjLEVBQWQsRUFBa0IsRUFBbEIsRUFBcUI7QUFDakIsZUFBTyxLQUFLLEtBQUwsQ0FBVyxLQUFLLEVBQUwsQ0FBWCxFQUFxQixLQUFLLEVBQUwsQ0FBckIsQ0FBUDtBQUNIOztBQUVELGFBQVMsWUFBVCxDQUFzQixFQUF0QixFQUEwQixLQUExQixFQUFnQztBQUM1QixZQUFJLE9BQU8sS0FBSyxLQUFMLENBQVg7QUFDQSxZQUFJLE9BQU8sS0FBSyxLQUFMLENBQVg7QUFDQSxlQUFPLEtBQUssRUFBTCxDQUFRLElBQVIsRUFBYyxJQUFkLEVBQXFCLENBQXJCLEVBQXdCLENBQXhCLEVBQTJCLENBQUMsSUFBNUIsRUFBbUMsSUFBbkMsRUFBeUMsQ0FBekMsRUFBNEMsQ0FBNUMsRUFBK0MsQ0FBL0MsRUFBbUQsQ0FBbkQsRUFBdUQsQ0FBdkQsRUFBMEQsQ0FBMUQsRUFBNkQsQ0FBN0QsRUFBZ0UsQ0FBaEUsRUFBbUUsQ0FBbkUsRUFBc0UsQ0FBdEUsRUFDRixFQURFLENBQ0MsQ0FERCxFQUNJLENBREosRUFDUSxDQURSLEVBQ1csQ0FEWCxFQUNjLEtBQUssRUFBTCxDQURkLEVBQ3lCLENBRHpCLEVBQzRCLENBRDVCLEVBQytCLENBRC9CLEVBQ2tDLENBRGxDLEVBQ3NDLENBRHRDLEVBQzBDLENBRDFDLEVBQzZDLENBRDdDLEVBQ2dELENBRGhELEVBQ21ELENBRG5ELEVBQ3NELENBRHRELEVBQ3lELENBRHpELEVBRUYsRUFGRSxDQUVDLElBRkQsRUFFTyxDQUFDLElBRlIsRUFFZSxDQUZmLEVBRWtCLENBRmxCLEVBRXFCLElBRnJCLEVBRTRCLElBRjVCLEVBRWtDLENBRmxDLEVBRXFDLENBRnJDLEVBRXdDLENBRnhDLEVBRTRDLENBRjVDLEVBRWdELENBRmhELEVBRW1ELENBRm5ELEVBRXNELENBRnRELEVBRXlELENBRnpELEVBRTRELENBRjVELEVBRStELENBRi9ELENBQVA7QUFHQTtBQUNIOztBQUVELGFBQVMsS0FBVCxDQUFlLEVBQWYsRUFBbUIsRUFBbkIsRUFBdUIsRUFBdkIsRUFBMkI7QUFDdkIsYUFBSyxNQUFNLEVBQU4sSUFBWSxDQUFaLEdBQWdCLEVBQXJCO0FBQ0EsWUFBRyxNQUFNLENBQU4sSUFBVyxNQUFNLENBQWpCLElBQXNCLE1BQU0sQ0FBL0IsRUFBaUM7QUFDN0IsbUJBQU8sSUFBUDtBQUNIO0FBQ0QsZUFBTyxLQUFLLEVBQUwsQ0FBUSxFQUFSLEVBQVksQ0FBWixFQUFlLENBQWYsRUFBa0IsQ0FBbEIsRUFBcUIsQ0FBckIsRUFBd0IsRUFBeEIsRUFBNEIsQ0FBNUIsRUFBK0IsQ0FBL0IsRUFBa0MsQ0FBbEMsRUFBcUMsQ0FBckMsRUFBd0MsRUFBeEMsRUFBNEMsQ0FBNUMsRUFBK0MsQ0FBL0MsRUFBa0QsQ0FBbEQsRUFBcUQsQ0FBckQsRUFBd0QsQ0FBeEQsQ0FBUDtBQUNIOztBQUVELGFBQVMsWUFBVCxDQUFzQixDQUF0QixFQUF5QixDQUF6QixFQUE0QixDQUE1QixFQUErQixDQUEvQixFQUFrQyxDQUFsQyxFQUFxQyxDQUFyQyxFQUF3QyxDQUF4QyxFQUEyQyxDQUEzQyxFQUE4QyxDQUE5QyxFQUFpRCxDQUFqRCxFQUFvRCxDQUFwRCxFQUF1RCxDQUF2RCxFQUEwRCxDQUExRCxFQUE2RCxDQUE3RCxFQUFnRSxDQUFoRSxFQUFtRSxDQUFuRSxFQUFzRTtBQUNsRSxhQUFLLEtBQUwsQ0FBVyxDQUFYLElBQWdCLENBQWhCO0FBQ0EsYUFBSyxLQUFMLENBQVcsQ0FBWCxJQUFnQixDQUFoQjtBQUNBLGFBQUssS0FBTCxDQUFXLENBQVgsSUFBZ0IsQ0FBaEI7QUFDQSxhQUFLLEtBQUwsQ0FBVyxDQUFYLElBQWdCLENBQWhCO0FBQ0EsYUFBSyxLQUFMLENBQVcsQ0FBWCxJQUFnQixDQUFoQjtBQUNBLGFBQUssS0FBTCxDQUFXLENBQVgsSUFBZ0IsQ0FBaEI7QUFDQSxhQUFLLEtBQUwsQ0FBVyxDQUFYLElBQWdCLENBQWhCO0FBQ0EsYUFBSyxLQUFMLENBQVcsQ0FBWCxJQUFnQixDQUFoQjtBQUNBLGFBQUssS0FBTCxDQUFXLENBQVgsSUFBZ0IsQ0FBaEI7QUFDQSxhQUFLLEtBQUwsQ0FBVyxDQUFYLElBQWdCLENBQWhCO0FBQ0EsYUFBSyxLQUFMLENBQVcsRUFBWCxJQUFpQixDQUFqQjtBQUNBLGFBQUssS0FBTCxDQUFXLEVBQVgsSUFBaUIsQ0FBakI7QUFDQSxhQUFLLEtBQUwsQ0FBVyxFQUFYLElBQWlCLENBQWpCO0FBQ0EsYUFBSyxLQUFMLENBQVcsRUFBWCxJQUFpQixDQUFqQjtBQUNBLGFBQUssS0FBTCxDQUFXLEVBQVgsSUFBaUIsQ0FBakI7QUFDQSxhQUFLLEtBQUwsQ0FBVyxFQUFYLElBQWlCLENBQWpCO0FBQ0EsZUFBTyxJQUFQO0FBQ0g7O0FBRUQsYUFBUyxTQUFULENBQW1CLEVBQW5CLEVBQXVCLEVBQXZCLEVBQTJCLEVBQTNCLEVBQStCO0FBQzNCLGFBQUssTUFBTSxDQUFYO0FBQ0EsWUFBRyxPQUFPLENBQVAsSUFBWSxPQUFPLENBQW5CLElBQXdCLE9BQU8sQ0FBbEMsRUFBb0M7QUFDaEMsbUJBQU8sS0FBSyxFQUFMLENBQVEsQ0FBUixFQUFVLENBQVYsRUFBWSxDQUFaLEVBQWMsQ0FBZCxFQUFnQixDQUFoQixFQUFrQixDQUFsQixFQUFvQixDQUFwQixFQUFzQixDQUF0QixFQUF3QixDQUF4QixFQUEwQixDQUExQixFQUE0QixDQUE1QixFQUE4QixDQUE5QixFQUFnQyxFQUFoQyxFQUFtQyxFQUFuQyxFQUFzQyxFQUF0QyxFQUF5QyxDQUF6QyxDQUFQO0FBQ0g7QUFDRCxlQUFPLElBQVA7QUFDSDs7QUFFRCxhQUFTLFNBQVQsQ0FBbUIsRUFBbkIsRUFBdUIsRUFBdkIsRUFBMkIsRUFBM0IsRUFBK0IsRUFBL0IsRUFBbUMsRUFBbkMsRUFBdUMsRUFBdkMsRUFBMkMsRUFBM0MsRUFBK0MsRUFBL0MsRUFBbUQsRUFBbkQsRUFBdUQsRUFBdkQsRUFBMkQsRUFBM0QsRUFBK0QsRUFBL0QsRUFBbUUsRUFBbkUsRUFBdUUsRUFBdkUsRUFBMkUsRUFBM0UsRUFBK0UsRUFBL0UsRUFBbUY7O0FBRS9FLFlBQUksS0FBSyxLQUFLLEtBQWQ7O0FBRUEsWUFBRyxPQUFPLENBQVAsSUFBWSxPQUFPLENBQW5CLElBQXdCLE9BQU8sQ0FBL0IsSUFBb0MsT0FBTyxDQUEzQyxJQUFnRCxPQUFPLENBQXZELElBQTRELE9BQU8sQ0FBbkUsSUFBd0UsT0FBTyxDQUEvRSxJQUFvRixPQUFPLENBQTNGLElBQWdHLE9BQU8sQ0FBdkcsSUFBNEcsT0FBTyxDQUFuSCxJQUF3SCxPQUFPLENBQS9ILElBQW9JLE9BQU8sQ0FBOUksRUFBZ0o7QUFDNUk7QUFDQTtBQUNJLGVBQUcsRUFBSCxJQUFTLEdBQUcsRUFBSCxJQUFTLEVBQVQsR0FBYyxHQUFHLEVBQUgsSUFBUyxFQUFoQztBQUNBLGVBQUcsRUFBSCxJQUFTLEdBQUcsRUFBSCxJQUFTLEVBQVQsR0FBYyxHQUFHLEVBQUgsSUFBUyxFQUFoQztBQUNBLGVBQUcsRUFBSCxJQUFTLEdBQUcsRUFBSCxJQUFTLEVBQVQsR0FBYyxHQUFHLEVBQUgsSUFBUyxFQUFoQztBQUNBLGVBQUcsRUFBSCxJQUFTLEdBQUcsRUFBSCxJQUFTLEVBQWxCO0FBQ0o7QUFDQSxpQkFBSyxtQkFBTCxHQUEyQixLQUEzQjtBQUNBLG1CQUFPLElBQVA7QUFDSDs7QUFFRCxZQUFJLEtBQUssR0FBRyxDQUFILENBQVQ7QUFDQSxZQUFJLEtBQUssR0FBRyxDQUFILENBQVQ7QUFDQSxZQUFJLEtBQUssR0FBRyxDQUFILENBQVQ7QUFDQSxZQUFJLEtBQUssR0FBRyxDQUFILENBQVQ7QUFDQSxZQUFJLEtBQUssR0FBRyxDQUFILENBQVQ7QUFDQSxZQUFJLEtBQUssR0FBRyxDQUFILENBQVQ7QUFDQSxZQUFJLEtBQUssR0FBRyxDQUFILENBQVQ7QUFDQSxZQUFJLEtBQUssR0FBRyxDQUFILENBQVQ7QUFDQSxZQUFJLEtBQUssR0FBRyxDQUFILENBQVQ7QUFDQSxZQUFJLEtBQUssR0FBRyxDQUFILENBQVQ7QUFDQSxZQUFJLEtBQUssR0FBRyxFQUFILENBQVQ7QUFDQSxZQUFJLEtBQUssR0FBRyxFQUFILENBQVQ7QUFDQSxZQUFJLEtBQUssR0FBRyxFQUFILENBQVQ7QUFDQSxZQUFJLEtBQUssR0FBRyxFQUFILENBQVQ7QUFDQSxZQUFJLEtBQUssR0FBRyxFQUFILENBQVQ7QUFDQSxZQUFJLEtBQUssR0FBRyxFQUFILENBQVQ7O0FBRUE7Ozs7O0FBS0EsV0FBRyxDQUFILElBQVEsS0FBSyxFQUFMLEdBQVUsS0FBSyxFQUFmLEdBQW9CLEtBQUssRUFBekIsR0FBOEIsS0FBSyxFQUEzQztBQUNBLFdBQUcsQ0FBSCxJQUFRLEtBQUssRUFBTCxHQUFVLEtBQUssRUFBZixHQUFvQixLQUFLLEVBQXpCLEdBQThCLEtBQUssRUFBM0M7QUFDQSxXQUFHLENBQUgsSUFBUSxLQUFLLEVBQUwsR0FBVSxLQUFLLEVBQWYsR0FBb0IsS0FBSyxFQUF6QixHQUE4QixLQUFLLEVBQTNDO0FBQ0EsV0FBRyxDQUFILElBQVEsS0FBSyxFQUFMLEdBQVUsS0FBSyxFQUFmLEdBQW9CLEtBQUssRUFBekIsR0FBOEIsS0FBSyxFQUEzQzs7QUFFQSxXQUFHLENBQUgsSUFBUSxLQUFLLEVBQUwsR0FBVSxLQUFLLEVBQWYsR0FBb0IsS0FBSyxFQUF6QixHQUE4QixLQUFLLEVBQTNDO0FBQ0EsV0FBRyxDQUFILElBQVEsS0FBSyxFQUFMLEdBQVUsS0FBSyxFQUFmLEdBQW9CLEtBQUssRUFBekIsR0FBOEIsS0FBSyxFQUEzQztBQUNBLFdBQUcsQ0FBSCxJQUFRLEtBQUssRUFBTCxHQUFVLEtBQUssRUFBZixHQUFvQixLQUFLLEVBQXpCLEdBQThCLEtBQUssRUFBM0M7QUFDQSxXQUFHLENBQUgsSUFBUSxLQUFLLEVBQUwsR0FBVSxLQUFLLEVBQWYsR0FBb0IsS0FBSyxFQUF6QixHQUE4QixLQUFLLEVBQTNDOztBQUVBLFdBQUcsQ0FBSCxJQUFRLEtBQUssRUFBTCxHQUFVLEtBQUssRUFBZixHQUFvQixLQUFLLEVBQXpCLEdBQThCLEtBQUssRUFBM0M7QUFDQSxXQUFHLENBQUgsSUFBUSxLQUFLLEVBQUwsR0FBVSxLQUFLLEVBQWYsR0FBb0IsS0FBSyxFQUF6QixHQUE4QixLQUFLLEVBQTNDO0FBQ0EsV0FBRyxFQUFILElBQVMsS0FBSyxFQUFMLEdBQVUsS0FBSyxFQUFmLEdBQW9CLEtBQUssRUFBekIsR0FBOEIsS0FBSyxFQUE1QztBQUNBLFdBQUcsRUFBSCxJQUFTLEtBQUssRUFBTCxHQUFVLEtBQUssRUFBZixHQUFvQixLQUFLLEVBQXpCLEdBQThCLEtBQUssRUFBNUM7O0FBRUEsV0FBRyxFQUFILElBQVMsS0FBSyxFQUFMLEdBQVUsS0FBSyxFQUFmLEdBQW9CLEtBQUssRUFBekIsR0FBOEIsS0FBSyxFQUE1QztBQUNBLFdBQUcsRUFBSCxJQUFTLEtBQUssRUFBTCxHQUFVLEtBQUssRUFBZixHQUFvQixLQUFLLEVBQXpCLEdBQThCLEtBQUssRUFBNUM7QUFDQSxXQUFHLEVBQUgsSUFBUyxLQUFLLEVBQUwsR0FBVSxLQUFLLEVBQWYsR0FBb0IsS0FBSyxFQUF6QixHQUE4QixLQUFLLEVBQTVDO0FBQ0EsV0FBRyxFQUFILElBQVMsS0FBSyxFQUFMLEdBQVUsS0FBSyxFQUFmLEdBQW9CLEtBQUssRUFBekIsR0FBOEIsS0FBSyxFQUE1Qzs7QUFFQSxhQUFLLG1CQUFMLEdBQTJCLEtBQTNCO0FBQ0EsZUFBTyxJQUFQO0FBQ0g7O0FBRUQsYUFBUyxVQUFULEdBQXNCO0FBQ2xCLFlBQUcsQ0FBQyxLQUFLLG1CQUFULEVBQTZCO0FBQ3pCLGlCQUFLLFNBQUwsR0FBaUIsRUFBRSxLQUFLLEtBQUwsQ0FBVyxDQUFYLE1BQWtCLENBQWxCLElBQXVCLEtBQUssS0FBTCxDQUFXLENBQVgsTUFBa0IsQ0FBekMsSUFBOEMsS0FBSyxLQUFMLENBQVcsQ0FBWCxNQUFrQixDQUFoRSxJQUFxRSxLQUFLLEtBQUwsQ0FBVyxDQUFYLE1BQWtCLENBQXZGLElBQTRGLEtBQUssS0FBTCxDQUFXLENBQVgsTUFBa0IsQ0FBOUcsSUFBbUgsS0FBSyxLQUFMLENBQVcsQ0FBWCxNQUFrQixDQUFySSxJQUEwSSxLQUFLLEtBQUwsQ0FBVyxDQUFYLE1BQWtCLENBQTVKLElBQWlLLEtBQUssS0FBTCxDQUFXLENBQVgsTUFBa0IsQ0FBbkwsSUFBd0wsS0FBSyxLQUFMLENBQVcsQ0FBWCxNQUFrQixDQUExTSxJQUErTSxLQUFLLEtBQUwsQ0FBVyxDQUFYLE1BQWtCLENBQWpPLElBQXNPLEtBQUssS0FBTCxDQUFXLEVBQVgsTUFBbUIsQ0FBelAsSUFBOFAsS0FBSyxLQUFMLENBQVcsRUFBWCxNQUFtQixDQUFqUixJQUFzUixLQUFLLEtBQUwsQ0FBVyxFQUFYLE1BQW1CLENBQXpTLElBQThTLEtBQUssS0FBTCxDQUFXLEVBQVgsTUFBbUIsQ0FBalUsSUFBc1UsS0FBSyxLQUFMLENBQVcsRUFBWCxNQUFtQixDQUF6VixJQUE4VixLQUFLLEtBQUwsQ0FBVyxFQUFYLE1BQW1CLENBQW5YLENBQWpCO0FBQ0EsaUJBQUssbUJBQUwsR0FBMkIsSUFBM0I7QUFDSDtBQUNELGVBQU8sS0FBSyxTQUFaO0FBQ0g7O0FBRUQsYUFBUyxNQUFULENBQWdCLElBQWhCLEVBQXFCO0FBQ2pCLFlBQUksSUFBSSxDQUFSO0FBQ0EsZUFBTyxJQUFJLEVBQVgsRUFBZTtBQUNYLGdCQUFHLEtBQUssS0FBTCxDQUFXLENBQVgsTUFBa0IsS0FBSyxLQUFMLENBQVcsQ0FBWCxDQUFyQixFQUFvQztBQUNoQyx1QkFBTyxLQUFQO0FBQ0g7QUFDRCxpQkFBRyxDQUFIO0FBQ0g7QUFDRCxlQUFPLElBQVA7QUFDSDs7QUFFRCxhQUFTLEtBQVQsQ0FBZSxJQUFmLEVBQW9CO0FBQ2hCLFlBQUksQ0FBSjtBQUNBLGFBQUksSUFBRSxDQUFOLEVBQVEsSUFBRSxFQUFWLEVBQWEsS0FBRyxDQUFoQixFQUFrQjtBQUNkLGlCQUFLLEtBQUwsQ0FBVyxDQUFYLElBQWdCLEtBQUssS0FBTCxDQUFXLENBQVgsQ0FBaEI7QUFDSDtBQUNKOztBQUVELGFBQVMsY0FBVCxDQUF3QixLQUF4QixFQUE4QjtBQUMxQixZQUFJLENBQUo7QUFDQSxhQUFJLElBQUUsQ0FBTixFQUFRLElBQUUsRUFBVixFQUFhLEtBQUcsQ0FBaEIsRUFBa0I7QUFDZCxpQkFBSyxLQUFMLENBQVcsQ0FBWCxJQUFnQixNQUFNLENBQU4sQ0FBaEI7QUFDSDtBQUNKOztBQUVELGFBQVMsWUFBVCxDQUFzQixDQUF0QixFQUF5QixDQUF6QixFQUE0QixDQUE1QixFQUErQjs7QUFFM0IsZUFBTztBQUNILGVBQUcsSUFBSSxLQUFLLEtBQUwsQ0FBVyxDQUFYLENBQUosR0FBb0IsSUFBSSxLQUFLLEtBQUwsQ0FBVyxDQUFYLENBQXhCLEdBQXdDLElBQUksS0FBSyxLQUFMLENBQVcsQ0FBWCxDQUE1QyxHQUE0RCxLQUFLLEtBQUwsQ0FBVyxFQUFYLENBRDVEO0FBRUgsZUFBRyxJQUFJLEtBQUssS0FBTCxDQUFXLENBQVgsQ0FBSixHQUFvQixJQUFJLEtBQUssS0FBTCxDQUFXLENBQVgsQ0FBeEIsR0FBd0MsSUFBSSxLQUFLLEtBQUwsQ0FBVyxDQUFYLENBQTVDLEdBQTRELEtBQUssS0FBTCxDQUFXLEVBQVgsQ0FGNUQ7QUFHSCxlQUFHLElBQUksS0FBSyxLQUFMLENBQVcsQ0FBWCxDQUFKLEdBQW9CLElBQUksS0FBSyxLQUFMLENBQVcsQ0FBWCxDQUF4QixHQUF3QyxJQUFJLEtBQUssS0FBTCxDQUFXLEVBQVgsQ0FBNUMsR0FBNkQsS0FBSyxLQUFMLENBQVcsRUFBWDtBQUg3RCxTQUFQO0FBS0E7Ozs7QUFJSDtBQUNELGFBQVMsUUFBVCxDQUFrQixDQUFsQixFQUFxQixDQUFyQixFQUF3QixDQUF4QixFQUEyQjtBQUN2QixlQUFPLElBQUksS0FBSyxLQUFMLENBQVcsQ0FBWCxDQUFKLEdBQW9CLElBQUksS0FBSyxLQUFMLENBQVcsQ0FBWCxDQUF4QixHQUF3QyxJQUFJLEtBQUssS0FBTCxDQUFXLENBQVgsQ0FBNUMsR0FBNEQsS0FBSyxLQUFMLENBQVcsRUFBWCxDQUFuRTtBQUNIO0FBQ0QsYUFBUyxRQUFULENBQWtCLENBQWxCLEVBQXFCLENBQXJCLEVBQXdCLENBQXhCLEVBQTJCO0FBQ3ZCLGVBQU8sSUFBSSxLQUFLLEtBQUwsQ0FBVyxDQUFYLENBQUosR0FBb0IsSUFBSSxLQUFLLEtBQUwsQ0FBVyxDQUFYLENBQXhCLEdBQXdDLElBQUksS0FBSyxLQUFMLENBQVcsQ0FBWCxDQUE1QyxHQUE0RCxLQUFLLEtBQUwsQ0FBVyxFQUFYLENBQW5FO0FBQ0g7QUFDRCxhQUFTLFFBQVQsQ0FBa0IsQ0FBbEIsRUFBcUIsQ0FBckIsRUFBd0IsQ0FBeEIsRUFBMkI7QUFDdkIsZUFBTyxJQUFJLEtBQUssS0FBTCxDQUFXLENBQVgsQ0FBSixHQUFvQixJQUFJLEtBQUssS0FBTCxDQUFXLENBQVgsQ0FBeEIsR0FBd0MsSUFBSSxLQUFLLEtBQUwsQ0FBVyxFQUFYLENBQTVDLEdBQTZELEtBQUssS0FBTCxDQUFXLEVBQVgsQ0FBcEU7QUFDSDs7QUFFRCxhQUFTLFlBQVQsQ0FBc0IsRUFBdEIsRUFBMEI7QUFDdEIsWUFBSSxjQUFjLEtBQUssS0FBTCxDQUFXLENBQVgsSUFBZ0IsS0FBSyxLQUFMLENBQVcsQ0FBWCxDQUFoQixHQUFnQyxLQUFLLEtBQUwsQ0FBVyxDQUFYLElBQWdCLEtBQUssS0FBTCxDQUFXLENBQVgsQ0FBbEU7QUFDQSxZQUFJLElBQUksS0FBSyxLQUFMLENBQVcsQ0FBWCxJQUFjLFdBQXRCO0FBQ0EsWUFBSSxJQUFJLENBQUUsS0FBSyxLQUFMLENBQVcsQ0FBWCxDQUFGLEdBQWdCLFdBQXhCO0FBQ0EsWUFBSSxJQUFJLENBQUUsS0FBSyxLQUFMLENBQVcsQ0FBWCxDQUFGLEdBQWdCLFdBQXhCO0FBQ0EsWUFBSSxJQUFJLEtBQUssS0FBTCxDQUFXLENBQVgsSUFBYyxXQUF0QjtBQUNBLFlBQUksSUFBSSxDQUFDLEtBQUssS0FBTCxDQUFXLENBQVgsSUFBZ0IsS0FBSyxLQUFMLENBQVcsRUFBWCxDQUFoQixHQUFpQyxLQUFLLEtBQUwsQ0FBVyxDQUFYLElBQWdCLEtBQUssS0FBTCxDQUFXLEVBQVgsQ0FBbEQsSUFBa0UsV0FBMUU7QUFDQSxZQUFJLElBQUksRUFBRyxLQUFLLEtBQUwsQ0FBVyxDQUFYLElBQWdCLEtBQUssS0FBTCxDQUFXLEVBQVgsQ0FBaEIsR0FBaUMsS0FBSyxLQUFMLENBQVcsQ0FBWCxJQUFnQixLQUFLLEtBQUwsQ0FBVyxFQUFYLENBQXBELElBQW9FLFdBQTVFO0FBQ0EsZUFBTyxDQUFDLEdBQUcsQ0FBSCxJQUFRLENBQVIsR0FBWSxHQUFHLENBQUgsSUFBUSxDQUFwQixHQUF3QixDQUF6QixFQUE0QixHQUFHLENBQUgsSUFBUSxDQUFSLEdBQVksR0FBRyxDQUFILElBQVEsQ0FBcEIsR0FBd0IsQ0FBcEQsRUFBdUQsQ0FBdkQsQ0FBUDtBQUNIOztBQUVELGFBQVMsYUFBVCxDQUF1QixHQUF2QixFQUEyQjtBQUN2QixZQUFJLENBQUo7QUFBQSxZQUFPLE1BQU0sSUFBSSxNQUFqQjtBQUFBLFlBQXlCLFNBQVMsRUFBbEM7QUFDQSxhQUFJLElBQUUsQ0FBTixFQUFRLElBQUUsR0FBVixFQUFjLEtBQUcsQ0FBakIsRUFBbUI7QUFDZixtQkFBTyxDQUFQLElBQVksYUFBYSxJQUFJLENBQUosQ0FBYixDQUFaO0FBQ0g7QUFDRCxlQUFPLE1BQVA7QUFDSDs7QUFFRCxhQUFTLG1CQUFULENBQTZCLEdBQTdCLEVBQWtDLEdBQWxDLEVBQXVDLEdBQXZDLEVBQTRDO0FBQ3hDLFlBQUksTUFBTSxpQkFBaUIsU0FBakIsRUFBNEIsQ0FBNUIsQ0FBVjtBQUNBLFlBQUcsS0FBSyxVQUFMLEVBQUgsRUFBc0I7QUFDbEIsZ0JBQUksQ0FBSixJQUFTLElBQUksQ0FBSixDQUFUO0FBQ0EsZ0JBQUksQ0FBSixJQUFTLElBQUksQ0FBSixDQUFUO0FBQ0EsZ0JBQUksQ0FBSixJQUFTLElBQUksQ0FBSixDQUFUO0FBQ0EsZ0JBQUksQ0FBSixJQUFTLElBQUksQ0FBSixDQUFUO0FBQ0EsZ0JBQUksQ0FBSixJQUFTLElBQUksQ0FBSixDQUFUO0FBQ0EsZ0JBQUksQ0FBSixJQUFTLElBQUksQ0FBSixDQUFUO0FBQ0gsU0FQRCxNQU9PO0FBQ0gsZ0JBQUksS0FBSyxLQUFLLEtBQUwsQ0FBVyxDQUFYLENBQVQ7QUFBQSxnQkFBd0IsS0FBSyxLQUFLLEtBQUwsQ0FBVyxDQUFYLENBQTdCO0FBQUEsZ0JBQTRDLEtBQUssS0FBSyxLQUFMLENBQVcsQ0FBWCxDQUFqRDtBQUFBLGdCQUFnRSxLQUFLLEtBQUssS0FBTCxDQUFXLENBQVgsQ0FBckU7QUFBQSxnQkFBb0YsTUFBTSxLQUFLLEtBQUwsQ0FBVyxFQUFYLENBQTFGO0FBQUEsZ0JBQTBHLE1BQU0sS0FBSyxLQUFMLENBQVcsRUFBWCxDQUFoSDtBQUNBLGdCQUFJLENBQUosSUFBUyxJQUFJLENBQUosSUFBUyxFQUFULEdBQWMsSUFBSSxDQUFKLElBQVMsRUFBdkIsR0FBNEIsR0FBckM7QUFDQSxnQkFBSSxDQUFKLElBQVMsSUFBSSxDQUFKLElBQVMsRUFBVCxHQUFjLElBQUksQ0FBSixJQUFTLEVBQXZCLEdBQTRCLEdBQXJDO0FBQ0EsZ0JBQUksQ0FBSixJQUFTLElBQUksQ0FBSixJQUFTLEVBQVQsR0FBYyxJQUFJLENBQUosSUFBUyxFQUF2QixHQUE0QixHQUFyQztBQUNBLGdCQUFJLENBQUosSUFBUyxJQUFJLENBQUosSUFBUyxFQUFULEdBQWMsSUFBSSxDQUFKLElBQVMsRUFBdkIsR0FBNEIsR0FBckM7QUFDQSxnQkFBSSxDQUFKLElBQVMsSUFBSSxDQUFKLElBQVMsRUFBVCxHQUFjLElBQUksQ0FBSixJQUFTLEVBQXZCLEdBQTRCLEdBQXJDO0FBQ0EsZ0JBQUksQ0FBSixJQUFTLElBQUksQ0FBSixJQUFTLEVBQVQsR0FBYyxJQUFJLENBQUosSUFBUyxFQUF2QixHQUE0QixHQUFyQztBQUNIO0FBQ0QsZUFBTyxHQUFQO0FBQ0g7O0FBRUQsYUFBUyxpQkFBVCxDQUEyQixDQUEzQixFQUE2QixDQUE3QixFQUErQixDQUEvQixFQUFpQztBQUM3QixZQUFJLEdBQUo7QUFDQSxZQUFHLEtBQUssVUFBTCxFQUFILEVBQXNCO0FBQ2xCLGtCQUFNLENBQUMsQ0FBRCxFQUFHLENBQUgsRUFBSyxDQUFMLENBQU47QUFDSCxTQUZELE1BRU87QUFDSCxrQkFBTSxDQUFDLElBQUksS0FBSyxLQUFMLENBQVcsQ0FBWCxDQUFKLEdBQW9CLElBQUksS0FBSyxLQUFMLENBQVcsQ0FBWCxDQUF4QixHQUF3QyxJQUFJLEtBQUssS0FBTCxDQUFXLENBQVgsQ0FBNUMsR0FBNEQsS0FBSyxLQUFMLENBQVcsRUFBWCxDQUE3RCxFQUE0RSxJQUFJLEtBQUssS0FBTCxDQUFXLENBQVgsQ0FBSixHQUFvQixJQUFJLEtBQUssS0FBTCxDQUFXLENBQVgsQ0FBeEIsR0FBd0MsSUFBSSxLQUFLLEtBQUwsQ0FBVyxDQUFYLENBQTVDLEdBQTRELEtBQUssS0FBTCxDQUFXLEVBQVgsQ0FBeEksRUFBdUosSUFBSSxLQUFLLEtBQUwsQ0FBVyxDQUFYLENBQUosR0FBb0IsSUFBSSxLQUFLLEtBQUwsQ0FBVyxDQUFYLENBQXhCLEdBQXdDLElBQUksS0FBSyxLQUFMLENBQVcsRUFBWCxDQUE1QyxHQUE2RCxLQUFLLEtBQUwsQ0FBVyxFQUFYLENBQXBOLENBQU47QUFDSDtBQUNELGVBQU8sR0FBUDtBQUNIOztBQUVELGFBQVMsdUJBQVQsQ0FBaUMsQ0FBakMsRUFBb0MsQ0FBcEMsRUFBdUM7QUFDbkMsWUFBRyxLQUFLLFVBQUwsRUFBSCxFQUFzQjtBQUNsQixtQkFBTyxJQUFJLEdBQUosR0FBVSxDQUFqQjtBQUNIO0FBQ0QsZUFBUSxJQUFJLEtBQUssS0FBTCxDQUFXLENBQVgsQ0FBSixHQUFvQixJQUFJLEtBQUssS0FBTCxDQUFXLENBQVgsQ0FBeEIsR0FBd0MsS0FBSyxLQUFMLENBQVcsRUFBWCxDQUF6QyxHQUF5RCxHQUF6RCxJQUE4RCxJQUFJLEtBQUssS0FBTCxDQUFXLENBQVgsQ0FBSixHQUFvQixJQUFJLEtBQUssS0FBTCxDQUFXLENBQVgsQ0FBeEIsR0FBd0MsS0FBSyxLQUFMLENBQVcsRUFBWCxDQUF0RyxDQUFQO0FBQ0g7O0FBRUQsYUFBUyxLQUFULEdBQWlCO0FBQ2I7QUFDQTs7O0FBR0EsWUFBSSxJQUFJLENBQVI7QUFDQSxZQUFJLFFBQVEsS0FBSyxLQUFqQjtBQUNBLFlBQUksV0FBVyxXQUFmO0FBQ0EsWUFBSSxJQUFJLEtBQVI7QUFDQSxlQUFNLElBQUUsRUFBUixFQUFXO0FBQ1Asd0JBQVksS0FBSyxNQUFNLENBQU4sSUFBUyxDQUFkLElBQWlCLENBQTdCO0FBQ0Esd0JBQVksTUFBTSxFQUFOLEdBQVcsR0FBWCxHQUFlLEdBQTNCO0FBQ0EsaUJBQUssQ0FBTDtBQUNIO0FBQ0QsZUFBTyxRQUFQO0FBQ0g7O0FBRUQsYUFBUyxPQUFULEdBQW1CO0FBQ2Y7QUFDQTs7O0FBR0EsWUFBSSxJQUFJLEtBQVI7QUFDQSxZQUFJLFFBQVEsS0FBSyxLQUFqQjtBQUNBLGVBQU8sWUFBWSxLQUFLLE1BQU0sQ0FBTixJQUFTLENBQWQsSUFBaUIsQ0FBN0IsR0FBaUMsR0FBakMsR0FBdUMsS0FBSyxNQUFNLENBQU4sSUFBUyxDQUFkLElBQWlCLENBQXhELEdBQTRELEdBQTVELEdBQWtFLEtBQUssTUFBTSxDQUFOLElBQVMsQ0FBZCxJQUFpQixDQUFuRixHQUF1RixHQUF2RixHQUE2RixLQUFLLE1BQU0sQ0FBTixJQUFTLENBQWQsSUFBaUIsQ0FBOUcsR0FBa0gsR0FBbEgsR0FBd0gsS0FBSyxNQUFNLEVBQU4sSUFBVSxDQUFmLElBQWtCLENBQTFJLEdBQThJLEdBQTlJLEdBQW9KLEtBQUssTUFBTSxFQUFOLElBQVUsQ0FBZixJQUFrQixDQUF0SyxHQUEwSyxHQUFqTDtBQUNIOztBQUVELGFBQVMsY0FBVCxHQUF5QjtBQUNyQixhQUFLLEtBQUwsR0FBYSxLQUFiO0FBQ0EsYUFBSyxNQUFMLEdBQWMsTUFBZDtBQUNBLGFBQUssT0FBTCxHQUFlLE9BQWY7QUFDQSxhQUFLLE9BQUwsR0FBZSxPQUFmO0FBQ0EsYUFBSyxPQUFMLEdBQWUsT0FBZjtBQUNBLGFBQUssSUFBTCxHQUFZLElBQVo7QUFDQSxhQUFLLFlBQUwsR0FBb0IsWUFBcEI7QUFDQSxhQUFLLEtBQUwsR0FBYSxLQUFiO0FBQ0EsYUFBSyxLQUFMLEdBQWEsS0FBYjtBQUNBLGFBQUssWUFBTCxHQUFvQixZQUFwQjtBQUNBLGFBQUssU0FBTCxHQUFpQixTQUFqQjtBQUNBLGFBQUssU0FBTCxHQUFpQixTQUFqQjtBQUNBLGFBQUssWUFBTCxHQUFvQixZQUFwQjtBQUNBLGFBQUssUUFBTCxHQUFnQixRQUFoQjtBQUNBLGFBQUssUUFBTCxHQUFnQixRQUFoQjtBQUNBLGFBQUssUUFBTCxHQUFnQixRQUFoQjtBQUNBLGFBQUssaUJBQUwsR0FBeUIsaUJBQXpCO0FBQ0EsYUFBSyxtQkFBTCxHQUEyQixtQkFBM0I7QUFDQSxhQUFLLHVCQUFMLEdBQStCLHVCQUEvQjtBQUNBLGFBQUssS0FBTCxHQUFhLEtBQWI7QUFDQSxhQUFLLE9BQUwsR0FBZSxPQUFmO0FBQ0EsYUFBSyxLQUFMLEdBQWEsS0FBYjtBQUNBLGFBQUssY0FBTCxHQUFzQixjQUF0QjtBQUNBLGFBQUssTUFBTCxHQUFjLE1BQWQ7QUFDQSxhQUFLLGFBQUwsR0FBcUIsYUFBckI7QUFDQSxhQUFLLFlBQUwsR0FBb0IsWUFBcEI7QUFDQSxhQUFLLEVBQUwsR0FBVSxLQUFLLFNBQWY7QUFDQSxhQUFLLFVBQUwsR0FBa0IsVUFBbEI7QUFDQSxhQUFLLFNBQUwsR0FBaUIsSUFBakI7QUFDQSxhQUFLLG1CQUFMLEdBQTJCLEtBQTNCOztBQUVBLGFBQUssS0FBTCxHQUFhLGlCQUFpQixTQUFqQixFQUE0QixFQUE1QixDQUFiO0FBQ0EsYUFBSyxLQUFMO0FBQ0g7O0FBRUQsV0FBTyxZQUFXO0FBQ2QsZUFBTyxJQUFJLGNBQUosRUFBUDtBQUNILEtBRkQ7QUFHSCxDQXBXYSxFQUFkOztBQXNXQSxPQUFPLE9BQVAsR0FBaUIsTUFBakI7Ozs7O0FDcllBLElBQUksbUJBQW9CLFlBQVU7QUFDakMsVUFBUyxrQkFBVCxDQUE0QixJQUE1QixFQUFrQyxHQUFsQyxFQUFzQztBQUNyQyxNQUFJLElBQUksQ0FBUjtBQUFBLE1BQVcsTUFBTSxFQUFqQjtBQUFBLE1BQXFCLEtBQXJCO0FBQ0EsVUFBTyxJQUFQO0FBQ0MsUUFBSyxPQUFMO0FBQ0EsUUFBSyxRQUFMO0FBQ0MsWUFBUSxDQUFSO0FBQ0E7QUFDRDtBQUNDLFlBQVEsR0FBUjtBQUNBO0FBUEY7QUFTQSxPQUFJLElBQUksQ0FBUixFQUFXLElBQUksR0FBZixFQUFvQixLQUFLLENBQXpCLEVBQTRCO0FBQzNCLE9BQUksSUFBSixDQUFTLEtBQVQ7QUFDQTtBQUNELFNBQU8sR0FBUDtBQUNBO0FBQ0QsVUFBUyxnQkFBVCxDQUEwQixJQUExQixFQUFnQyxHQUFoQyxFQUFvQztBQUNuQyxNQUFHLFNBQVMsU0FBWixFQUF1QjtBQUN0QixVQUFPLElBQUksWUFBSixDQUFpQixHQUFqQixDQUFQO0FBQ0EsR0FGRCxNQUVPLElBQUcsU0FBUyxPQUFaLEVBQXFCO0FBQzNCLFVBQU8sSUFBSSxVQUFKLENBQWUsR0FBZixDQUFQO0FBQ0EsR0FGTSxNQUVBLElBQUcsU0FBUyxRQUFaLEVBQXNCO0FBQzVCLFVBQU8sSUFBSSxpQkFBSixDQUFzQixHQUF0QixDQUFQO0FBQ0E7QUFDRDtBQUNELEtBQUcsT0FBTyxpQkFBUCxLQUE2QixVQUE3QixJQUEyQyxPQUFPLFlBQVAsS0FBd0IsVUFBdEUsRUFBa0Y7QUFDakYsU0FBTyxnQkFBUDtBQUNBLEVBRkQsTUFFTztBQUNOLFNBQU8sa0JBQVA7QUFDQTtBQUNELENBL0J1QixFQUF4Qjs7QUFpQ0EsT0FBTyxPQUFQLEdBQWlCLGdCQUFqQjs7Ozs7QUNqQ0EsSUFBSSxnQkFBZ0IsUUFBUSwyQkFBUixDQUFwQjs7QUFFQSxTQUFTLGtCQUFULENBQTRCLElBQTVCLEVBQWtDO0FBQ2pDLFFBQU8sT0FBTyxNQUFQLENBQWMsRUFBZCxFQUFrQixjQUFjLElBQWQsQ0FBbEIsQ0FBUDtBQUNBOztBQUVELE9BQU8sT0FBUCxHQUFpQjtBQUNoQixxQkFBcUI7QUFETCxDQUFqQjs7Ozs7QUNOQSxJQUFJLGlCQUFpQixRQUFRLDJCQUFSLENBQXJCO0FBQ0EsSUFBSSxjQUFjLFFBQVEsc0JBQVIsQ0FBbEI7O0FBRUEsU0FBUyxXQUFULENBQXFCLFFBQXJCLEVBQStCLFNBQS9CLEVBQTBDOztBQUV6QyxVQUFTLFVBQVQsR0FBc0I7QUFDckIsU0FBTyxTQUFTLE1BQWhCO0FBQ0E7O0FBRUQsVUFBUyxrQkFBVCxDQUE0QixRQUE1QixFQUFzQyxJQUF0QyxFQUE0QztBQUMzQyxTQUFPLFNBQVMsTUFBVCxDQUFnQixVQUFTLE9BQVQsRUFBa0I7QUFDeEMsVUFBTyxRQUFRLGNBQVIsR0FBeUIsSUFBekIsQ0FBOEIsRUFBOUIsS0FBcUMsWUFBWSxJQUFaLENBQTVDO0FBQ0EsR0FGTSxDQUFQO0FBR0E7O0FBRUQsVUFBUyxrQkFBVCxDQUE0QixRQUE1QixFQUFzQyxJQUF0QyxFQUE0QztBQUMzQyxTQUFPLFNBQVMsTUFBVCxDQUFnQixVQUFTLE9BQVQsRUFBa0I7QUFDeEMsVUFBTyxRQUFRLGNBQVIsR0FBeUIsSUFBekIsQ0FBOEIsRUFBOUIsS0FBcUMsSUFBNUM7QUFDQSxHQUZNLENBQVA7QUFHQTs7QUFFRCxVQUFTLHNCQUFULENBQWdDLFFBQWhDLEVBQTBDLElBQTFDLEVBQWdEO0FBQy9DLFNBQU8sU0FBUyxNQUFULENBQWdCLFVBQVMsT0FBVCxFQUFrQjtBQUN4QyxPQUFHLFFBQVEsV0FBUixDQUFvQixJQUFwQixDQUFILEVBQThCO0FBQzdCLFdBQU8sUUFBUSxXQUFSLENBQW9CLElBQXBCLENBQVA7QUFDQTtBQUNELFVBQU8sS0FBUDtBQUNBLEdBTE0sQ0FBUDtBQU1BOztBQUVELFVBQVMsZUFBVCxDQUF5QixRQUF6QixFQUFtQztBQUNsQyxTQUFPLFlBQVksbUJBQW1CLFFBQW5CLEVBQTZCLFFBQTdCLENBQVosRUFBb0QsT0FBcEQsQ0FBUDtBQUNBOztBQUVELFVBQVMsZUFBVCxDQUF5QixRQUF6QixFQUFtQztBQUNsQyxTQUFPLFlBQVksbUJBQW1CLFFBQW5CLEVBQTZCLFFBQTdCLENBQVosRUFBb0QsT0FBcEQsQ0FBUDtBQUNBOztBQUVELFVBQVMsdUJBQVQsQ0FBaUMsUUFBakMsRUFBMkM7QUFDMUMsU0FBTyxZQUFZLFNBQVMsTUFBVCxDQUFnQixVQUFTLE9BQVQsRUFBa0I7QUFDcEQsVUFBTyxRQUFRLFdBQVIsQ0FBb0IsUUFBcEIsQ0FBUDtBQUNBLEdBRmtCLEVBRWhCLEdBRmdCLENBRVosVUFBUyxPQUFULEVBQWtCO0FBQ3hCLFVBQU8sUUFBUSxXQUFSLENBQW9CLFFBQXBCLENBQVA7QUFDQSxHQUprQixDQUFaLEVBSUgsVUFKRyxDQUFQO0FBS0E7O0FBRUQsVUFBUyxnQkFBVCxDQUEwQixRQUExQixFQUFvQztBQUNuQyxNQUFJLFNBQVMsdUJBQXVCLFFBQXZCLEVBQWlDLFFBQWpDLENBQWI7QUFDQSxNQUFJLGFBQWEsT0FBTyxHQUFQLENBQVcsVUFBUyxPQUFULEVBQWlCO0FBQzVDLFVBQU8sUUFBUSxXQUFSLENBQW9CLFFBQXBCLENBQVA7QUFDQSxHQUZnQixDQUFqQjtBQUdBLFNBQU8sWUFBWSxVQUFaLEVBQXdCLFVBQXhCLENBQVA7QUFDQTs7QUFFRCxVQUFTLFVBQVQsQ0FBb0IsWUFBcEIsRUFBa0M7QUFDakMsTUFBSSxjQUFjLGVBQWUsWUFBZixDQUFsQjtBQUNBLE1BQUksV0FBVyxZQUFZLFFBQTNCO0FBQ0EsTUFBSSxXQUFKLEVBQWlCLFdBQWpCLEVBQThCLGFBQTlCO0FBQ0EsTUFBSSxjQUFjLFVBQWQsSUFBNEIsY0FBYyxPQUE5QyxFQUF1RDtBQUN0RCxpQkFBYyxnQkFBZ0IsUUFBaEIsQ0FBZDtBQUNBLGlCQUFjLGdCQUFnQixRQUFoQixDQUFkO0FBQ0EsT0FBSSxZQUFZLE1BQVosS0FBdUIsQ0FBdkIsSUFBNEIsWUFBWSxNQUFaLEtBQXVCLENBQXZELEVBQTBEO0FBQ3pELG9CQUFnQixpQkFBaUIsUUFBakIsQ0FBaEI7QUFDQSxJQUZELE1BRU87QUFDTixvQkFBZ0IsWUFBWSxNQUFaLENBQW1CLFdBQW5CLENBQWhCO0FBQ0E7QUFDRCxPQUFJLFlBQVksWUFBaEIsRUFBOEI7QUFDN0IsV0FBTyxjQUFjLFVBQWQsQ0FBeUIsWUFBWSxZQUFyQyxDQUFQO0FBQ0EsSUFGRCxNQUVPO0FBQ04sV0FBTyxhQUFQO0FBQ0E7QUFDRCxHQWJELE1BYU8sSUFBRyxjQUFjLFVBQWpCLEVBQTZCO0FBQ25DLG1CQUFnQix3QkFBd0IsUUFBeEIsQ0FBaEI7QUFDQSxPQUFJLFlBQVksWUFBaEIsRUFBOEI7QUFDN0IsV0FBTyxjQUFjLFVBQWQsQ0FBeUIsWUFBWSxZQUFyQyxDQUFQO0FBQ0EsSUFGRCxNQUVPO0FBQ04sV0FBTyxhQUFQO0FBQ0E7QUFDRDtBQUNEOztBQUVELFVBQVMsTUFBVCxDQUFnQixLQUFoQixFQUF1QjtBQUN0QixNQUFJLGdCQUFnQixNQUFNLFdBQU4sRUFBcEI7QUFDQSxTQUFPLFlBQVksU0FBUyxNQUFULENBQWdCLGFBQWhCLENBQVosRUFBNEMsU0FBNUMsQ0FBUDtBQUNBOztBQUVELFVBQVMsV0FBVCxHQUF1QjtBQUN0QixTQUFPLFFBQVA7QUFDQTs7QUFFRCxVQUFTLGtCQUFULENBQTRCLEtBQTVCLEVBQW1DO0FBQ2xDLFNBQU8sU0FBUyxLQUFULENBQVA7QUFDQTs7QUFFRCxLQUFJLFVBQVU7QUFDYixjQUFZLFVBREM7QUFFYixVQUFRLE1BRks7QUFHYixlQUFhLFdBSEE7QUFJYixzQkFBb0I7QUFKUCxFQUFkOztBQU9BLFFBQU8sY0FBUCxDQUFzQixPQUF0QixFQUErQixRQUEvQixFQUF5QztBQUN4QyxPQUFLO0FBRG1DLEVBQXpDOztBQUlBLFFBQU8sT0FBUDtBQUNBOztBQUVELE9BQU8sT0FBUCxHQUFpQixXQUFqQjs7Ozs7QUM1R0EsSUFBSSxxQkFBcUIsUUFBUSw2QkFBUixDQUF6QjtBQUNBLElBQUksaUJBQWlCLFFBQVEseUJBQVIsQ0FBckI7O0FBRUEsU0FBUyxXQUFULENBQXFCLEtBQXJCLEVBQTRCOztBQUUzQixVQUFTLGlCQUFULENBQTJCLFFBQTNCLEVBQXFDLFlBQXJDLEVBQW1EO0FBQ2xELE1BQUkscUJBQXFCLE1BQU0sVUFBTixJQUFvQixFQUE3QztBQUNBLE1BQUksSUFBSSxDQUFSO0FBQUEsTUFBVyxNQUFNLG1CQUFtQixNQUFwQztBQUNBLFNBQU0sSUFBSSxHQUFWLEVBQWU7QUFDZCxPQUFHLG1CQUFtQixDQUFuQixFQUFzQixJQUF0QixLQUErQixRQUFsQyxFQUE0QztBQUMzQyxXQUFPLG1CQUFtQixDQUFuQixFQUFzQixLQUE3QjtBQUNBO0FBQ0QsUUFBSyxDQUFMO0FBQ0E7QUFDRCxTQUFPLElBQVA7QUFFQTs7QUFFRCxVQUFTLFdBQVQsQ0FBcUIsUUFBckIsRUFBK0I7QUFDOUIsU0FBTyxDQUFDLENBQUMsa0JBQWtCLFFBQWxCLENBQVQ7QUFDQTs7QUFFRCxVQUFTLFdBQVQsQ0FBcUIsUUFBckIsRUFBK0I7QUFDOUIsU0FBTyxrQkFBa0IsUUFBbEIsQ0FBUDtBQUNBOztBQUVELFVBQVMscUJBQVQsQ0FBK0IsS0FBL0IsRUFBc0M7QUFDckMsU0FBTyxNQUFNLE1BQU4sQ0FBYSxxQkFBYixDQUFtQyxLQUFuQyxDQUFQO0FBQ0E7O0FBRUQsVUFBUyxtQkFBVCxDQUE2QixLQUE3QixFQUFvQztBQUNuQyxTQUFPLE1BQU0sTUFBTixDQUFhLG1CQUFiLENBQWlDLEtBQWpDLENBQVA7QUFDQTs7QUFFRCxLQUFJLFVBQVU7QUFDYixlQUFhLFdBREE7QUFFYixlQUFhLFdBRkE7QUFHYix5QkFBdUIscUJBSFY7QUFJYix1QkFBcUI7QUFKUixFQUFkO0FBTUEsUUFBTyxPQUFQO0FBQ0E7O0FBRUQsT0FBTyxPQUFQLEdBQWlCLFdBQWpCOzs7OztBQzNDQSxJQUFJLGNBQWMsUUFBUSx5QkFBUixDQUFsQjtBQUNBLElBQUksWUFBWSxRQUFRLHVCQUFSLENBQWhCO0FBQ0EsSUFBSSxVQUFVLFFBQVEsbUJBQVIsQ0FBZDtBQUNBLElBQUksU0FBUyxRQUFRLGlDQUFSLENBQWI7O0FBRUEsU0FBUyxTQUFULENBQW1CLEtBQW5CLEVBQTBCOztBQUV6QixLQUFJLFlBQVksVUFBVSxNQUFNLE9BQU4sQ0FBYyxjQUFkLENBQTZCLEtBQXZDLEVBQThDLEtBQTlDLENBQWhCO0FBQ0EsS0FBSSxVQUFVLFFBQVEsTUFBTSxPQUFOLENBQWMsY0FBZCxDQUE2QixjQUE3QixJQUErQyxFQUF2RCxFQUEyRCxLQUEzRCxDQUFkOztBQUVBLFVBQVMsaUJBQVQsR0FBNkI7QUFDNUIsUUFBTSxVQUFOLENBQWlCLElBQWpCLENBQXNCO0FBQ3JCLFNBQU0sV0FEZTtBQUVyQixVQUFPO0FBRmMsR0FBdEIsRUFHRTtBQUNELFNBQU0sV0FETDtBQUVELFVBQU87QUFGTixHQUhGLEVBTUU7QUFDRCxTQUFNLFNBREw7QUFFRCxVQUFPO0FBRk4sR0FORixFQVNFO0FBQ0QsU0FBTSxTQURMO0FBRUQsVUFBTztBQUZOLEdBVEY7QUFhQTs7QUFFRSxVQUFTLGlCQUFULENBQTJCLEtBQTNCLEVBQWtDLENBQ2pDOztBQUVKLFVBQVMsbUJBQVQsQ0FBNkIsS0FBN0IsRUFBb0M7QUFDbkMsTUFBSSxVQUFVLE1BQU0sT0FBcEI7QUFDRyxNQUFHLE1BQU0sTUFBTixDQUFhLG1CQUFoQixFQUFxQztBQUNqQyxXQUFRLE1BQU0sTUFBTixDQUFhLG1CQUFiLENBQWlDLEtBQWpDLENBQVI7QUFDQTtBQUNKLE1BQUksYUFBYSxRQUFqQjtBQUNHLE1BQUksZUFBZSxNQUFNLFdBQU4sQ0FBa0IsV0FBbEIsRUFBK0Isa0JBQS9CLEVBQW5CO0FBQ0EsZUFBYSxhQUFiLENBQTJCLFVBQTNCO0FBQ0EsTUFBRyxRQUFRLFNBQVIsSUFBcUIsUUFBUSxTQUFSLENBQWtCLE1BQTFDLEVBQWlEO0FBQzdDLE9BQUksQ0FBSjtBQUFBLE9BQU8sTUFBTSxRQUFRLFNBQVIsQ0FBa0IsTUFBL0I7QUFDQSxRQUFJLElBQUUsQ0FBTixFQUFRLElBQUUsR0FBVixFQUFjLEtBQUcsQ0FBakIsRUFBbUI7QUFDZixZQUFRLFNBQVIsQ0FBa0IsQ0FBbEIsRUFBcUIsY0FBckIsQ0FBb0MsS0FBcEMsQ0FBMEMsYUFBMUMsQ0FBd0QsVUFBeEQ7QUFDSDtBQUNKO0FBQ0QsU0FBTyxXQUFXLFlBQVgsQ0FBd0IsS0FBeEIsQ0FBUDtBQUNOOztBQUVELFVBQVMscUJBQVQsQ0FBK0IsS0FBL0IsRUFBc0M7QUFDckMsTUFBSSxVQUFVLE1BQU0sT0FBcEI7QUFDQSxNQUFJLGFBQWEsUUFBakI7QUFDTSxNQUFJLGVBQWUsTUFBTSxXQUFOLENBQWtCLFdBQWxCLEVBQStCLGtCQUEvQixFQUFuQjtBQUNBLGVBQWEsYUFBYixDQUEyQixVQUEzQjtBQUNBLE1BQUcsUUFBUSxTQUFSLElBQXFCLFFBQVEsU0FBUixDQUFrQixNQUExQyxFQUFpRDtBQUM3QyxPQUFJLENBQUo7QUFBQSxPQUFPLE1BQU0sUUFBUSxTQUFSLENBQWtCLE1BQS9CO0FBQ0EsUUFBSSxJQUFFLENBQU4sRUFBUSxJQUFFLEdBQVYsRUFBYyxLQUFHLENBQWpCLEVBQW1CO0FBQ2YsWUFBUSxTQUFSLENBQWtCLENBQWxCLEVBQXFCLGNBQXJCLENBQW9DLEtBQXBDLENBQTBDLGFBQTFDLENBQXdELFVBQXhEO0FBQ0g7QUFDSjtBQUNELFVBQVEsV0FBVyxpQkFBWCxDQUE2QixNQUFNLENBQU4sQ0FBN0IsRUFBc0MsTUFBTSxDQUFOLENBQXRDLEVBQStDLE1BQU0sQ0FBTixLQUFVLENBQXpELENBQVI7QUFDQSxNQUFHLE1BQU0sTUFBTixDQUFhLHFCQUFoQixFQUF1QztBQUN0QyxVQUFPLE1BQU0sTUFBTixDQUFhLHFCQUFiLENBQW1DLEtBQW5DLENBQVA7QUFDQSxHQUZELE1BRU87QUFDTixVQUFPLEtBQVA7QUFDQTtBQUNQOztBQUVELFVBQVMsY0FBVCxHQUEwQjtBQUN6QixTQUFPLE1BQU0sT0FBYjtBQUNBOztBQUVELEtBQUksVUFBVTtBQUNiLGtCQUFnQixjQURIO0FBRWIsdUJBQXFCLG1CQUZSO0FBR2IseUJBQXVCO0FBSFYsRUFBZDs7QUFNQTs7QUFFQSxRQUFPLE9BQU8sTUFBUCxDQUFjLEtBQWQsRUFBcUIsWUFBWSxLQUFaLENBQXJCLEVBQXlDLE9BQXpDLENBQVA7QUFDQTs7QUFFRCxPQUFPLE9BQVAsR0FBaUIsU0FBakI7Ozs7O0FDaEZBLElBQUksY0FBYyxRQUFRLHNCQUFSLENBQWxCO0FBQ0EsSUFBSSxZQUFZLFFBQVEsNEJBQVIsQ0FBaEI7O0FBRUEsU0FBUyxTQUFULENBQW1CLFFBQW5CLEVBQTZCOztBQUU1QixVQUFTLFVBQVQsR0FBc0I7QUFDckIsU0FBTyxTQUFTLE1BQWhCO0FBQ0E7O0FBRUQsVUFBUyxrQkFBVCxDQUE0QixRQUE1QixFQUFzQyxJQUF0QyxFQUE0QztBQUMzQyxTQUFPLFNBQVMsTUFBVCxDQUFnQixVQUFTLE9BQVQsRUFBa0I7QUFDeEMsVUFBTyxRQUFRLElBQVIsQ0FBYSxFQUFiLEtBQW9CLFlBQVksSUFBWixDQUEzQjtBQUNBLEdBRk0sQ0FBUDtBQUdBOztBQUVELFVBQVMsa0JBQVQsQ0FBNEIsUUFBNUIsRUFBc0MsSUFBdEMsRUFBNEM7QUFDM0MsU0FBTyxTQUFTLE1BQVQsQ0FBZ0IsVUFBUyxPQUFULEVBQWtCO0FBQ3hDLFVBQU8sUUFBUSxJQUFSLENBQWEsRUFBYixLQUFvQixJQUEzQjtBQUNBLEdBRk0sQ0FBUDtBQUdBOztBQUVELFVBQVMsU0FBVCxHQUFxQjtBQUNuQixTQUFPLFVBQVUsUUFBVixDQUFQO0FBQ0Q7O0FBRUQsVUFBUyxlQUFULENBQXlCLElBQXpCLEVBQStCO0FBQzlCLE1BQUksZUFBZSxtQkFBbUIsUUFBbkIsRUFBNkIsSUFBN0IsQ0FBbkI7QUFDQSxTQUFPLFVBQVUsWUFBVixDQUFQO0FBQ0E7O0FBRUQsVUFBUyxlQUFULENBQXlCLElBQXpCLEVBQStCO0FBQzlCLE1BQUksZUFBZSxtQkFBbUIsUUFBbkIsRUFBNkIsSUFBN0IsQ0FBbkI7QUFDQSxTQUFPLFVBQVUsWUFBVixDQUFQO0FBQ0E7O0FBRUQsVUFBUyxLQUFULENBQWUsS0FBZixFQUFzQjtBQUNyQixNQUFJLFNBQVMsU0FBUyxNQUF0QixFQUE4QjtBQUM3QixVQUFPLEVBQVA7QUFDQTtBQUNELFNBQU8sVUFBVSxTQUFTLFNBQVMsS0FBVCxDQUFULENBQVYsQ0FBUDtBQUNBOztBQUVELFVBQVMsb0JBQVQsQ0FBOEIsaUJBQTlCLEVBQWlELElBQWpELEVBQXVEO0FBQ3RELG9CQUFrQixNQUFsQixDQUF5QixVQUFTLFdBQVQsRUFBc0IsS0FBdEIsRUFBNEI7QUFDcEQsT0FBSSxTQUFTLEtBQWI7QUFDQSxlQUFZLEtBQVosSUFBcUIsWUFBVztBQUMvQixRQUFJLGFBQWEsU0FBakI7QUFDQSxXQUFPLFNBQVMsR0FBVCxDQUFhLFVBQVMsT0FBVCxFQUFpQjtBQUNwQyxTQUFJLFFBQVEsVUFBVSxPQUFWLENBQVo7QUFDQSxTQUFHLE1BQU0sTUFBTixDQUFILEVBQWtCO0FBQ2pCLGFBQU8sTUFBTSxNQUFOLEVBQWMsS0FBZCxDQUFvQixJQUFwQixFQUEwQixVQUExQixDQUFQO0FBQ0E7QUFDRCxZQUFPLElBQVA7QUFDQSxLQU5NLENBQVA7QUFPQSxJQVREO0FBVUEsVUFBTyxXQUFQO0FBQ0EsR0FiRCxFQWFHLE9BYkg7QUFjQTs7QUFFRCxVQUFTLGlCQUFULEdBQTZCO0FBQzVCLFNBQU8sUUFBUDtBQUNBOztBQUVELFVBQVMsTUFBVCxDQUFnQixJQUFoQixFQUFzQjtBQUNyQixTQUFPLFNBQVMsTUFBVCxDQUFnQixLQUFLLGlCQUFMLEVBQWhCLENBQVA7QUFDQTs7QUFFRCxLQUFJLFVBQVU7QUFDYixhQUFXLFNBREU7QUFFYixtQkFBaUIsZUFGSjtBQUdiLG1CQUFpQixlQUhKO0FBSWIsU0FBTyxLQUpNO0FBS2IsVUFBUSxNQUxLO0FBTWIscUJBQW1CO0FBTk4sRUFBZDs7QUFTQSxzQkFBcUIsQ0FBQyxjQUFELEVBQWlCLFNBQWpCLEVBQTRCLGFBQTVCLENBQXJCO0FBQ0Esc0JBQXFCLENBQUMsU0FBRCxFQUFZLFNBQVosRUFBdUIsaUJBQXZCLEVBQTBDLGVBQTFDLEVBQTJELG9CQUEzRCxDQUFyQjs7QUFFQSxRQUFPLGNBQVAsQ0FBc0IsT0FBdEIsRUFBK0IsUUFBL0IsRUFBeUM7QUFDeEMsT0FBSztBQURtQyxFQUF6QztBQUdBLFFBQU8sT0FBUDtBQUNBOztBQUVELE9BQU8sT0FBUCxHQUFpQixTQUFqQjs7Ozs7QUNyRkEsSUFBSSxjQUFjLFFBQVEsNEJBQVIsQ0FBbEI7QUFDQSxJQUFJLFdBQVcsUUFBUSx5QkFBUixDQUFmOztBQUVBLFNBQVMsTUFBVCxDQUFnQixPQUFoQixFQUF5QixNQUF6QixFQUFpQzs7QUFFaEMsS0FBSSxXQUFXLEVBQWY7O0FBRUEsS0FBSSxRQUFRO0FBQ1gsV0FBUyxPQURFO0FBRVgsVUFBUSxNQUZHO0FBR1gsY0FBWTtBQUhELEVBQVo7O0FBTUEsVUFBUyxpQkFBVCxHQUE2QjtBQUM1QixTQUFPLENBQ047QUFDQyxTQUFNLG1CQURQO0FBRUMsVUFBTyxTQUFTLFFBQVEsQ0FBakIsRUFBb0IsTUFBcEI7QUFGUixHQURNLEVBS047QUFDQyxTQUFNLE1BRFA7QUFFQyxVQUFPLFNBQVMsUUFBUSxFQUFqQixFQUFxQixNQUFyQjtBQUZSLEdBTE0sRUFTTjtBQUNDLFNBQU0sVUFEUDtBQUVDLFVBQU8sU0FBUyxRQUFRLENBQWpCLEVBQW9CLE1BQXBCO0FBRlIsR0FUTSxFQWFOO0FBQ0MsU0FBTSxZQURQO0FBRUMsVUFBTyxTQUFTLFFBQVEsRUFBakIsRUFBcUIsTUFBckI7QUFGUixHQWJNLEVBaUJOO0FBQ0MsU0FBTSxZQURQO0FBRUMsVUFBTyxTQUFTLFFBQVEsRUFBakIsRUFBcUIsTUFBckI7QUFGUixHQWpCTSxFQXFCTjtBQUNDLFNBQU0sWUFEUDtBQUVDLFVBQU8sU0FBUyxRQUFRLEVBQWpCLEVBQXFCLE1BQXJCO0FBRlIsR0FyQk0sRUF5Qk47QUFDQyxTQUFNLGFBRFA7QUFFQyxVQUFPLFNBQVMsUUFBUSxFQUFqQixFQUFxQixNQUFyQjtBQUZSLEdBekJNLENBQVA7QUE4QkE7O0FBRUQsVUFBUyxjQUFULEdBQTBCO0FBQ3pCLFNBQU8sTUFBTSxPQUFiO0FBQ0E7O0FBRUQsS0FBSSxVQUFVO0FBQ2Isa0JBQWdCO0FBREgsRUFBZDs7QUFJQSxRQUFPLE9BQU8sTUFBUCxDQUFjLFFBQWQsRUFBd0IsWUFBWSxLQUFaLENBQXhCLEVBQTRDLE9BQTVDLENBQVA7QUFDQTs7QUFFRCxPQUFPLE9BQVAsR0FBaUIsTUFBakI7Ozs7O0FDekRBLElBQUksY0FBYyxRQUFRLDRCQUFSLENBQWxCO0FBQ0EsSUFBSSxZQUFZLFFBQVEsY0FBUixDQUFoQjtBQUNBLElBQUksWUFBWSxRQUFRLCtCQUFSLENBQWhCO0FBQ0EsSUFBSSxXQUFXLFFBQVEseUJBQVIsQ0FBZjtBQUNBLElBQUksWUFBWSxRQUFRLGFBQVIsQ0FBaEI7O0FBRUEsU0FBUyxXQUFULENBQXFCLE9BQXJCLEVBQThCLE1BQTlCLEVBQXNDOztBQUVyQyxLQUFJLFdBQVcsRUFBZjs7QUFFQSxLQUFJLFFBQVE7QUFDWCxXQUFTLE9BREU7QUFFWCxVQUFRLE1BRkc7QUFHWCxjQUFZO0FBSEQsRUFBWjs7QUFNQSxVQUFTLGFBQVQsQ0FBdUIsS0FBdkIsRUFBOEIsS0FBOUIsRUFBcUM7QUFDcEMsTUFBSSxZQUFZLElBQWhCO0FBQ0EsTUFBSSxLQUFLO0FBQ1IsU0FBTSxNQUFNO0FBREosR0FBVDs7QUFJQSxXQUFTLFdBQVQsR0FBdUI7QUFDdEIsT0FBRyxDQUFDLFNBQUosRUFBZTtBQUNkLGdCQUFZLFVBQVUsUUFBUSxRQUFSLENBQWlCLEtBQWpCLENBQVYsRUFBbUMsS0FBbkMsQ0FBWjtBQUNBO0FBQ0QsVUFBTyxTQUFQO0FBQ0E7O0FBRUQsU0FBTyxjQUFQLENBQXNCLEVBQXRCLEVBQTBCLE9BQTFCLEVBQW1DO0FBQ2xDLFFBQU07QUFENEIsR0FBbkM7QUFHQSxTQUFPLEVBQVA7QUFDQTs7QUFHRCxVQUFTLGlCQUFULEdBQTZCO0FBQzVCLE1BQUksb0JBQW9CLFFBQVEsTUFBUixDQUFlLEdBQWYsQ0FBbUIsYUFBbkIsQ0FBeEI7QUFDQSxTQUFPLENBQ047QUFDQyxTQUFNLFlBRFA7QUFFQyxVQUFPLFVBQVUsUUFBUSxFQUFsQjtBQUZSLEdBRE0sRUFLTCxNQUxLLENBS0UsaUJBTEYsQ0FBUDtBQU1BOztBQUVELEtBQUksVUFBVSxFQUFkOztBQUdBLFFBQU8sT0FBTyxNQUFQLENBQWMsUUFBZCxFQUF3QixVQUFVLEtBQVYsQ0FBeEIsRUFBMEMsWUFBWSxNQUFNLFFBQWxCLEVBQTRCLE9BQTVCLENBQTFDLEVBQWdGLE9BQWhGLENBQVA7QUFDQTs7QUFFRCxPQUFPLE9BQVAsR0FBaUIsV0FBakI7Ozs7O0FDcERBLElBQUksY0FBYyxRQUFRLDRCQUFSLENBQWxCO0FBQ0EsSUFBSSxnQkFBZ0IsUUFBUSw4QkFBUixDQUFwQjs7QUFFQSxTQUFTLFNBQVQsQ0FBbUIsUUFBbkIsRUFBNkIsTUFBN0IsRUFBcUM7QUFDcEMsS0FBSSxRQUFRO0FBQ1gsWUFBVSxRQURDO0FBRVgsVUFBUTtBQUZHLEVBQVo7O0FBS0EsS0FBSSxtQkFBbUIsS0FBdkI7QUFDQSxLQUFJLHFCQUFxQixDQUF6QjtBQUNBLEtBQUksb0JBQW9CLENBQXhCO0FBQ0EsS0FBSSxlQUFlLENBQW5CO0FBQUEsS0FBc0IsY0FBYyxDQUFwQztBQUNBLEtBQUksV0FBVyxDQUFmO0FBQ0EsS0FBSSxRQUFRLElBQVo7QUFDQSxLQUFJLGFBQWEsQ0FBakI7QUFDQSxLQUFJLFNBQVMsQ0FBYjtBQUNBLEtBQUksVUFBVSxLQUFkO0FBQ0EsS0FBSSxlQUFlLEtBQW5CO0FBQ0EsS0FBSSxpQkFBaUIsRUFBckI7QUFDQSxLQUFJLGdCQUFKO0FBQ0EsS0FBSSxxQkFBcUIsSUFBekI7QUFDQSxLQUFJLGtCQUFrQjtBQUNyQixRQUFNLENBQUM7QUFEYyxFQUF0Qjs7QUFJQSxVQUFTLFdBQVQsQ0FBcUIsSUFBckIsRUFBMkIsR0FBM0IsRUFBZ0MsS0FBaEMsRUFBdUM7QUFDdEMsWUFBVSxLQUFWO0FBQ0EsTUFBRyxLQUFILEVBQVU7QUFDVDtBQUNBLGlCQUFjLElBQWQ7QUFDQTtBQUNELE1BQUcsWUFBSCxFQUFpQjtBQUNoQixXQUFRLEdBQVIsQ0FBWSxJQUFaLEVBQWtCLEdBQWxCO0FBQ0E7QUFDRCxlQUFhLENBQWI7QUFDQSxpQkFBZSxLQUFLLEdBQUwsRUFBZjtBQUNBLHVCQUFxQixJQUFyQjtBQUNBLHNCQUFvQixHQUFwQjtBQUNBO0FBQ0E7O0FBRUQsVUFBUyxpQkFBVCxHQUE2QjtBQUM1QixNQUFJLGFBQWEsZUFBZSxLQUFmLEVBQWpCO0FBQ0EsY0FBWSxXQUFXLENBQVgsQ0FBWixFQUEyQixXQUFXLENBQVgsQ0FBM0I7QUFDQTs7QUFFRCxVQUFTLFlBQVQsQ0FBc0IsSUFBdEIsRUFBNEIsR0FBNUIsRUFBaUM7QUFDaEMsaUJBQWUsSUFBZixDQUFvQixDQUFDLElBQUQsRUFBTyxHQUFQLENBQXBCO0FBQ0E7O0FBRUQsVUFBUyxVQUFULEdBQXNCO0FBQ3JCLGlCQUFlLE1BQWYsR0FBd0IsQ0FBeEI7QUFDQTs7QUFFRCxVQUFTLGNBQVQsQ0FBd0IsWUFBeEIsRUFBc0M7QUFDckMsTUFBRyx1QkFBdUIsaUJBQTFCLEVBQTZDO0FBQzVDLGlCQUFjLGtCQUFkO0FBQ0EsR0FGRCxNQUVPLElBQUcsQ0FBQyxPQUFKLEVBQWE7QUFDbkIsT0FBSSxVQUFVLEtBQUssR0FBTCxFQUFkO0FBQ0EsT0FBSSxjQUFjLFVBQVUsVUFBVSxZQUFwQixJQUFvQyxJQUF0RDtBQUNBLGtCQUFlLE9BQWY7QUFDQSxPQUFHLHFCQUFxQixpQkFBeEIsRUFBMkM7QUFDMUMsbUJBQWUsV0FBZjtBQUNBLFFBQUcsY0FBYyxpQkFBakIsRUFBb0M7QUFDbkMsbUJBQWMsQ0FBZDtBQUNBLFNBQUcsZUFBZSxNQUFsQixFQUEwQjtBQUN6QjtBQUNBLE1BRkQsTUFFTyxJQUFHLENBQUMsS0FBSixFQUFXO0FBQ2pCLG9CQUFjLGlCQUFkO0FBQ0EsTUFGTSxNQUVBO0FBQ047O0FBRUEsb0JBQWMsZUFBZSxvQkFBb0Isa0JBQW5DLENBQWQ7QUFDQTtBQUNBO0FBQ0M7QUFDRDtBQUNEO0FBQ0QsSUFqQkQsTUFpQk87QUFDTixtQkFBZSxXQUFmO0FBQ0EsUUFBRyxjQUFjLGlCQUFqQixFQUFvQztBQUNuQyxtQkFBYyxDQUFkO0FBQ0EsU0FBRyxlQUFlLE1BQWxCLEVBQTBCO0FBQ3pCO0FBQ0EsTUFGRCxNQUVPLElBQUcsQ0FBQyxLQUFKLEVBQVc7QUFDakIsb0JBQWMsaUJBQWQ7QUFDQSxNQUZNLE1BRUE7QUFDTixvQkFBYyxzQkFBc0Isb0JBQW9CLFdBQTFDLENBQWQ7QUFDQTtBQUNEO0FBQ0Q7QUFDRCxPQUFHLFlBQUgsRUFBaUI7QUFDaEIsWUFBUSxHQUFSLENBQVksV0FBWjtBQUNBO0FBQ0Q7QUFDRCxNQUFHLFNBQVMsWUFBVCxJQUF5QixnQkFBZ0IsSUFBaEIsS0FBeUIsV0FBckQsRUFBa0U7QUFDakUsbUJBQWdCLElBQWhCLEdBQXVCLFdBQXZCO0FBQ0EsWUFBUyxZQUFULENBQXNCLGVBQXRCO0FBQ0E7QUFDRCxTQUFPLFdBQVA7QUFDQTs7QUFFRCxVQUFTLFdBQVQsR0FBdUI7QUFDdEIsTUFBRyxDQUFDLGdCQUFKLEVBQXNCO0FBQ3JCLHNCQUFtQixJQUFuQjtBQUNBLHNCQUFtQixTQUFTLFFBQVQsQ0FBa0IsY0FBbEIsRUFBa0MsWUFBbEMsQ0FBbkI7QUFDQTtBQUNEOztBQUVELFVBQVMsTUFBVCxDQUFnQixHQUFoQixFQUFxQixLQUFyQixFQUE0QjtBQUMzQixZQUFVLEtBQVY7QUFDQSxNQUFHLEtBQUgsRUFBVTtBQUNUO0FBQ0E7QUFDRDtBQUNBLHNCQUFvQixHQUFwQjtBQUNBOztBQUVELFVBQVMsY0FBVCxHQUEwQjtBQUN6QixNQUFHLGdCQUFILEVBQXFCO0FBQ3BCLFVBQU8sV0FBUDtBQUNBLEdBRkQsTUFFTztBQUNOLFVBQU8sU0FBUyxDQUFULEdBQWEsU0FBUyxJQUE3QjtBQUNBO0FBQ0Q7O0FBRUQsVUFBUyxPQUFULENBQWlCLElBQWpCLEVBQXVCO0FBQ3RCLFVBQVEsSUFBUjtBQUNBOztBQUVELFVBQVMsUUFBVCxDQUFrQixLQUFsQixFQUF5QjtBQUN4QixXQUFTLEtBQVQ7QUFDQTs7QUFFRCxVQUFTLFlBQVQsQ0FBc0IsSUFBdEIsRUFBNEI7QUFDM0IsaUJBQWUsSUFBZjtBQUNBOztBQUVELFVBQVMsS0FBVCxHQUFpQjtBQUNoQixZQUFVLElBQVY7QUFDQTs7QUFFRCxVQUFTLE9BQVQsR0FBbUI7QUFDbEIsTUFBRyxnQkFBSCxFQUFxQjtBQUNwQjtBQUNBLFNBQU0sUUFBTixHQUFpQixJQUFqQjtBQUNBLFNBQU0sTUFBTixHQUFlLElBQWY7QUFDQTtBQUNEOztBQUVELEtBQUksVUFBVTtBQUNiLGVBQWEsV0FEQTtBQUViLFVBQVEsTUFGSztBQUdiLGdCQUFjLFlBSEQ7QUFJYixjQUFZLFVBSkM7QUFLYixXQUFTLE9BTEk7QUFNYixZQUFVLFFBTkc7QUFPYixTQUFPLEtBUE07QUFRYixnQkFBYyxZQVJEO0FBU2Isa0JBQWdCLGNBVEg7QUFVYixnQkFBYyxJQVZEO0FBV2IsV0FBUztBQVhJLEVBQWQ7O0FBY0EsS0FBSSxXQUFXLEVBQWY7O0FBRUEsUUFBTyxPQUFPLE1BQVAsQ0FBYyxRQUFkLEVBQXdCLE9BQXhCLEVBQWlDLGNBQWMsS0FBZCxDQUFqQyxFQUF1RCxZQUFZLEtBQVosQ0FBdkQsQ0FBUDtBQUNBOztBQUVELE9BQU8sT0FBUCxHQUFpQixTQUFqQjs7Ozs7QUMxS0EsSUFBSSxXQUFXLFFBQVEseUJBQVIsQ0FBZjs7QUFFQSxTQUFTLGFBQVQsQ0FBdUIsTUFBdkIsRUFBK0IsTUFBL0IsRUFBdUM7O0FBRXRDLFFBQU8sU0FBUyxPQUFPLENBQWhCLEVBQW1CLE1BQW5CLENBQVA7QUFDQTs7QUFFRCxPQUFPLE9BQVAsR0FBaUIsYUFBakI7Ozs7O0FDUEEsSUFBSSxjQUFjLFFBQVEsNEJBQVIsQ0FBbEI7QUFDQSxJQUFJLFdBQVcsUUFBUSx5QkFBUixDQUFmO0FBQ0EsSUFBSSxnQkFBZ0IsUUFBUSxpQkFBUixDQUFwQjs7QUFFQSxTQUFTLE9BQVQsQ0FBaUIsT0FBakIsRUFBMEIsTUFBMUIsRUFBa0M7O0FBRWpDLEtBQUksUUFBUTtBQUNYLFVBQVEsTUFERztBQUVYLGNBQVk7QUFGRCxFQUFaOztBQUtBLFVBQVMsUUFBVCxDQUFrQixVQUFsQixFQUE4QixLQUE5QixFQUFxQztBQUNwQyxNQUFJLEtBQUssV0FBVyxJQUFYLEdBQWtCLFdBQVcsSUFBWCxDQUFnQixFQUFsQyxHQUF1QyxNQUFNLFFBQU4sRUFBaEQ7QUFDQSxNQUFJLGdCQUFnQixXQUFXLElBQVgsR0FBa0IsUUFBUSxXQUFXLGNBQW5CLEVBQW1DLE1BQW5DLENBQWxCLEdBQStELFNBQVMsV0FBVyxDQUFwQixFQUF1QixNQUF2QixDQUFuRjtBQUNBLFNBQU87QUFDTixTQUFNLEVBREE7QUFFTixVQUFPO0FBRkQsR0FBUDtBQUlBOztBQUVELFVBQVMsZUFBVCxHQUEyQjtBQUMxQixNQUFJLENBQUo7QUFBQSxNQUFPLE1BQU0sUUFBUSxNQUFyQjtBQUNBLE1BQUksTUFBTSxFQUFWO0FBQ0EsT0FBSyxJQUFJLENBQVQsRUFBWSxJQUFJLEdBQWhCLEVBQXFCLEtBQUssQ0FBMUIsRUFBNkI7QUFDNUIsT0FBSSxJQUFKLENBQVMsU0FBUyxRQUFRLENBQVIsQ0FBVCxFQUFxQixDQUFyQixDQUFUO0FBQ0E7QUFDRCxTQUFPLEdBQVA7QUFDQTs7QUFFRCxLQUFJLFVBQVUsRUFBZDs7QUFHQSxRQUFPLE9BQU8sTUFBUCxDQUFjLE9BQWQsRUFBdUIsWUFBWSxLQUFaLENBQXZCLENBQVA7QUFDQTs7QUFFRCxPQUFPLE9BQVAsR0FBaUIsT0FBakI7Ozs7O0FDbkNBLElBQUksWUFBWSxRQUFRLGNBQVIsQ0FBaEI7O0FBRUEsU0FBUyxLQUFULENBQWUsT0FBZixFQUF3QjtBQUN2QixLQUFJLFFBQVE7QUFDWCxXQUFTLE9BREU7QUFFWCxjQUFZO0FBRkQsRUFBWjs7QUFLQSxLQUFJLFVBQVUsRUFBZDs7QUFHQSxRQUFPLE9BQU8sTUFBUCxDQUFjLEVBQWQsRUFBa0IsVUFBVSxLQUFWLENBQWxCLEVBQW9DLE9BQXBDLENBQVA7QUFDQTs7QUFFRCxPQUFPLE9BQVAsR0FBaUIsS0FBakI7Ozs7O0FDZEEsSUFBSSxZQUFZLFFBQVEsY0FBUixDQUFoQjs7QUFFQSxTQUFTLFdBQVQsQ0FBcUIsT0FBckIsRUFBOEIsTUFBOUIsRUFBc0M7O0FBRXJDLEtBQUksV0FBVyxFQUFmOztBQUVBLEtBQUksUUFBUTtBQUNYLFdBQVMsT0FERTtBQUVYLFVBQVEsTUFGRztBQUdYLGNBQVk7QUFIRCxFQUFaOztBQU1BLFVBQVMsaUJBQVQsR0FBNkI7QUFDNUIsU0FBTyxFQUFQO0FBRUE7O0FBRUQsS0FBSSxVQUFVLEVBQWQ7O0FBR0EsUUFBTyxPQUFPLE1BQVAsQ0FBYyxRQUFkLEVBQXdCLFVBQVUsS0FBVixDQUF4QixFQUEwQyxPQUExQyxDQUFQO0FBQ0E7O0FBRUQsT0FBTyxPQUFQLEdBQWlCLFdBQWpCOzs7OztBQ3ZCQSxJQUFJLFlBQVksUUFBUSxjQUFSLENBQWhCO0FBQ0EsSUFBSSxnQkFBZ0IsUUFBUSxpQkFBUixDQUFwQjs7QUFFQSxTQUFTLEtBQVQsQ0FBZSxPQUFmLEVBQXdCLE1BQXhCLEVBQWdDOztBQUUvQixLQUFJLFFBQVE7QUFDWCxjQUFZLEVBREQ7QUFFWCxVQUFRLE1BRkc7QUFHWCxXQUFTO0FBSEUsRUFBWjtBQUtBLEtBQUksZ0JBQWdCLGNBQWMsUUFBUSxJQUFSLENBQWEsTUFBM0IsRUFBbUMsUUFBUSxTQUEzQyxFQUFzRCxLQUF0RCxDQUFwQjs7QUFJQSxVQUFTLGlCQUFULEdBQTZCO0FBQzVCLFFBQU0sVUFBTixDQUFpQixJQUFqQixDQUNDO0FBQ0MsU0FBTSxVQURQO0FBRUMsVUFBTztBQUZSLEdBREQ7QUFNQTs7QUFFRCxLQUFJLFVBQVUsRUFBZDs7QUFHQTs7QUFFQSxRQUFPLE9BQU8sTUFBUCxDQUFjLEtBQWQsRUFBcUIsVUFBVSxLQUFWLENBQXJCLEVBQXVDLE9BQXZDLENBQVA7QUFDQTs7QUFFRCxPQUFPLE9BQVAsR0FBaUIsS0FBakI7Ozs7O0FDL0JBLElBQUksY0FBYyxRQUFRLDRCQUFSLENBQWxCO0FBQ0EsSUFBSSxXQUFXLFFBQVEseUJBQVIsQ0FBZjtBQUNBLElBQUksaUJBQWlCLFFBQVEsa0JBQVIsQ0FBckI7QUFDQSxJQUFJLFlBQVksUUFBUSxhQUFSLENBQWhCO0FBQ0EsSUFBSSxjQUFjLFFBQVEsZUFBUixDQUFsQjtBQUNBLElBQUksZUFBZSxRQUFRLGdCQUFSLENBQW5CO0FBQ0EsSUFBSSxvQkFBb0IsUUFBUSxxQkFBUixDQUF4QjtBQUNBLElBQUksc0JBQXNCLFFBQVEsdUJBQVIsQ0FBMUI7QUFDQSxJQUFJLGlCQUFpQixRQUFRLGtCQUFSLENBQXJCO0FBQ0EsSUFBSSxnQkFBZ0IsUUFBUSxpQkFBUixDQUFwQjtBQUNBLElBQUksZ0JBQWdCLFFBQVEsaUJBQVIsQ0FBcEI7QUFDQSxJQUFJLG9CQUFvQixRQUFRLHFCQUFSLENBQXhCO0FBQ0EsSUFBSSxZQUFZLFFBQVEsYUFBUixDQUFoQjtBQUNBLElBQUksWUFBWSxRQUFRLHdCQUFSLENBQWhCO0FBQ0EsSUFBSSxTQUFTLFFBQVEsb0NBQVIsQ0FBYjs7QUFFQSxTQUFTLGFBQVQsQ0FBdUIsVUFBdkIsRUFBbUMsTUFBbkMsRUFBMkMsTUFBM0MsRUFBbUQ7QUFDbEQsS0FBSSxRQUFRO0FBQ1gsY0FBWSxtQkFERDtBQUVYLFVBQVE7QUFGRyxFQUFaOztBQUtBLEtBQUksd0JBQXdCLEVBQTVCOztBQUVBLFVBQVMsZ0JBQVQsQ0FBMEIsS0FBMUIsRUFBaUMsS0FBakMsRUFBd0M7QUFDdkMsTUFBSSxLQUFLO0FBQ1IsU0FBTSxNQUFNO0FBREosR0FBVDtBQUdBLFNBQU8sY0FBUCxDQUFzQixFQUF0QixFQUEwQixPQUExQixFQUFtQztBQUNoQyxNQURnQyxpQkFDMUI7QUFDTCxRQUFHLHNCQUFzQixLQUF0QixDQUFILEVBQWlDO0FBQ2hDLFlBQU8sc0JBQXNCLEtBQXRCLENBQVA7QUFDQSxLQUZELE1BRU87QUFDTixTQUFJLFFBQUo7QUFDQTtBQUNELFFBQUcsTUFBTSxFQUFOLEtBQWEsSUFBaEIsRUFBc0I7QUFDckIsZ0JBQVcsY0FBYyxXQUFXLEtBQVgsRUFBa0IsRUFBaEMsRUFBb0MsT0FBTyxLQUFQLEVBQWMsRUFBbEQsRUFBc0QsS0FBdEQsQ0FBWDtBQUNBLEtBRkQsTUFFTyxJQUFHLE1BQU0sRUFBTixLQUFhLElBQWhCLEVBQXNCO0FBQzVCLGdCQUFXLGVBQWUsT0FBTyxLQUFQLENBQWYsRUFBOEIsS0FBOUIsQ0FBWDtBQUNBLEtBRk0sTUFFQSxJQUFHLE1BQU0sRUFBTixLQUFhLElBQWhCLEVBQXNCO0FBQzVCLGdCQUFXLGFBQWEsT0FBTyxLQUFQLENBQWIsRUFBNEIsS0FBNUIsQ0FBWDtBQUNBLEtBRk0sTUFFQSxJQUFHLE1BQU0sRUFBTixLQUFhLElBQWhCLEVBQXNCO0FBQzVCLGdCQUFXLFVBQVUsT0FBTyxLQUFQLENBQVYsRUFBeUIsS0FBekIsQ0FBWDtBQUNBLEtBRk0sTUFFQSxJQUFHLE1BQU0sRUFBTixLQUFhLElBQWhCLEVBQXNCO0FBQzVCLGdCQUFXLFlBQVksT0FBTyxLQUFQLENBQVosRUFBMkIsS0FBM0IsQ0FBWDtBQUNBLEtBRk0sTUFFQSxJQUFHLE1BQU0sRUFBTixLQUFhLElBQWhCLEVBQXNCO0FBQzVCLGdCQUFXLGtCQUFrQixPQUFPLEtBQVAsQ0FBbEIsRUFBaUMsS0FBakMsQ0FBWDtBQUNBLEtBRk0sTUFFQSxJQUFHLE1BQU0sRUFBTixLQUFhLElBQWhCLEVBQXNCO0FBQzVCLGdCQUFXLG9CQUFvQixPQUFPLEtBQVAsQ0FBcEIsRUFBbUMsS0FBbkMsQ0FBWDtBQUNBLEtBRk0sTUFFQSxJQUFHLE1BQU0sRUFBTixLQUFhLElBQWhCLEVBQXNCO0FBQzVCLGdCQUFXLGVBQWUsT0FBTyxLQUFQLENBQWYsRUFBOEIsS0FBOUIsQ0FBWDtBQUNBLEtBRk0sTUFFQSxJQUFHLE1BQU0sRUFBTixLQUFhLElBQWhCLEVBQXNCO0FBQzVCLGdCQUFXLGNBQWMsT0FBTyxLQUFQLENBQWQsRUFBNkIsS0FBN0IsQ0FBWDtBQUNBLEtBRk0sTUFFQSxJQUFHLE1BQU0sRUFBTixLQUFhLElBQWhCLEVBQXNCO0FBQzVCLGdCQUFXLGNBQWMsT0FBTyxLQUFQLENBQWQsRUFBNkIsS0FBN0IsQ0FBWDtBQUNBLEtBRk0sTUFFQSxJQUFHLE1BQU0sRUFBTixLQUFhLElBQWhCLEVBQXNCO0FBQzVCLGdCQUFXLGtCQUFrQixPQUFPLEtBQVAsQ0FBbEIsRUFBaUMsS0FBakMsQ0FBWDtBQUNBLEtBRk0sTUFFQSxJQUFHLE1BQU0sRUFBTixLQUFhLElBQWhCLEVBQXNCO0FBQzVCLGdCQUFXLFVBQVUsT0FBTyxLQUFQLENBQVYsRUFBeUIsS0FBekIsQ0FBWDtBQUNBLEtBRk0sTUFFQSxJQUFHLE1BQU0sRUFBTixLQUFhLElBQWhCLEVBQXNCO0FBQzVCLGdCQUFXLFVBQVUsT0FBTyxLQUFQLEVBQWMsU0FBZCxDQUF3QixNQUFsQyxFQUEwQyxLQUExQyxDQUFYO0FBQ0EsS0FGTSxNQUVBO0FBQ04sYUFBUSxHQUFSLENBQVksTUFBTSxFQUFsQjtBQUNBO0FBQ0QsMEJBQXNCLEtBQXRCLElBQStCLFFBQS9CO0FBQ0EsV0FBTyxRQUFQO0FBQ0E7QUF0QytCLEdBQW5DO0FBd0NBLFNBQU8sRUFBUDtBQUNBOztBQUVELFVBQVMsaUJBQVQsR0FBNkI7QUFDNUIsU0FBTyxXQUFXLEdBQVgsQ0FBZSxVQUFTLEtBQVQsRUFBZ0IsS0FBaEIsRUFBdUI7QUFDNUMsVUFBTyxpQkFBaUIsS0FBakIsRUFBd0IsS0FBeEIsQ0FBUDtBQUNBLEdBRk0sQ0FBUDtBQUdBOztBQUVELFVBQVMscUJBQVQsQ0FBK0IsS0FBL0IsRUFBc0M7QUFDckMsTUFBRyxNQUFNLFdBQU4sQ0FBa0IsV0FBbEIsQ0FBSCxFQUFtQztBQUMvQixPQUFJLGFBQWEsUUFBakI7QUFDRyxPQUFJLGVBQWUsTUFBTSxXQUFOLENBQWtCLFdBQWxCLEVBQStCLGtCQUEvQixFQUFuQjtBQUNOLGdCQUFhLGFBQWIsQ0FBMkIsVUFBM0I7QUFDTSxXQUFRLFdBQVcsaUJBQVgsQ0FBNkIsTUFBTSxDQUFOLENBQTdCLEVBQXNDLE1BQU0sQ0FBTixDQUF0QyxFQUErQyxNQUFNLENBQU4sS0FBVSxDQUF6RCxDQUFSO0FBQ047QUFDRCxTQUFPLE1BQU0sTUFBTixDQUFhLHFCQUFiLENBQW1DLEtBQW5DLENBQVA7QUFDQTs7QUFFRCxVQUFTLG1CQUFULENBQTZCLEtBQTdCLEVBQW9DO0FBQ25DLFVBQVEsTUFBTSxNQUFOLENBQWEsbUJBQWIsQ0FBaUMsS0FBakMsQ0FBUjtBQUNBLE1BQUcsTUFBTSxXQUFOLENBQWtCLFdBQWxCLENBQUgsRUFBbUM7QUFDL0IsT0FBSSxhQUFhLFFBQWpCO0FBQ0csT0FBSSxlQUFlLE1BQU0sV0FBTixDQUFrQixXQUFsQixFQUErQixrQkFBL0IsRUFBbkI7QUFDTixnQkFBYSxhQUFiLENBQTJCLFVBQTNCO0FBQ00sV0FBUSxXQUFXLFlBQVgsQ0FBd0IsS0FBeEIsQ0FBUjtBQUNOO0FBQ0QsU0FBTyxLQUFQO0FBQ0E7O0FBRUQsS0FBSSxVQUFVO0FBQ2IseUJBQXVCLHFCQURWO0FBRWIsdUJBQXFCOztBQUd0Qjs7QUFMYyxFQUFkLENBT0EsT0FBTyxPQUFPLE1BQVAsQ0FBYyxLQUFkLEVBQXFCLFlBQVksS0FBWixDQUFyQixFQUF5QyxPQUF6QyxDQUFQO0FBQ0E7O0FBRUQsT0FBTyxPQUFQLEdBQWlCLGFBQWpCOzs7OztBQzVHQSxJQUFJLGNBQWMsUUFBUSw0QkFBUixDQUFsQjtBQUNBLElBQUksV0FBVyxRQUFRLHlCQUFSLENBQWY7O0FBRUEsU0FBUyxZQUFULENBQXNCLE9BQXRCLEVBQStCLE1BQS9CLEVBQXVDOztBQUV0QyxLQUFJLFFBQVE7QUFDWCxVQUFRLE1BREc7QUFFWCxjQUFZO0FBRkQsRUFBWjs7QUFLQSxVQUFTLGlCQUFULEdBQTZCO0FBQzVCLFNBQU8sQ0FDTjtBQUNDLFNBQU0sTUFEUDtBQUVDLFVBQU8sU0FBUyxRQUFRLEVBQVIsQ0FBVyxDQUFwQixFQUF1QixNQUF2QjtBQUZSLEdBRE0sRUFLTjtBQUNDLFNBQU0sVUFEUDtBQUVDLFVBQU8sU0FBUyxRQUFRLEVBQVIsQ0FBVyxDQUFwQixFQUF1QixNQUF2QjtBQUZSLEdBTE0sQ0FBUDtBQVVBOztBQUVELEtBQUksVUFBVSxFQUFkOztBQUdBLFFBQU8sT0FBTyxNQUFQLENBQWMsT0FBZCxFQUF1QixZQUFZLEtBQVosQ0FBdkIsQ0FBUDtBQUNBOztBQUVELE9BQU8sT0FBUCxHQUFpQixZQUFqQjs7Ozs7QUM3QkEsSUFBSSxjQUFjLFFBQVEsNEJBQVIsQ0FBbEI7QUFDQSxJQUFJLFdBQVcsUUFBUSx5QkFBUixDQUFmOztBQUVBLFNBQVMsU0FBVCxDQUFtQixPQUFuQixFQUE0QixNQUE1QixFQUFvQzs7QUFFbkMsS0FBSSxRQUFRO0FBQ1gsVUFBUSxNQURHO0FBRVgsY0FBWTtBQUZELEVBQVo7O0FBS0EsVUFBUyxpQkFBVCxHQUE2QjtBQUM1QixTQUFPLENBQ047QUFDQyxTQUFNLE9BRFA7QUFFQyxVQUFPLFNBQVMsUUFBUSxDQUFqQixFQUFvQixNQUFwQjtBQUZSLEdBRE0sRUFLTjtBQUNDLFNBQU0sU0FEUDtBQUVDLFVBQU8sU0FBUyxRQUFRLENBQWpCLEVBQW9CLE1BQXBCO0FBRlIsR0FMTSxDQUFQO0FBVUE7O0FBRUQsS0FBSSxVQUFVLEVBQWQ7O0FBR0EsUUFBTyxPQUFPLE1BQVAsQ0FBYyxPQUFkLEVBQXVCLFlBQVksS0FBWixDQUF2QixDQUFQO0FBQ0E7O0FBRUQsT0FBTyxPQUFQLEdBQWlCLFNBQWpCOzs7OztBQzdCQSxJQUFJLGNBQWMsUUFBUSw0QkFBUixDQUFsQjtBQUNBLElBQUksV0FBVyxRQUFRLHlCQUFSLENBQWY7O0FBRUEsU0FBUyxpQkFBVCxDQUEyQixPQUEzQixFQUFvQyxNQUFwQyxFQUE0Qzs7QUFFM0MsS0FBSSxRQUFRO0FBQ1gsVUFBUSxNQURHO0FBRVgsY0FBWTtBQUZELEVBQVo7O0FBS0EsVUFBUyxpQkFBVCxHQUE2QjtBQUM1QixTQUFPLENBQ047QUFDQyxTQUFNLGFBRFA7QUFFQyxVQUFPLFNBQVMsUUFBUSxDQUFqQixFQUFvQixNQUFwQjtBQUZSLEdBRE0sRUFLTjtBQUNDLFNBQU0sV0FEUDtBQUVDLFVBQU8sU0FBUyxRQUFRLENBQWpCLEVBQW9CLE1BQXBCO0FBRlIsR0FMTSxFQVNOO0FBQ0MsU0FBTSxTQURQO0FBRUMsVUFBTyxTQUFTLFFBQVEsQ0FBakIsRUFBb0IsTUFBcEI7QUFGUixHQVRNLEVBYU47QUFDQyxTQUFNLGtCQURQO0FBRUMsVUFBTyxTQUFTLFFBQVEsQ0FBakIsRUFBb0IsTUFBcEI7QUFGUixHQWJNLEVBaUJOO0FBQ0MsU0FBTSxpQkFEUDtBQUVDLFVBQU8sU0FBUyxRQUFRLENBQWpCLEVBQW9CLE1BQXBCO0FBRlIsR0FqQk0sRUFxQk47QUFDQyxTQUFNLFFBRFA7QUFFQyxVQUFPLFNBQVMsUUFBUSxDQUFSLENBQVUsSUFBbkIsRUFBeUIsTUFBekI7QUFGUixHQXJCTSxDQUFQO0FBMEJBOztBQUVELEtBQUksVUFBVSxFQUFkOztBQUdBLFFBQU8sT0FBTyxNQUFQLENBQWMsT0FBZCxFQUF1QixZQUFZLEtBQVosQ0FBdkIsQ0FBUDtBQUNBOztBQUVELE9BQU8sT0FBUCxHQUFpQixpQkFBakI7Ozs7O0FDN0NBLElBQUksY0FBYyxRQUFRLDRCQUFSLENBQWxCO0FBQ0EsSUFBSSxXQUFXLFFBQVEseUJBQVIsQ0FBZjs7QUFFQSxTQUFTLG1CQUFULENBQTZCLE9BQTdCLEVBQXNDLE1BQXRDLEVBQThDOztBQUU3QyxLQUFJLFFBQVE7QUFDWCxVQUFRLE1BREc7QUFFWCxjQUFZO0FBRkQsRUFBWjs7QUFLQSxVQUFTLGlCQUFULEdBQTZCO0FBQzVCLFNBQU8sQ0FDTjtBQUNDLFNBQU0sYUFEUDtBQUVDLFVBQU8sU0FBUyxRQUFRLENBQWpCLEVBQW9CLE1BQXBCO0FBRlIsR0FETSxFQUtOO0FBQ0MsU0FBTSxXQURQO0FBRUMsVUFBTyxTQUFTLFFBQVEsQ0FBakIsRUFBb0IsTUFBcEI7QUFGUixHQUxNLEVBU047QUFDQyxTQUFNLFNBRFA7QUFFQyxVQUFPLFNBQVMsUUFBUSxDQUFqQixFQUFvQixNQUFwQjtBQUZSLEdBVE0sRUFhTjtBQUNDLFNBQU0sa0JBRFA7QUFFQyxVQUFPLFNBQVMsUUFBUSxDQUFqQixFQUFvQixNQUFwQjtBQUZSLEdBYk0sRUFpQk47QUFDQyxTQUFNLGlCQURQO0FBRUMsVUFBTyxTQUFTLFFBQVEsQ0FBakIsRUFBb0IsTUFBcEI7QUFGUixHQWpCTSxFQXFCTjtBQUNDLFNBQU0sUUFEUDtBQUVDLFVBQU8sU0FBUyxRQUFRLENBQVIsQ0FBVSxJQUFuQixFQUF5QixNQUF6QjtBQUZSLEdBckJNLEVBeUJOO0FBQ0MsU0FBTSxjQURQO0FBRUMsVUFBTyxTQUFTLFFBQVEsQ0FBakIsRUFBb0IsTUFBcEI7QUFGUixHQXpCTSxDQUFQO0FBOEJBOztBQUVELEtBQUksVUFBVSxFQUFkOztBQUdBLFFBQU8sT0FBTyxNQUFQLENBQWMsT0FBZCxFQUF1QixZQUFZLEtBQVosQ0FBdkIsQ0FBUDtBQUNBOztBQUVELE9BQU8sT0FBUCxHQUFpQixtQkFBakI7Ozs7O0FDakRBLElBQUksY0FBYyxRQUFRLDRCQUFSLENBQWxCO0FBQ0EsSUFBSSxXQUFXLFFBQVEseUJBQVIsQ0FBZjs7QUFFQSxTQUFTLFNBQVQsQ0FBbUIsT0FBbkIsRUFBNEIsTUFBNUIsRUFBb0M7O0FBRW5DLEtBQUksUUFBUTtBQUNYLFVBQVEsTUFERztBQUVYLGNBQVk7QUFGRCxFQUFaOztBQUtBLFVBQVMsT0FBVCxDQUFpQixLQUFqQixFQUF3QjtBQUN2QixXQUFTLFFBQVEsRUFBakIsRUFBcUIsUUFBckIsQ0FBOEIsS0FBOUI7QUFDQTs7QUFFRCxVQUFTLGlCQUFULEdBQTZCO0FBQzVCLFNBQU8sQ0FDTjtBQUNDLFNBQU0sTUFEUDtBQUVDLFVBQU0sU0FBUyxRQUFRLEVBQWpCLEVBQXFCLE1BQXJCO0FBRlAsR0FETSxDQUFQO0FBTUE7O0FBRUQsS0FBSSxVQUFVLEVBQWQ7O0FBR0EsUUFBTyxPQUFPLE1BQVAsQ0FBYyxPQUFkLEVBQXVCLFlBQVksS0FBWixDQUF2QixDQUFQO0FBQ0E7O0FBRUQsT0FBTyxPQUFQLEdBQWlCLFNBQWpCOzs7OztBQzdCQSxJQUFJLGNBQWMsUUFBUSw0QkFBUixDQUFsQjtBQUNBLElBQUksV0FBVyxRQUFRLHlCQUFSLENBQWY7O0FBRUEsU0FBUyxhQUFULENBQXVCLE9BQXZCLEVBQWdDLE1BQWhDLEVBQXdDOztBQUV2QyxLQUFJLFFBQVE7QUFDWCxVQUFRLE1BREc7QUFFWCxjQUFZO0FBRkQsRUFBWjs7QUFLQSxVQUFTLGlCQUFULEdBQTZCO0FBQzVCLFNBQU8sQ0FDTjtBQUNDLFNBQU0sUUFEUDtBQUVDLFVBQU8sU0FBUyxRQUFRLEVBQVIsQ0FBVyxFQUFwQixFQUF3QixNQUF4QjtBQUZSLEdBRE0sRUFLTjtBQUNDLFNBQU0sVUFEUDtBQUVDLFVBQU8sU0FBUyxRQUFRLEVBQVIsQ0FBVyxDQUFwQixFQUF1QixNQUF2QjtBQUZSLEdBTE0sRUFTTjtBQUNDLFNBQU0sVUFEUDtBQUVDLFVBQU8sU0FBUyxRQUFRLEVBQVIsQ0FBVyxDQUFwQixFQUF1QixNQUF2QjtBQUZSLEdBVE0sRUFhTjtBQUNDLFNBQU0sY0FEUDtBQUVDLFVBQU8sU0FBUyxRQUFRLEVBQVIsQ0FBVyxFQUFwQixFQUF3QixNQUF4QjtBQUZSLEdBYk0sRUFpQk47QUFDQyxTQUFNLGNBRFA7QUFFQyxVQUFPLFNBQVMsUUFBUSxFQUFSLENBQVcsRUFBcEIsRUFBd0IsTUFBeEI7QUFGUixHQWpCTSxFQXFCTjtBQUNDLFNBQU0saUJBRFA7QUFFQyxVQUFPLFNBQVMsUUFBUSxFQUFSLENBQVcsRUFBcEIsRUFBd0IsTUFBeEI7QUFGUixHQXJCTSxFQXlCTjtBQUNDLFNBQU0saUJBRFA7QUFFQyxVQUFPLFNBQVMsUUFBUSxFQUFSLENBQVcsRUFBcEIsRUFBd0IsTUFBeEI7QUFGUixHQXpCTSxDQUFQO0FBOEJBOztBQUVELEtBQUksVUFBVSxFQUFkOztBQUdBLFFBQU8sT0FBTyxNQUFQLENBQWMsT0FBZCxFQUF1QixZQUFZLEtBQVosQ0FBdkIsQ0FBUDtBQUNBOztBQUVELE9BQU8sT0FBUCxHQUFpQixhQUFqQjs7Ozs7QUNqREEsSUFBSSxjQUFjLFFBQVEsNEJBQVIsQ0FBbEI7QUFDQSxJQUFJLFdBQVcsUUFBUSx5QkFBUixDQUFmOztBQUVBLFNBQVMsY0FBVCxDQUF3QixPQUF4QixFQUFpQyxNQUFqQyxFQUF5Qzs7QUFFeEMsS0FBSSxRQUFRO0FBQ1gsVUFBUSxNQURHO0FBRVgsY0FBWTtBQUZELEVBQVo7O0FBS0EsVUFBUyxpQkFBVCxHQUE2QjtBQUM1QixTQUFPLENBQ047QUFDQyxTQUFNLE1BRFA7QUFFQyxVQUFPLFNBQVMsUUFBUSxFQUFSLENBQVcsQ0FBcEIsRUFBdUIsTUFBdkI7QUFGUixHQURNLEVBS047QUFDQyxTQUFNLFVBRFA7QUFFQyxVQUFPLFNBQVMsUUFBUSxFQUFSLENBQVcsQ0FBcEIsRUFBdUIsTUFBdkI7QUFGUixHQUxNLEVBU047QUFDQyxTQUFNLFdBRFA7QUFFQyxVQUFPLFNBQVMsUUFBUSxFQUFSLENBQVcsQ0FBcEIsRUFBdUIsTUFBdkI7QUFGUixHQVRNLENBQVA7QUFjQTs7QUFFRCxLQUFJLFVBQVUsRUFBZDs7QUFHQSxRQUFPLE9BQU8sTUFBUCxDQUFjLE9BQWQsRUFBdUIsWUFBWSxLQUFaLENBQXZCLENBQVA7QUFDQTs7QUFFRCxPQUFPLE9BQVAsR0FBaUIsY0FBakI7Ozs7O0FDakNBLElBQUksY0FBYyxRQUFRLDRCQUFSLENBQWxCO0FBQ0EsSUFBSSxXQUFXLFFBQVEseUJBQVIsQ0FBZjtBQUNBLElBQUksWUFBWSxRQUFRLHdCQUFSLENBQWhCOztBQUVBLFNBQVMsYUFBVCxDQUF1QixPQUF2QixFQUFnQyxNQUFoQyxFQUF3Qzs7QUFFdkMsS0FBSSxRQUFRO0FBQ1gsVUFBUSxNQURHO0FBRVgsY0FBWTtBQUZELEVBQVo7O0FBS0EsVUFBUyxpQkFBVCxHQUE2QjtBQUM1QixTQUFPLENBQ047QUFDQyxTQUFNLFFBRFA7QUFFQyxVQUFPLFNBQVMsUUFBUSxDQUFqQixFQUFvQixNQUFwQjtBQUZSLEdBRE0sRUFLTjtBQUNDLFNBQU0sUUFEUDtBQUVDLFVBQU8sU0FBUyxRQUFRLENBQWpCLEVBQW9CLE1BQXBCO0FBRlIsR0FMTSxFQVNOO0FBQ0MsU0FBTSxXQURQO0FBRUMsVUFBTyxVQUFVLFFBQVEsRUFBbEIsRUFBc0IsTUFBdEI7QUFGUixHQVRNLENBQVA7QUFjQTs7QUFFRCxLQUFJLFVBQVUsRUFBZDs7QUFHQSxRQUFPLE9BQU8sTUFBUCxDQUFjLE9BQWQsRUFBdUIsWUFBWSxLQUFaLENBQXZCLENBQVA7QUFDQTs7QUFFRCxPQUFPLE9BQVAsR0FBaUIsYUFBakI7Ozs7O0FDbENBLElBQUksY0FBYyxRQUFRLDRCQUFSLENBQWxCO0FBQ0EsSUFBSSxXQUFXLFFBQVEseUJBQVIsQ0FBZjs7QUFFQSxTQUFTLGlCQUFULENBQTJCLE9BQTNCLEVBQW9DLE1BQXBDLEVBQTRDOztBQUUzQyxLQUFJLFFBQVE7QUFDWCxVQUFRLE1BREc7QUFFWCxjQUFZO0FBRkQsRUFBWjs7QUFLQSxVQUFTLGlCQUFULEdBQTZCO0FBQzVCLFNBQU8sQ0FDTjtBQUNDLFNBQU0sUUFEUDtBQUVDLFVBQU8sU0FBUyxRQUFRLEVBQWpCLEVBQXFCLE1BQXJCO0FBRlIsR0FETSxDQUFQO0FBTUE7O0FBRUQsS0FBSSxVQUFVLEVBQWQ7O0FBR0EsUUFBTyxPQUFPLE1BQVAsQ0FBYyxPQUFkLEVBQXVCLFlBQVksS0FBWixDQUF2QixDQUFQO0FBQ0E7O0FBRUQsT0FBTyxPQUFQLEdBQWlCLGlCQUFqQjs7Ozs7QUN6QkEsSUFBSSxjQUFjLFFBQVEsNEJBQVIsQ0FBbEI7QUFDQSxJQUFJLFdBQVcsUUFBUSx5QkFBUixDQUFmOztBQUVBLFNBQVMsV0FBVCxDQUFxQixPQUFyQixFQUE4QixNQUE5QixFQUFzQztBQUNyQyxLQUFJLFFBQVE7QUFDWCxVQUFRLE1BREc7QUFFWCxjQUFZO0FBRkQsRUFBWjs7QUFLQSxVQUFTLGlCQUFULEdBQTZCO0FBQzVCLFNBQU8sQ0FDTjtBQUNDLFNBQU0sT0FEUDtBQUVDLFVBQU8sU0FBUyxRQUFRLENBQWpCLEVBQW9CLE1BQXBCO0FBRlIsR0FETSxFQUtOO0FBQ0MsU0FBTSxjQURQO0FBRUMsVUFBTyxTQUFTLFFBQVEsQ0FBakIsRUFBb0IsTUFBcEI7QUFGUixHQUxNLEVBU047QUFDQyxTQUFNLFNBRFA7QUFFQyxVQUFPLFNBQVMsUUFBUSxDQUFqQixFQUFvQixNQUFwQjtBQUZSLEdBVE0sQ0FBUDtBQWNBOztBQUVELEtBQUksVUFBVSxFQUFkOztBQUdBLFFBQU8sT0FBTyxNQUFQLENBQWMsT0FBZCxFQUF1QixZQUFZLEtBQVosQ0FBdkIsQ0FBUDtBQUNBOztBQUVELE9BQU8sT0FBUCxHQUFpQixXQUFqQjs7Ozs7QUNoQ0EsSUFBSSxjQUFjLFFBQVEsNEJBQVIsQ0FBbEI7QUFDQSxJQUFJLFdBQVcsUUFBUSx5QkFBUixDQUFmOztBQUVBLFNBQVMsY0FBVCxDQUF3QixPQUF4QixFQUFpQyxNQUFqQyxFQUF5Qzs7QUFFeEMsS0FBSSxRQUFRO0FBQ1gsVUFBUSxNQURHO0FBRVgsY0FBWTtBQUZELEVBQVo7O0FBS0EsVUFBUyxpQkFBVCxHQUE2QjtBQUM1QixTQUFPLENBQ047QUFDQyxTQUFNLE9BRFA7QUFFQyxVQUFPLFNBQVMsUUFBUSxDQUFqQixFQUFvQixNQUFwQjtBQUZSLEdBRE0sRUFLTjtBQUNDLFNBQU0sS0FEUDtBQUVDLFVBQU8sU0FBUyxRQUFRLENBQWpCLEVBQW9CLE1BQXBCO0FBRlIsR0FMTSxFQVNOO0FBQ0MsU0FBTSxRQURQO0FBRUMsVUFBTyxTQUFTLFFBQVEsQ0FBakIsRUFBb0IsTUFBcEI7QUFGUixHQVRNLENBQVA7QUFjQTs7QUFFRCxLQUFJLFVBQVUsRUFBZDs7QUFHQSxRQUFPLE9BQU8sTUFBUCxDQUFjLE9BQWQsRUFBdUIsWUFBWSxLQUFaLENBQXZCLENBQVA7QUFDQTs7QUFFRCxPQUFPLE9BQVAsR0FBaUIsY0FBakI7Ozs7O0FDakNBLElBQUksWUFBWSxRQUFRLGNBQVIsQ0FBaEI7O0FBRUEsU0FBUyxLQUFULENBQWUsT0FBZixFQUF3QixNQUF4QixFQUFnQzs7QUFFL0IsS0FBSSxRQUFRO0FBQ1gsV0FBUyxPQURFO0FBRVgsVUFBUSxNQUZHO0FBR1gsY0FBWTtBQUhELEVBQVo7O0FBTUEsVUFBUyxpQkFBVCxHQUE2QjtBQUM1QixTQUFPLEVBQVA7QUFFQTs7QUFFRCxLQUFJLFVBQVUsRUFBZDs7QUFHQSxRQUFPLE9BQU8sTUFBUCxDQUFjLEVBQWQsRUFBa0IsVUFBVSxLQUFWLENBQWxCLEVBQW9DLE9BQXBDLENBQVA7QUFDQTs7QUFFRCxPQUFPLE9BQVAsR0FBaUIsS0FBakI7Ozs7O0FDckJBLElBQUksY0FBYyxRQUFRLDRCQUFSLENBQWxCO0FBQ0EsSUFBSSxXQUFXLFFBQVEseUJBQVIsQ0FBZjtBQUNBLElBQUksZUFBZSxRQUFRLGdCQUFSLENBQW5COztBQUVBLFNBQVMsSUFBVCxDQUFjLE9BQWQsRUFBdUIsTUFBdkIsRUFBK0I7O0FBRTlCLEtBQUksV0FBVyxFQUFmOztBQUVBLEtBQUksUUFBUTtBQUNYLFdBQVMsT0FERTtBQUVYLFVBQVEsTUFGRztBQUdYLGNBQVk7QUFIRCxFQUFaOztBQU1BLFVBQVMsZUFBVCxDQUF5QixTQUF6QixFQUFvQztBQUNuQyxNQUFJLGFBQUo7QUFDQSxjQUFZLFlBQVc7QUFDdEIsT0FBSSxXQUFXLFVBQVUsUUFBUSxZQUFSLENBQXFCLFdBQS9CLENBQWY7QUFDQSxPQUFJLGtCQUFrQixRQUF0QixFQUFnQztBQUMvQixZQUFRLGtCQUFSLENBQTJCLFFBQTNCO0FBQ0E7QUFDRCxHQUxELEVBS0csR0FMSDtBQU1BLFVBQVEsR0FBUixDQUFZLE9BQVo7QUFDQTs7QUFFRCxVQUFTLFlBQVQsR0FBd0I7QUFDdkIsTUFBSSxxQkFBcUIsRUFBekI7QUFDQSxNQUFJLFlBQVksUUFBUSxZQUFSLENBQXFCLGNBQXJDO0FBQ0EsTUFBSSxDQUFKO0FBQUEsTUFBTyxNQUFNLFVBQVUsTUFBdkI7QUFDQSxNQUFJLFlBQUo7QUFDQSxPQUFLLElBQUksQ0FBVCxFQUFZLElBQUksR0FBaEIsRUFBcUIsS0FBSyxDQUExQixFQUE2QjtBQUM1QixrQkFBZSxhQUFhLFVBQVUsQ0FBVixDQUFiLENBQWY7QUFDQSxzQkFBbUIsSUFBbkIsQ0FBd0I7QUFDdkIsVUFBTSxRQUFRLFlBQVIsQ0FBcUIsU0FBckIsQ0FBK0IsQ0FBL0IsQ0FBaUMsQ0FBakMsRUFBb0MsRUFBcEMsSUFBMEMsZUFBZSxJQUFFLENBQWpCLENBRHpCLEVBQzhDO0FBQ3JFLFdBQU87QUFGZ0IsSUFBeEI7QUFJQTtBQUNELFNBQU8sa0JBQVA7QUFDQTs7QUFFRCxVQUFTLGlCQUFULEdBQTZCO0FBQzVCLFNBQU8sQ0FDTjtBQUNDLFNBQUssUUFETjtBQUVDLFVBQU87QUFDTixjQUFVO0FBREo7QUFGUixHQURNLEVBT0wsTUFQSyxDQU9FLGNBUEYsQ0FBUDtBQVFBOztBQUVELEtBQUksVUFBVSxFQUFkOztBQUdBLFFBQU8sT0FBTyxNQUFQLENBQWMsUUFBZCxFQUF3QixPQUF4QixFQUFpQyxZQUFZLEtBQVosQ0FBakMsQ0FBUDtBQUVBOztBQUVELE9BQU8sT0FBUCxHQUFpQixJQUFqQjs7Ozs7QUMxREEsSUFBSSxjQUFjLFFBQVEsNEJBQVIsQ0FBbEI7QUFDQSxJQUFJLFdBQVcsUUFBUSx5QkFBUixDQUFmOztBQUVBLFNBQVMsWUFBVCxDQUFzQixRQUF0QixFQUFnQzs7QUFFL0IsS0FBSSxXQUFXLEVBQWY7O0FBRUEsS0FBSSxRQUFRO0FBQ1gsY0FBWTtBQURELEVBQVo7O0FBSUEsVUFBUyxjQUFULENBQXdCLEtBQXhCLEVBQStCO0FBQzlCLFdBQVMsU0FBUyxDQUFULENBQVcsQ0FBcEIsRUFBdUIsUUFBdkIsQ0FBZ0MsS0FBaEM7QUFDQTs7QUFFRCxVQUFTLGlCQUFULENBQTJCLEtBQTNCLEVBQWtDO0FBQ2pDLFdBQVMsU0FBUyxDQUFULENBQVcsRUFBcEIsRUFBd0IsUUFBeEIsQ0FBaUMsS0FBakM7QUFDQTs7QUFFRCxVQUFTLFlBQVQsQ0FBc0IsS0FBdEIsRUFBNkI7QUFDNUIsV0FBUyxTQUFTLENBQVQsQ0FBVyxFQUFwQixFQUF3QixRQUF4QixDQUFpQyxLQUFqQztBQUNBOztBQUVELFVBQVMsVUFBVCxDQUFvQixLQUFwQixFQUEyQjtBQUMxQixXQUFTLFNBQVMsQ0FBVCxDQUFXLEVBQXBCLEVBQXdCLFFBQXhCLENBQWlDLEtBQWpDO0FBQ0E7O0FBRUQsVUFBUyxpQkFBVCxDQUEyQixLQUEzQixFQUFrQztBQUNqQyxXQUFTLFNBQVMsQ0FBVCxDQUFXLEVBQXBCLEVBQXdCLFFBQXhCLENBQWlDLEtBQWpDO0FBQ0E7O0FBRUQsVUFBUyxjQUFULENBQXdCLEtBQXhCLEVBQStCO0FBQzlCLFdBQVMsU0FBUyxDQUFULENBQVcsRUFBcEIsRUFBd0IsUUFBeEIsQ0FBaUMsS0FBakM7QUFDQTs7QUFFRCxVQUFTLFVBQVQsQ0FBb0IsS0FBcEIsRUFBMkI7QUFDMUIsV0FBUyxTQUFTLENBQVQsQ0FBVyxDQUFwQixFQUF1QixRQUF2QixDQUFnQyxLQUFoQztBQUNBOztBQUVELFVBQVMsV0FBVCxDQUFxQixLQUFyQixFQUE0QjtBQUMzQixXQUFTLFNBQVMsQ0FBVCxDQUFXLENBQXBCLEVBQXVCLFFBQXZCLENBQWdDLEtBQWhDO0FBQ0E7O0FBRUQsVUFBUyxXQUFULENBQXFCLEtBQXJCLEVBQTRCO0FBQzNCLFdBQVMsU0FBUyxDQUFULENBQVcsQ0FBcEIsRUFBdUIsUUFBdkIsQ0FBZ0MsS0FBaEM7QUFDQTs7QUFFRCxVQUFTLFlBQVQsQ0FBc0IsS0FBdEIsRUFBNkI7QUFDNUIsV0FBUyxTQUFTLENBQVQsQ0FBVyxFQUFwQixFQUF3QixRQUF4QixDQUFpQyxLQUFqQztBQUNBOztBQUVELFVBQVMsWUFBVCxDQUFzQixLQUF0QixFQUE2QjtBQUM1QixXQUFTLFNBQVMsQ0FBVCxDQUFXLEVBQXBCLEVBQXdCLFFBQXhCLENBQWlDLEtBQWpDO0FBQ0E7O0FBRUQsVUFBUyxRQUFULENBQWtCLEtBQWxCLEVBQXlCO0FBQ3hCLFdBQVMsU0FBUyxDQUFULENBQVcsQ0FBcEIsRUFBdUIsUUFBdkIsQ0FBZ0MsS0FBaEM7QUFDQTs7QUFFRCxVQUFTLFdBQVQsQ0FBcUIsS0FBckIsRUFBNEI7QUFDM0IsV0FBUyxTQUFTLENBQVQsQ0FBVyxFQUFwQixFQUF3QixRQUF4QixDQUFpQyxLQUFqQztBQUNBOztBQUVELFVBQVMsY0FBVCxDQUF3QixLQUF4QixFQUErQjtBQUM5QixXQUFTLFNBQVMsQ0FBVCxDQUFXLEVBQXBCLEVBQXdCLFFBQXhCLENBQWlDLEtBQWpDO0FBQ0E7O0FBRUQsVUFBUyxPQUFULENBQWlCLEtBQWpCLEVBQXdCO0FBQ3ZCLFdBQVMsU0FBUyxDQUFULENBQVcsRUFBcEIsRUFBd0IsUUFBeEIsQ0FBaUMsS0FBakM7QUFDQTs7QUFFRCxVQUFTLGdCQUFULENBQTBCLEtBQTFCLEVBQWlDO0FBQ2hDLFdBQVMsU0FBUyxDQUFULENBQVcsRUFBcEIsRUFBd0IsUUFBeEIsQ0FBaUMsS0FBakM7QUFDQTs7QUFFRCxVQUFTLGNBQVQsQ0FBd0IsS0FBeEIsRUFBK0I7QUFDOUIsV0FBUyxTQUFTLENBQVQsQ0FBVyxFQUFwQixFQUF3QixRQUF4QixDQUFpQyxLQUFqQztBQUNBOztBQUVELFVBQVMsbUJBQVQsQ0FBNkIsS0FBN0IsRUFBb0M7QUFDbkMsV0FBUyxTQUFTLENBQVQsQ0FBVyxFQUFwQixFQUF3QixRQUF4QixDQUFpQyxLQUFqQztBQUNBOztBQUVELFVBQVMsWUFBVCxDQUFzQixLQUF0QixFQUE2QjtBQUM1QixXQUFTLFNBQVMsQ0FBVCxDQUFXLEVBQXBCLEVBQXdCLFFBQXhCLENBQWlDLEtBQWpDO0FBQ0E7O0FBRUQsVUFBUyxtQkFBVCxDQUE2QixLQUE3QixFQUFvQztBQUNuQyxXQUFTLFNBQVMsQ0FBVCxDQUFXLEVBQXBCLEVBQXdCLFFBQXhCLENBQWlDLEtBQWpDO0FBQ0E7O0FBRUQsVUFBUyxpQkFBVCxDQUEyQixLQUEzQixFQUFrQztBQUNqQyxXQUFTLFNBQVMsQ0FBVCxDQUFXLENBQXBCLEVBQXVCLFFBQXZCLENBQWdDLEtBQWhDO0FBQ0E7O0FBRUQsVUFBUyxpQkFBVCxHQUE2QjtBQUM1QixTQUFPLENBQ047QUFDQyxTQUFLLGNBRE47QUFFQyxVQUFPO0FBQ04sY0FBVTtBQURKO0FBRlIsR0FETSxFQU9OO0FBQ0MsU0FBSyxpQkFETjtBQUVDLFVBQU87QUFDTixjQUFVO0FBREo7QUFGUixHQVBNLEVBYU47QUFDQyxTQUFLLFlBRE47QUFFQyxVQUFPO0FBQ04sY0FBVTtBQURKO0FBRlIsR0FiTSxFQW1CTjtBQUNDLFNBQUssVUFETjtBQUVDLFVBQU87QUFDTixjQUFVO0FBREo7QUFGUixHQW5CTSxFQXlCTjtBQUNDLFNBQUssaUJBRE47QUFFQyxVQUFPO0FBQ04sY0FBVTtBQURKO0FBRlIsR0F6Qk0sRUErQk47QUFDQyxTQUFLLGNBRE47QUFFQyxVQUFPO0FBQ04sY0FBVTtBQURKO0FBRlIsR0EvQk0sRUFxQ047QUFDQyxTQUFLLFNBRE47QUFFQyxVQUFPO0FBQ04sY0FBVTtBQURKO0FBRlIsR0FyQ00sRUEyQ047QUFDQyxTQUFLLFVBRE47QUFFQyxVQUFPO0FBQ04sY0FBVTtBQURKO0FBRlIsR0EzQ00sRUFpRE47QUFDQyxTQUFLLFlBRE47QUFFQyxVQUFPO0FBQ04sY0FBVTtBQURKO0FBRlIsR0FqRE0sRUF1RE47QUFDQyxTQUFLLFlBRE47QUFFQyxVQUFPO0FBQ04sY0FBVTtBQURKO0FBRlIsR0F2RE0sRUE2RE47QUFDQyxTQUFLLE9BRE47QUFFQyxVQUFPO0FBQ04sY0FBVTtBQURKO0FBRlIsR0E3RE0sRUFtRU47QUFDQyxTQUFLLFdBRE47QUFFQyxVQUFPO0FBQ04sY0FBVTtBQURKO0FBRlIsR0FuRU0sRUF5RU47QUFDQyxTQUFLLGNBRE47QUFFQyxVQUFPO0FBQ04sY0FBVTtBQURKO0FBRlIsR0F6RU0sRUErRU47QUFDQyxTQUFLLE1BRE47QUFFQyxVQUFPO0FBQ04sY0FBVTtBQURKO0FBRlIsR0EvRU0sRUFxRk47QUFDQyxTQUFLLGNBRE47QUFFQyxVQUFPO0FBQ04sY0FBVTtBQURKO0FBRlIsR0FyRk0sRUEyRk47QUFDQyxTQUFLLGlCQUROO0FBRUMsVUFBTztBQUNOLGNBQVU7QUFESjtBQUZSLEdBM0ZNLEVBaUdOO0FBQ0MsU0FBSyxnQkFETjtBQUVDLFVBQU87QUFDTixjQUFVO0FBREo7QUFGUixHQWpHTSxFQXVHTjtBQUNDLFNBQUssbUJBRE47QUFFQyxVQUFPO0FBQ04sY0FBVTtBQURKO0FBRlIsR0F2R00sRUE2R047QUFDQyxTQUFLLG1CQUROO0FBRUMsVUFBTztBQUNOLGNBQVU7QUFESjtBQUZSLEdBN0dNLEVBbUhOO0FBQ0MsU0FBSyxZQUROO0FBRUMsVUFBTztBQUNOLGNBQVU7QUFESjtBQUZSLEdBbkhNLENBQVA7QUEySEE7O0FBRUQsS0FBSSxVQUFVLEVBQWQ7O0FBR0EsUUFBTyxPQUFPLE1BQVAsQ0FBYyxRQUFkLEVBQXdCLE9BQXhCLEVBQWlDLFlBQVksS0FBWixDQUFqQyxDQUFQO0FBRUE7O0FBRUQsT0FBTyxPQUFQLEdBQWlCLFlBQWpCOzs7OztBQ3BPQSxJQUFJLFlBQVksUUFBUSxjQUFSLENBQWhCO0FBQ0EsSUFBSSxPQUFPLFFBQVEsUUFBUixDQUFYOztBQUVBLFNBQVMsV0FBVCxDQUFxQixPQUFyQixFQUE4Qjs7QUFFN0IsS0FBSSxXQUFXLEVBQWY7O0FBRUEsS0FBSSxlQUFlLEtBQUssT0FBTCxDQUFuQjtBQUNBLEtBQUksUUFBUTtBQUNYLFdBQVMsT0FERTtBQUVYLGNBQVk7QUFGRCxFQUFaOztBQUtBLFVBQVMsaUJBQVQsR0FBNkI7QUFDNUIsU0FBTyxDQUNOO0FBQ0MsU0FBTSxNQURQO0FBRUMsVUFBTztBQUZSLEdBRE0sRUFLTjtBQUNDLFNBQU0sTUFEUDtBQUVDLFVBQU87QUFGUixHQUxNLENBQVA7QUFVQTs7QUFFRCxVQUFTLE9BQVQsR0FBbUI7QUFDbEIsU0FBTyxRQUFRLFlBQVIsQ0FBcUIsV0FBckIsQ0FBaUMsQ0FBeEM7QUFDQTs7QUFFRCxVQUFTLE9BQVQsQ0FBaUIsS0FBakIsRUFBd0IsS0FBeEIsRUFBK0I7QUFDOUIsa0JBQWdCLEVBQUMsR0FBRyxLQUFKLEVBQWhCLEVBQTRCLEtBQTVCO0FBQ0E7O0FBRUQsVUFBUyxlQUFULENBQXlCLElBQXpCLEVBQStCLEtBQS9CLEVBQXNDO0FBQ3JDLFNBQU8sUUFBUSxrQkFBUixDQUEyQixJQUEzQixFQUFpQyxLQUFqQyxDQUFQO0FBQ0E7O0FBRUQsVUFBUyxhQUFULENBQXVCLFVBQXZCLEVBQW1DO0FBQ2xDLFNBQU8sUUFBUSxhQUFSLENBQXNCLFVBQXRCLENBQVA7QUFDQTs7QUFFRCxVQUFTLGtCQUFULENBQTRCLFNBQTVCLEVBQXVDO0FBQ3RDLFNBQU8sUUFBUSxrQkFBUixDQUEyQixTQUEzQixDQUFQO0FBQ0E7O0FBRUQsS0FBSSxVQUFVO0FBQ2IsV0FBUyxPQURJO0FBRWIsV0FBUyxPQUZJO0FBR2IsaUJBQWUsYUFIRjtBQUliLG1CQUFpQixlQUpKO0FBS2Isc0JBQW9CO0FBTFAsRUFBZDs7QUFRQSxRQUFPLE9BQU8sTUFBUCxDQUFjLFFBQWQsRUFBd0IsVUFBVSxLQUFWLENBQXhCLEVBQTBDLE9BQTFDLENBQVA7QUFFQTs7QUFFRCxPQUFPLE9BQVAsR0FBaUIsV0FBakI7Ozs7O0FDMURBLElBQUksY0FBYyxRQUFRLDRCQUFSLENBQWxCO0FBQ0EsSUFBSSxXQUFXLFFBQVEseUJBQVIsQ0FBZjs7QUFFQSxTQUFTLFNBQVQsQ0FBbUIsS0FBbkIsRUFBMEIsTUFBMUIsRUFBa0M7QUFDakMsS0FBSSxRQUFRO0FBQ1gsY0FBWTtBQURELEVBQVo7O0FBSUEsVUFBUyxpQkFBVCxHQUE2QjtBQUM1QixTQUFPLENBQ047QUFDQyxTQUFNLGNBRFA7QUFFQyxVQUFPLFNBQVMsTUFBTSxDQUFmLEVBQWtCLE1BQWxCO0FBRlIsR0FETSxFQUtOO0FBQ0MsU0FBTSxtQkFEUDtBQUVDLFVBQU8sU0FBUyxNQUFNLENBQWYsRUFBa0IsTUFBbEI7QUFGUixHQUxNLEVBU047QUFDQyxTQUFNLFVBRFA7QUFFQyxVQUFPLFNBQVMsTUFBTSxDQUFmLEVBQWtCLE1BQWxCO0FBRlIsR0FUTSxFQWFOO0FBQ0MsU0FBTSxPQURQO0FBRUMsVUFBTyxTQUFTLE1BQU0sQ0FBZixFQUFrQixNQUFsQjtBQUZSLEdBYk0sRUFpQk47QUFDQyxTQUFNLFVBRFA7QUFFQyxVQUFPLFNBQVMsTUFBTSxDQUFmLEVBQWtCLE1BQWxCO0FBRlIsR0FqQk0sRUFxQk47QUFDQyxTQUFNLFlBRFA7QUFFQyxVQUFPLFNBQVMsTUFBTSxFQUFmLEVBQW1CLE1BQW5CO0FBRlIsR0FyQk0sRUF5Qk47QUFDQyxTQUFNLFlBRFA7QUFFQyxVQUFPLFNBQVMsTUFBTSxFQUFmLEVBQW1CLE1BQW5CO0FBRlIsR0F6Qk0sRUE2Qk47QUFDQyxTQUFNLFlBRFA7QUFFQyxVQUFPLFNBQVMsTUFBTSxFQUFmLEVBQW1CLE1BQW5CO0FBRlIsR0E3Qk0sRUFpQ047QUFDQyxTQUFNLFlBRFA7QUFFQyxVQUFPLFNBQVMsTUFBTSxFQUFmLEVBQW1CLE1BQW5CO0FBRlIsR0FqQ00sRUFxQ047QUFDQyxTQUFNLFlBRFA7QUFFQyxVQUFPLFNBQVMsTUFBTSxFQUFmLEVBQW1CLE1BQW5CO0FBRlIsR0FyQ00sRUF5Q047QUFDQyxTQUFNLFlBRFA7QUFFQyxVQUFPLFNBQVMsTUFBTSxFQUFmLEVBQW1CLE1BQW5CO0FBRlIsR0F6Q00sRUE2Q047QUFDQyxTQUFNLFNBRFA7QUFFQyxVQUFPLFNBQVMsTUFBTSxDQUFmLEVBQWtCLE1BQWxCO0FBRlIsR0E3Q00sQ0FBUDtBQWtEQTs7QUFFRCxVQUFTLGtCQUFULEdBQThCO0FBQzdCLFNBQU8sS0FBUDtBQUNBOztBQUVELEtBQUksVUFBVTtBQUNiLHNCQUFvQjtBQURQLEVBQWQ7O0FBSUEsUUFBTyxPQUFPLE1BQVAsQ0FBYyxPQUFkLEVBQXVCLFlBQVksS0FBWixDQUF2QixDQUFQO0FBQ0E7O0FBRUQsT0FBTyxPQUFQLEdBQWlCLFNBQWpCOzs7OztBQ3hFQSxJQUFJLGNBQWMsUUFBUSx5QkFBUixDQUFsQjtBQUNBLElBQUksZ0JBQWdCLFFBQVEsaUJBQVIsQ0FBcEI7O0FBRUEsU0FBUyxRQUFULENBQWtCLFFBQWxCLEVBQTRCLE1BQTVCLEVBQW9DO0FBQ25DLEtBQUksUUFBUTtBQUNYLFlBQVUsUUFEQztBQUVYLFVBQVE7QUFGRyxFQUFaOztBQUtBLFVBQVMsT0FBVCxHQUFtQjtBQUNsQixRQUFNLFFBQU4sR0FBaUIsSUFBakI7QUFDQSxRQUFNLE1BQU4sR0FBZSxJQUFmO0FBQ0E7O0FBRUQsS0FBSSxVQUFVO0FBQ2IsV0FBUztBQURJLEVBQWQ7O0FBSUEsUUFBTyxPQUFPLE1BQVAsQ0FBYyxFQUFkLEVBQWtCLE9BQWxCLEVBQTJCLGNBQWMsS0FBZCxDQUEzQixFQUFpRCxZQUFZLEtBQVosQ0FBakQsQ0FBUDtBQUNBOztBQUVELE9BQU8sT0FBUCxHQUFpQixRQUFqQjs7Ozs7OztBQ3JCQSxTQUFTLGFBQVQsQ0FBdUIsS0FBdkIsRUFBOEI7O0FBRTdCLFVBQVMsUUFBVCxDQUFrQixLQUFsQixFQUF5QjtBQUN4QixNQUFJLFdBQVcsTUFBTSxRQUFyQjtBQUNBLE1BQUcsQ0FBQyxRQUFELElBQWEsQ0FBQyxTQUFTLFNBQTFCLEVBQXFDO0FBQ3BDO0FBQ0E7QUFDRCxNQUFJLE9BQU8sS0FBUCxLQUFpQixVQUFyQixFQUFpQztBQUNoQyxVQUFPLFNBQVMsU0FBVCxDQUFtQixLQUFuQixDQUFQO0FBQ0EsR0FGRCxNQUVPLElBQUksU0FBUyxRQUFULEtBQXNCLGtCQUF0QixJQUE0QyxRQUFPLEtBQVAseUNBQU8sS0FBUCxPQUFpQixRQUE3RCxJQUF5RSxNQUFNLE1BQU4sS0FBaUIsQ0FBOUYsRUFBaUc7QUFDdkcsVUFBTyxTQUFTLFNBQVQsQ0FBbUIsWUFBVTtBQUFDLFdBQU8sS0FBUDtBQUFhLElBQTNDLENBQVA7QUFDQSxHQUZNLE1BRUEsSUFBSSxTQUFTLFFBQVQsS0FBc0IsZ0JBQXRCLElBQTBDLE9BQU8sS0FBUCxLQUFpQixRQUEvRCxFQUF5RTtBQUMvRSxVQUFPLFNBQVMsU0FBVCxDQUFtQixZQUFVO0FBQUMsV0FBTyxLQUFQO0FBQWEsSUFBM0MsQ0FBUDtBQUNBO0FBQ0Q7O0FBRUQsVUFBUyxRQUFULEdBQW9CO0FBQ25CLFNBQU8sTUFBTSxRQUFOLENBQWUsQ0FBdEI7QUFDQTs7QUFFRCxLQUFJLFVBQVU7QUFDYixZQUFVLFFBREc7QUFFYixZQUFVO0FBRkcsRUFBZDs7QUFLQSxRQUFPLE9BQVA7QUFDQTs7QUFFRCxPQUFPLE9BQVAsR0FBaUIsYUFBakI7Ozs7O0FDNUJBLElBQUksWUFBWSxRQUFRLG9CQUFSLENBQWhCO0FBQ0EsSUFBSSxjQUFjLFFBQVEseUJBQVIsQ0FBbEI7O0FBRUEsU0FBUyxRQUFULENBQWtCLEtBQWxCLEVBQXlCOztBQUV4QixPQUFNLEtBQU4sR0FBYyxVQUFkOztBQUVBLFVBQVMsZUFBVCxHQUEyQjtBQUMxQixTQUFPLE1BQU0sU0FBTixDQUFnQixRQUF2QjtBQUNBOztBQUVELFFBQU8sT0FBTyxNQUFQLENBQWM7QUFDcEIsbUJBQWlCO0FBREcsRUFBZCxFQUVKLFVBQVUsTUFBTSxRQUFoQixDQUZJLEVBRXVCLFlBQVksTUFBTSxRQUFsQixFQUE0QixVQUE1QixDQUZ2QixDQUFQO0FBR0E7O0FBRUQsT0FBTyxPQUFQLEdBQWlCLFFBQWpCIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsInZhciBSZW5kZXJlciA9IHJlcXVpcmUoJy4uL3JlbmRlcmVyL1JlbmRlcmVyJyk7XG52YXIgbGF5ZXJfYXBpID0gcmVxdWlyZSgnLi4vaGVscGVycy9sYXllckFQSUJ1aWxkZXInKTtcblxuZnVuY3Rpb24gQW5pbWF0aW9uSXRlbUZhY3RvcnkoYW5pbWF0aW9uKSB7XG5cblx0dmFyIHN0YXRlID0ge1xuXHRcdGFuaW1hdGlvbjogYW5pbWF0aW9uLFxuXHRcdGVsZW1lbnRzOiBhbmltYXRpb24ucmVuZGVyZXIuZWxlbWVudHMubWFwKChpdGVtKSA9PiBsYXllcl9hcGkoaXRlbSwgYW5pbWF0aW9uKSksXG5cdFx0Ym91bmRpbmdSZWN0OiBudWxsLFxuXHRcdHNjYWxlRGF0YTogbnVsbFxuXHR9XG5cblx0ZnVuY3Rpb24gZ2V0Q3VycmVudEZyYW1lKCkge1xuXHRcdHJldHVybiBhbmltYXRpb24uY3VycmVudEZyYW1lO1xuXHR9XG5cblx0ZnVuY3Rpb24gZ2V0Q3VycmVudFRpbWUoKSB7XG5cdFx0cmV0dXJuIGFuaW1hdGlvbi5jdXJyZW50RnJhbWUgLyBhbmltYXRpb24uZnJhbWVSYXRlO1xuXHR9XG5cblx0ZnVuY3Rpb24gYWRkVmFsdWVDYWxsYmFjayhwcm9wZXJ0aWVzLCB2YWx1ZSkge1xuXHRcdHZhciBpLCBsZW4gPSBwcm9wZXJ0aWVzLmxlbmd0aDtcblx0XHRmb3IgKGkgPSAwOyBpIDwgbGVuOyBpICs9IDEpIHtcblx0XHRcdHByb3BlcnRpZXMuZ2V0UHJvcGVydHlBdEluZGV4KGkpLnNldFZhbHVlKHZhbHVlKTtcblx0XHR9XG5cdH1cblxuXHRmdW5jdGlvbiB0b0tleXBhdGhMYXllclBvaW50KHByb3BlcnRpZXMsIHBvaW50KSB7XG5cdFx0dmFyIGksIGxlbiA9IHByb3BlcnRpZXMubGVuZ3RoO1xuXHRcdHZhciBwb2ludHMgPSBbXTtcblx0XHRmb3IgKGkgPSAwOyBpIDwgbGVuOyBpICs9IDEpIHtcblx0XHRcdHBvaW50cy5wdXNoKHByb3BlcnRpZXMuZ2V0UHJvcGVydHlBdEluZGV4KGkpLnRvS2V5cGF0aExheWVyUG9pbnQocG9pbnQpKTtcblx0XHR9XG5cdFx0aWYocG9pbnRzLmxlbmd0aCA9PT0gMSkge1xuXHRcdFx0cmV0dXJuIHBvaW50c1swXTtcblx0XHR9XG5cdFx0cmV0dXJuIHBvaW50cztcblx0fVxuXG5cdGZ1bmN0aW9uIGZyb21LZXlwYXRoTGF5ZXJQb2ludChwcm9wZXJ0aWVzLCBwb2ludCkge1xuXHRcdHZhciBpLCBsZW4gPSBwcm9wZXJ0aWVzLmxlbmd0aDtcblx0XHR2YXIgcG9pbnRzID0gW107XG5cdFx0Zm9yIChpID0gMDsgaSA8IGxlbjsgaSArPSAxKSB7XG5cdFx0XHRwb2ludHMucHVzaChwcm9wZXJ0aWVzLmdldFByb3BlcnR5QXRJbmRleChpKS5mcm9tS2V5cGF0aExheWVyUG9pbnQocG9pbnQpKTtcblx0XHR9XG5cdFx0aWYocG9pbnRzLmxlbmd0aCA9PT0gMSkge1xuXHRcdFx0cmV0dXJuIHBvaW50c1swXTtcblx0XHR9XG5cdFx0cmV0dXJuIHBvaW50cztcblx0fVxuXG5cdGZ1bmN0aW9uIGNhbGN1bGF0ZVNjYWxlRGF0YShib3VuZGluZ1JlY3QpIHtcblx0XHR2YXIgY29tcFdpZHRoID0gYW5pbWF0aW9uLmFuaW1hdGlvbkRhdGEudztcbiAgICAgICAgdmFyIGNvbXBIZWlnaHQgPSBhbmltYXRpb24uYW5pbWF0aW9uRGF0YS5oO1xuXHRcdHZhciBjb21wUmVsID0gY29tcFdpZHRoIC8gY29tcEhlaWdodDtcbiAgICAgICAgdmFyIGVsZW1lbnRXaWR0aCA9IGJvdW5kaW5nUmVjdC53aWR0aDtcbiAgICAgICAgdmFyIGVsZW1lbnRIZWlnaHQgPSBib3VuZGluZ1JlY3QuaGVpZ2h0O1xuICAgICAgICB2YXIgZWxlbWVudFJlbCA9IGVsZW1lbnRXaWR0aCAvIGVsZW1lbnRIZWlnaHQ7XG4gICAgICAgIHZhciBzY2FsZSxzY2FsZVhPZmZzZXQsc2NhbGVZT2Zmc2V0O1xuICAgICAgICB2YXIgeEFsaWdubWVudCwgeUFsaWdubWVudCwgc2NhbGVNb2RlO1xuICAgICAgICB2YXIgYXNwZWN0UmF0aW8gPSBhbmltYXRpb24ucmVuZGVyZXIucmVuZGVyQ29uZmlnLnByZXNlcnZlQXNwZWN0UmF0aW8uc3BsaXQoJyAnKTtcbiAgICAgICAgaWYoYXNwZWN0UmF0aW9bMV0gPT09ICdtZWV0Jykge1xuICAgICAgICBcdHNjYWxlID0gZWxlbWVudFJlbCA+IGNvbXBSZWwgPyBlbGVtZW50SGVpZ2h0IC8gY29tcEhlaWdodCA6IGVsZW1lbnRXaWR0aCAvIGNvbXBXaWR0aDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgXHRzY2FsZSA9IGVsZW1lbnRSZWwgPiBjb21wUmVsID8gZWxlbWVudFdpZHRoIC8gY29tcFdpZHRoIDogZWxlbWVudEhlaWdodCAvIGNvbXBIZWlnaHQ7XG4gICAgICAgIH1cbiAgICAgICAgeEFsaWdubWVudCA9IGFzcGVjdFJhdGlvWzBdLnN1YnN0cigwLDQpO1xuICAgICAgICB5QWxpZ25tZW50ID0gYXNwZWN0UmF0aW9bMF0uc3Vic3RyKDQpO1xuICAgICAgICBpZih4QWxpZ25tZW50ID09PSAneE1pbicpIHtcbiAgICAgICAgXHRzY2FsZVhPZmZzZXQgPSAwO1xuICAgICAgICB9IGVsc2UgaWYoeEFsaWdubWVudCA9PT0gJ3hNaWQnKSB7XG4gICAgICAgIFx0c2NhbGVYT2Zmc2V0ID0gKGVsZW1lbnRXaWR0aCAtIGNvbXBXaWR0aCAqIHNjYWxlKSAvIDI7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgIFx0c2NhbGVYT2Zmc2V0ID0gKGVsZW1lbnRXaWR0aCAtIGNvbXBXaWR0aCAqIHNjYWxlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmKHlBbGlnbm1lbnQgPT09ICdZTWluJykge1xuXHQgICAgICAgIHNjYWxlWU9mZnNldCA9IDA7XG4gICAgICAgIH0gZWxzZSBpZih5QWxpZ25tZW50ID09PSAnWU1pZCcpIHtcblx0ICAgICAgICBzY2FsZVlPZmZzZXQgPSAoZWxlbWVudEhlaWdodCAtIGNvbXBIZWlnaHQgKiBzY2FsZSkgLyAyO1xuICAgICAgICB9IGVsc2Uge1xuXHQgICAgICAgIHNjYWxlWU9mZnNldCA9IChlbGVtZW50SGVpZ2h0IC0gY29tcEhlaWdodCAqIHNjYWxlKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4ge1xuICAgICAgICBcdHNjYWxlWU9mZnNldDogc2NhbGVZT2Zmc2V0LFxuICAgICAgICBcdHNjYWxlWE9mZnNldDogc2NhbGVYT2Zmc2V0LFxuICAgICAgICBcdHNjYWxlOiBzY2FsZVxuICAgICAgICB9XG5cdH1cblxuXHRmdW5jdGlvbiByZWNhbGN1bGF0ZVNpemUoY29udGFpbmVyKSB7XG5cdFx0dmFyIGNvbnRhaW5lciA9IGFuaW1hdGlvbi53cmFwcGVyO1xuXHRcdHN0YXRlLmJvdW5kaW5nUmVjdCA9IGNvbnRhaW5lci5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcblx0XHRzdGF0ZS5zY2FsZURhdGEgPSBjYWxjdWxhdGVTY2FsZURhdGEoc3RhdGUuYm91bmRpbmdSZWN0KTtcblx0fVxuXG5cdGZ1bmN0aW9uIHRvQ29udGFpbmVyUG9pbnQocG9pbnQpIHtcblx0XHRpZighYW5pbWF0aW9uLndyYXBwZXIgfHwgIWFuaW1hdGlvbi53cmFwcGVyLmdldEJvdW5kaW5nQ2xpZW50UmVjdCkge1xuXHRcdFx0cmV0dXJuIHBvaW50O1xuXHRcdH1cblx0XHRpZighc3RhdGUuYm91bmRpbmdSZWN0KSB7XG5cdFx0XHRyZWNhbGN1bGF0ZVNpemUoKTtcblx0XHR9XG5cblx0XHR2YXIgYm91bmRpbmdSZWN0ID0gc3RhdGUuYm91bmRpbmdSZWN0O1xuXHRcdHZhciBuZXdQb2ludCA9IFtwb2ludFswXSAtIGJvdW5kaW5nUmVjdC5sZWZ0LCBwb2ludFsxXSAtIGJvdW5kaW5nUmVjdC50b3BdO1xuXHRcdHZhciBzY2FsZURhdGEgPSBzdGF0ZS5zY2FsZURhdGE7XG5cbiAgICAgICAgbmV3UG9pbnRbMF0gPSAobmV3UG9pbnRbMF0gLSBzY2FsZURhdGEuc2NhbGVYT2Zmc2V0KSAvIHNjYWxlRGF0YS5zY2FsZTtcbiAgICAgICAgbmV3UG9pbnRbMV0gPSAobmV3UG9pbnRbMV0gLSBzY2FsZURhdGEuc2NhbGVZT2Zmc2V0KSAvIHNjYWxlRGF0YS5zY2FsZTtcblxuXHRcdHJldHVybiBuZXdQb2ludDtcblx0fVxuXG5cdGZ1bmN0aW9uIGZyb21Db250YWluZXJQb2ludChwb2ludCkge1xuXHRcdGlmKCFhbmltYXRpb24ud3JhcHBlciB8fCAhYW5pbWF0aW9uLndyYXBwZXIuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KSB7XG5cdFx0XHRyZXR1cm4gcG9pbnQ7XG5cdFx0fVxuXHRcdGlmKCFzdGF0ZS5ib3VuZGluZ1JlY3QpIHtcblx0XHRcdHJlY2FsY3VsYXRlU2l6ZSgpO1xuXHRcdH1cblx0XHR2YXIgYm91bmRpbmdSZWN0ID0gc3RhdGUuYm91bmRpbmdSZWN0O1xuXHRcdHZhciBzY2FsZURhdGEgPSBzdGF0ZS5zY2FsZURhdGE7XG5cblx0XHR2YXIgbmV3UG9pbnQgPSBbcG9pbnRbMF0gKiBzY2FsZURhdGEuc2NhbGUgKyBzY2FsZURhdGEuc2NhbGVYT2Zmc2V0LCBwb2ludFsxXSAqIHNjYWxlRGF0YS5zY2FsZSArIHNjYWxlRGF0YS5zY2FsZVlPZmZzZXRdO1xuXG5cdFx0dmFyIG5ld1BvaW50ID0gW25ld1BvaW50WzBdICsgYm91bmRpbmdSZWN0LmxlZnQsIG5ld1BvaW50WzFdICsgYm91bmRpbmdSZWN0LnRvcF07XG5cdFx0cmV0dXJuIG5ld1BvaW50O1xuXHR9XG5cblx0ZnVuY3Rpb24gZ2V0U2NhbGVEYXRhKCkge1xuXHRcdHJldHVybiBzdGF0ZS5zY2FsZURhdGE7XG5cdH1cblxuXHR2YXIgbWV0aG9kcyA9IHtcblx0XHRyZWNhbGN1bGF0ZVNpemU6IHJlY2FsY3VsYXRlU2l6ZSxcblx0XHRnZXRTY2FsZURhdGE6IGdldFNjYWxlRGF0YSxcblx0XHR0b0NvbnRhaW5lclBvaW50OiB0b0NvbnRhaW5lclBvaW50LFxuXHRcdGZyb21Db250YWluZXJQb2ludDogZnJvbUNvbnRhaW5lclBvaW50LFxuXHRcdGdldEN1cnJlbnRGcmFtZTogZ2V0Q3VycmVudEZyYW1lLFxuXHRcdGdldEN1cnJlbnRUaW1lOiBnZXRDdXJyZW50VGltZSxcblx0XHRhZGRWYWx1ZUNhbGxiYWNrOiBhZGRWYWx1ZUNhbGxiYWNrLFxuXHRcdHRvS2V5cGF0aExheWVyUG9pbnQ6IHRvS2V5cGF0aExheWVyUG9pbnQsXG5cdFx0ZnJvbUtleXBhdGhMYXllclBvaW50OiBmcm9tS2V5cGF0aExheWVyUG9pbnRcblx0fVxuXG5cdHJldHVybiBPYmplY3QuYXNzaWduKHt9LCBSZW5kZXJlcihzdGF0ZSksIG1ldGhvZHMpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IEFuaW1hdGlvbkl0ZW1GYWN0b3J5OyIsIm1vZHVsZS5leHBvcnRzID0gJywnOyIsIm1vZHVsZS5leHBvcnRzID0ge1xuXHQgMDogMCxcblx0IDE6IDEsXG5cdCAyOiAyLFxuXHQgMzogMyxcblx0IDQ6IDQsXG5cdCA1OiA1LFxuXHQgMTM6IDEzLFxuXHQnY29tcCc6IDAsXG5cdCdjb21wb3NpdGlvbic6IDAsXG5cdCdzb2xpZCc6IDEsXG5cdCdpbWFnZSc6IDIsXG5cdCdudWxsJzogMyxcblx0J3NoYXBlJzogNCxcblx0J3RleHQnOiA1LFxuXHQnY2FtZXJhJzogMTNcbn0iLCJtb2R1bGUuZXhwb3J0cyA9IHtcblx0TEFZRVJfVFJBTlNGT1JNOiAndHJhbnNmb3JtJ1xufSIsInZhciBrZXlfcGF0aF9zZXBhcmF0b3IgPSByZXF1aXJlKCcuLi9lbnVtcy9rZXlfcGF0aF9zZXBhcmF0b3InKTtcbnZhciBzYW5pdGl6ZVN0cmluZyA9IHJlcXVpcmUoJy4vc3RyaW5nU2FuaXRpemVyJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24ocHJvcGVydHlQYXRoKSB7XG5cdHZhciBrZXlQYXRoU3BsaXQgPSBwcm9wZXJ0eVBhdGguc3BsaXQoa2V5X3BhdGhfc2VwYXJhdG9yKTtcblx0dmFyIHNlbGVjdG9yID0ga2V5UGF0aFNwbGl0LnNoaWZ0KCk7XG5cdHJldHVybiB7XG5cdFx0c2VsZWN0b3I6IHNhbml0aXplU3RyaW5nKHNlbGVjdG9yKSxcblx0XHRwcm9wZXJ0eVBhdGg6IGtleVBhdGhTcGxpdC5qb2luKGtleV9wYXRoX3NlcGFyYXRvcilcblx0fVxufSIsInZhciBUZXh0RWxlbWVudCA9IHJlcXVpcmUoJy4uL2xheWVyL3RleHQvVGV4dEVsZW1lbnQnKTtcbnZhciBTaGFwZUVsZW1lbnQgPSByZXF1aXJlKCcuLi9sYXllci9zaGFwZS9TaGFwZScpO1xudmFyIE51bGxFbGVtZW50ID0gcmVxdWlyZSgnLi4vbGF5ZXIvbnVsbF9lbGVtZW50L051bGxFbGVtZW50Jyk7XG52YXIgU29saWRFbGVtZW50ID0gcmVxdWlyZSgnLi4vbGF5ZXIvc29saWQvU29saWRFbGVtZW50Jyk7XG52YXIgSW1hZ2VFbGVtZW50ID0gcmVxdWlyZSgnLi4vbGF5ZXIvaW1hZ2UvSW1hZ2VFbGVtZW50Jyk7XG52YXIgQ2FtZXJhRWxlbWVudCA9IHJlcXVpcmUoJy4uL2xheWVyL2NhbWVyYS9DYW1lcmEnKTtcbnZhciBMYXllckJhc2UgPSByZXF1aXJlKCcuLi9sYXllci9MYXllckJhc2UnKTtcblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGdldExheWVyQXBpKGVsZW1lbnQsIHBhcmVudCkge1xuXHR2YXIgbGF5ZXJUeXBlID0gZWxlbWVudC5kYXRhLnR5O1xuXHR2YXIgQ29tcG9zaXRpb24gPSByZXF1aXJlKCcuLi9sYXllci9jb21wb3NpdGlvbi9Db21wb3NpdGlvbicpO1xuXHRzd2l0Y2gobGF5ZXJUeXBlKSB7XG5cdFx0Y2FzZSAwOlxuXHRcdHJldHVybiBDb21wb3NpdGlvbihlbGVtZW50LCBwYXJlbnQpO1xuXHRcdGNhc2UgMTpcblx0XHRyZXR1cm4gU29saWRFbGVtZW50KGVsZW1lbnQsIHBhcmVudCk7XG5cdFx0Y2FzZSAyOlxuXHRcdHJldHVybiBJbWFnZUVsZW1lbnQoZWxlbWVudCwgcGFyZW50KTtcblx0XHRjYXNlIDM6XG5cdFx0cmV0dXJuIE51bGxFbGVtZW50KGVsZW1lbnQsIHBhcmVudCk7XG5cdFx0Y2FzZSA0OlxuXHRcdHJldHVybiBTaGFwZUVsZW1lbnQoZWxlbWVudCwgcGFyZW50LCBlbGVtZW50LmRhdGEuc2hhcGVzLCBlbGVtZW50Lml0ZW1zRGF0YSk7XG5cdFx0Y2FzZSA1OlxuXHRcdHJldHVybiBUZXh0RWxlbWVudChlbGVtZW50LCBwYXJlbnQpO1xuXHRcdGNhc2UgMTM6XG5cdFx0cmV0dXJuIENhbWVyYUVsZW1lbnQoZWxlbWVudCwgcGFyZW50KTtcblx0XHRkZWZhdWx0OlxuXHRcdHJldHVybiBMYXllckJhc2UoZWxlbWVudCwgcGFyZW50KTtcblx0fVxufSIsImZ1bmN0aW9uIHNhbml0aXplU3RyaW5nKHN0cmluZykge1xuXHRyZXR1cm4gc3RyaW5nLnRyaW0oKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBzYW5pdGl6ZVN0cmluZyIsInZhciBjcmVhdGVUeXBlZEFycmF5ID0gcmVxdWlyZSgnLi90eXBlZEFycmF5cycpXG5cbi8qIVxuIFRyYW5zZm9ybWF0aW9uIE1hdHJpeCB2Mi4wXG4gKGMpIEVwaXN0ZW1leCAyMDE0LTIwMTVcbiB3d3cuZXBpc3RlbWV4LmNvbVxuIEJ5IEtlbiBGeXJzdGVuYmVyZ1xuIENvbnRyaWJ1dGlvbnMgYnkgbGVlb25peWEuXG4gTGljZW5zZTogTUlULCBoZWFkZXIgcmVxdWlyZWQuXG4gKi9cblxuLyoqXG4gKiAyRCB0cmFuc2Zvcm1hdGlvbiBtYXRyaXggb2JqZWN0IGluaXRpYWxpemVkIHdpdGggaWRlbnRpdHkgbWF0cml4LlxuICpcbiAqIFRoZSBtYXRyaXggY2FuIHN5bmNocm9uaXplIGEgY2FudmFzIGNvbnRleHQgYnkgc3VwcGx5aW5nIHRoZSBjb250ZXh0XG4gKiBhcyBhbiBhcmd1bWVudCwgb3IgbGF0ZXIgYXBwbHkgY3VycmVudCBhYnNvbHV0ZSB0cmFuc2Zvcm0gdG8gYW5cbiAqIGV4aXN0aW5nIGNvbnRleHQuXG4gKlxuICogQWxsIHZhbHVlcyBhcmUgaGFuZGxlZCBhcyBmbG9hdGluZyBwb2ludCB2YWx1ZXMuXG4gKlxuICogQHBhcmFtIHtDYW52YXNSZW5kZXJpbmdDb250ZXh0MkR9IFtjb250ZXh0XSAtIE9wdGlvbmFsIGNvbnRleHQgdG8gc3luYyB3aXRoIE1hdHJpeFxuICogQHByb3Age251bWJlcn0gYSAtIHNjYWxlIHhcbiAqIEBwcm9wIHtudW1iZXJ9IGIgLSBzaGVhciB5XG4gKiBAcHJvcCB7bnVtYmVyfSBjIC0gc2hlYXIgeFxuICogQHByb3Age251bWJlcn0gZCAtIHNjYWxlIHlcbiAqIEBwcm9wIHtudW1iZXJ9IGUgLSB0cmFuc2xhdGUgeFxuICogQHByb3Age251bWJlcn0gZiAtIHRyYW5zbGF0ZSB5XG4gKiBAcHJvcCB7Q2FudmFzUmVuZGVyaW5nQ29udGV4dDJEfG51bGx9IFtjb250ZXh0PW51bGxdIC0gc2V0IG9yIGdldCBjdXJyZW50IGNhbnZhcyBjb250ZXh0XG4gKiBAY29uc3RydWN0b3JcbiAqL1xuXG52YXIgTWF0cml4ID0gKGZ1bmN0aW9uKCl7XG5cbiAgICB2YXIgX2NvcyA9IE1hdGguY29zO1xuICAgIHZhciBfc2luID0gTWF0aC5zaW47XG4gICAgdmFyIF90YW4gPSBNYXRoLnRhbjtcbiAgICB2YXIgX3JuZCA9IE1hdGgucm91bmQ7XG5cbiAgICBmdW5jdGlvbiByZXNldCgpe1xuICAgICAgICB0aGlzLnByb3BzWzBdID0gMTtcbiAgICAgICAgdGhpcy5wcm9wc1sxXSA9IDA7XG4gICAgICAgIHRoaXMucHJvcHNbMl0gPSAwO1xuICAgICAgICB0aGlzLnByb3BzWzNdID0gMDtcbiAgICAgICAgdGhpcy5wcm9wc1s0XSA9IDA7XG4gICAgICAgIHRoaXMucHJvcHNbNV0gPSAxO1xuICAgICAgICB0aGlzLnByb3BzWzZdID0gMDtcbiAgICAgICAgdGhpcy5wcm9wc1s3XSA9IDA7XG4gICAgICAgIHRoaXMucHJvcHNbOF0gPSAwO1xuICAgICAgICB0aGlzLnByb3BzWzldID0gMDtcbiAgICAgICAgdGhpcy5wcm9wc1sxMF0gPSAxO1xuICAgICAgICB0aGlzLnByb3BzWzExXSA9IDA7XG4gICAgICAgIHRoaXMucHJvcHNbMTJdID0gMDtcbiAgICAgICAgdGhpcy5wcm9wc1sxM10gPSAwO1xuICAgICAgICB0aGlzLnByb3BzWzE0XSA9IDA7XG4gICAgICAgIHRoaXMucHJvcHNbMTVdID0gMTtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcm90YXRlKGFuZ2xlKSB7XG4gICAgICAgIGlmKGFuZ2xlID09PSAwKXtcbiAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICB9XG4gICAgICAgIHZhciBtQ29zID0gX2NvcyhhbmdsZSk7XG4gICAgICAgIHZhciBtU2luID0gX3NpbihhbmdsZSk7XG4gICAgICAgIHJldHVybiB0aGlzLl90KG1Db3MsIC1tU2luLCAgMCwgMCwgbVNpbiwgIG1Db3MsIDAsIDAsIDAsICAwLCAgMSwgMCwgMCwgMCwgMCwgMSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcm90YXRlWChhbmdsZSl7XG4gICAgICAgIGlmKGFuZ2xlID09PSAwKXtcbiAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICB9XG4gICAgICAgIHZhciBtQ29zID0gX2NvcyhhbmdsZSk7XG4gICAgICAgIHZhciBtU2luID0gX3NpbihhbmdsZSk7XG4gICAgICAgIHJldHVybiB0aGlzLl90KDEsIDAsIDAsIDAsIDAsIG1Db3MsIC1tU2luLCAwLCAwLCBtU2luLCAgbUNvcywgMCwgMCwgMCwgMCwgMSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcm90YXRlWShhbmdsZSl7XG4gICAgICAgIGlmKGFuZ2xlID09PSAwKXtcbiAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICB9XG4gICAgICAgIHZhciBtQ29zID0gX2NvcyhhbmdsZSk7XG4gICAgICAgIHZhciBtU2luID0gX3NpbihhbmdsZSk7XG4gICAgICAgIHJldHVybiB0aGlzLl90KG1Db3MsICAwLCAgbVNpbiwgMCwgMCwgMSwgMCwgMCwgLW1TaW4sICAwLCAgbUNvcywgMCwgMCwgMCwgMCwgMSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcm90YXRlWihhbmdsZSl7XG4gICAgICAgIGlmKGFuZ2xlID09PSAwKXtcbiAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICB9XG4gICAgICAgIHZhciBtQ29zID0gX2NvcyhhbmdsZSk7XG4gICAgICAgIHZhciBtU2luID0gX3NpbihhbmdsZSk7XG4gICAgICAgIHJldHVybiB0aGlzLl90KG1Db3MsIC1tU2luLCAgMCwgMCwgbVNpbiwgIG1Db3MsIDAsIDAsIDAsICAwLCAgMSwgMCwgMCwgMCwgMCwgMSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc2hlYXIoc3gsc3kpe1xuICAgICAgICByZXR1cm4gdGhpcy5fdCgxLCBzeSwgc3gsIDEsIDAsIDApO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHNrZXcoYXgsIGF5KXtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2hlYXIoX3RhbihheCksIF90YW4oYXkpKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBza2V3RnJvbUF4aXMoYXgsIGFuZ2xlKXtcbiAgICAgICAgdmFyIG1Db3MgPSBfY29zKGFuZ2xlKTtcbiAgICAgICAgdmFyIG1TaW4gPSBfc2luKGFuZ2xlKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuX3QobUNvcywgbVNpbiwgIDAsIDAsIC1tU2luLCAgbUNvcywgMCwgMCwgMCwgIDAsICAxLCAwLCAwLCAwLCAwLCAxKVxuICAgICAgICAgICAgLl90KDEsIDAsICAwLCAwLCBfdGFuKGF4KSwgIDEsIDAsIDAsIDAsICAwLCAgMSwgMCwgMCwgMCwgMCwgMSlcbiAgICAgICAgICAgIC5fdChtQ29zLCAtbVNpbiwgIDAsIDAsIG1TaW4sICBtQ29zLCAwLCAwLCAwLCAgMCwgIDEsIDAsIDAsIDAsIDAsIDEpO1xuICAgICAgICAvL3JldHVybiB0aGlzLl90KG1Db3MsIG1TaW4sIC1tU2luLCBtQ29zLCAwLCAwKS5fdCgxLCAwLCBfdGFuKGF4KSwgMSwgMCwgMCkuX3QobUNvcywgLW1TaW4sIG1TaW4sIG1Db3MsIDAsIDApO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHNjYWxlKHN4LCBzeSwgc3opIHtcbiAgICAgICAgc3ogPSBpc05hTihzeikgPyAxIDogc3o7XG4gICAgICAgIGlmKHN4ID09IDEgJiYgc3kgPT0gMSAmJiBzeiA9PSAxKXtcbiAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLl90KHN4LCAwLCAwLCAwLCAwLCBzeSwgMCwgMCwgMCwgMCwgc3osIDAsIDAsIDAsIDAsIDEpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHNldFRyYW5zZm9ybShhLCBiLCBjLCBkLCBlLCBmLCBnLCBoLCBpLCBqLCBrLCBsLCBtLCBuLCBvLCBwKSB7XG4gICAgICAgIHRoaXMucHJvcHNbMF0gPSBhO1xuICAgICAgICB0aGlzLnByb3BzWzFdID0gYjtcbiAgICAgICAgdGhpcy5wcm9wc1syXSA9IGM7XG4gICAgICAgIHRoaXMucHJvcHNbM10gPSBkO1xuICAgICAgICB0aGlzLnByb3BzWzRdID0gZTtcbiAgICAgICAgdGhpcy5wcm9wc1s1XSA9IGY7XG4gICAgICAgIHRoaXMucHJvcHNbNl0gPSBnO1xuICAgICAgICB0aGlzLnByb3BzWzddID0gaDtcbiAgICAgICAgdGhpcy5wcm9wc1s4XSA9IGk7XG4gICAgICAgIHRoaXMucHJvcHNbOV0gPSBqO1xuICAgICAgICB0aGlzLnByb3BzWzEwXSA9IGs7XG4gICAgICAgIHRoaXMucHJvcHNbMTFdID0gbDtcbiAgICAgICAgdGhpcy5wcm9wc1sxMl0gPSBtO1xuICAgICAgICB0aGlzLnByb3BzWzEzXSA9IG47XG4gICAgICAgIHRoaXMucHJvcHNbMTRdID0gbztcbiAgICAgICAgdGhpcy5wcm9wc1sxNV0gPSBwO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICBmdW5jdGlvbiB0cmFuc2xhdGUodHgsIHR5LCB0eikge1xuICAgICAgICB0eiA9IHR6IHx8IDA7XG4gICAgICAgIGlmKHR4ICE9PSAwIHx8IHR5ICE9PSAwIHx8IHR6ICE9PSAwKXtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl90KDEsMCwwLDAsMCwxLDAsMCwwLDAsMSwwLHR4LHR5LHR6LDEpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHRyYW5zZm9ybShhMiwgYjIsIGMyLCBkMiwgZTIsIGYyLCBnMiwgaDIsIGkyLCBqMiwgazIsIGwyLCBtMiwgbjIsIG8yLCBwMikge1xuXG4gICAgICAgIHZhciBfcCA9IHRoaXMucHJvcHM7XG5cbiAgICAgICAgaWYoYTIgPT09IDEgJiYgYjIgPT09IDAgJiYgYzIgPT09IDAgJiYgZDIgPT09IDAgJiYgZTIgPT09IDAgJiYgZjIgPT09IDEgJiYgZzIgPT09IDAgJiYgaDIgPT09IDAgJiYgaTIgPT09IDAgJiYgajIgPT09IDAgJiYgazIgPT09IDEgJiYgbDIgPT09IDApe1xuICAgICAgICAgICAgLy9OT1RFOiBjb21tZW50aW5nIHRoaXMgY29uZGl0aW9uIGJlY2F1c2UgVHVyYm9GYW4gZGVvcHRpbWl6ZXMgY29kZSB3aGVuIHByZXNlbnRcbiAgICAgICAgICAgIC8vaWYobTIgIT09IDAgfHwgbjIgIT09IDAgfHwgbzIgIT09IDApe1xuICAgICAgICAgICAgICAgIF9wWzEyXSA9IF9wWzEyXSAqIGEyICsgX3BbMTVdICogbTI7XG4gICAgICAgICAgICAgICAgX3BbMTNdID0gX3BbMTNdICogZjIgKyBfcFsxNV0gKiBuMjtcbiAgICAgICAgICAgICAgICBfcFsxNF0gPSBfcFsxNF0gKiBrMiArIF9wWzE1XSAqIG8yO1xuICAgICAgICAgICAgICAgIF9wWzE1XSA9IF9wWzE1XSAqIHAyO1xuICAgICAgICAgICAgLy99XG4gICAgICAgICAgICB0aGlzLl9pZGVudGl0eUNhbGN1bGF0ZWQgPSBmYWxzZTtcbiAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIGExID0gX3BbMF07XG4gICAgICAgIHZhciBiMSA9IF9wWzFdO1xuICAgICAgICB2YXIgYzEgPSBfcFsyXTtcbiAgICAgICAgdmFyIGQxID0gX3BbM107XG4gICAgICAgIHZhciBlMSA9IF9wWzRdO1xuICAgICAgICB2YXIgZjEgPSBfcFs1XTtcbiAgICAgICAgdmFyIGcxID0gX3BbNl07XG4gICAgICAgIHZhciBoMSA9IF9wWzddO1xuICAgICAgICB2YXIgaTEgPSBfcFs4XTtcbiAgICAgICAgdmFyIGoxID0gX3BbOV07XG4gICAgICAgIHZhciBrMSA9IF9wWzEwXTtcbiAgICAgICAgdmFyIGwxID0gX3BbMTFdO1xuICAgICAgICB2YXIgbTEgPSBfcFsxMl07XG4gICAgICAgIHZhciBuMSA9IF9wWzEzXTtcbiAgICAgICAgdmFyIG8xID0gX3BbMTRdO1xuICAgICAgICB2YXIgcDEgPSBfcFsxNV07XG5cbiAgICAgICAgLyogbWF0cml4IG9yZGVyIChjYW52YXMgY29tcGF0aWJsZSk6XG4gICAgICAgICAqIGFjZVxuICAgICAgICAgKiBiZGZcbiAgICAgICAgICogMDAxXG4gICAgICAgICAqL1xuICAgICAgICBfcFswXSA9IGExICogYTIgKyBiMSAqIGUyICsgYzEgKiBpMiArIGQxICogbTI7XG4gICAgICAgIF9wWzFdID0gYTEgKiBiMiArIGIxICogZjIgKyBjMSAqIGoyICsgZDEgKiBuMiA7XG4gICAgICAgIF9wWzJdID0gYTEgKiBjMiArIGIxICogZzIgKyBjMSAqIGsyICsgZDEgKiBvMiA7XG4gICAgICAgIF9wWzNdID0gYTEgKiBkMiArIGIxICogaDIgKyBjMSAqIGwyICsgZDEgKiBwMiA7XG5cbiAgICAgICAgX3BbNF0gPSBlMSAqIGEyICsgZjEgKiBlMiArIGcxICogaTIgKyBoMSAqIG0yIDtcbiAgICAgICAgX3BbNV0gPSBlMSAqIGIyICsgZjEgKiBmMiArIGcxICogajIgKyBoMSAqIG4yIDtcbiAgICAgICAgX3BbNl0gPSBlMSAqIGMyICsgZjEgKiBnMiArIGcxICogazIgKyBoMSAqIG8yIDtcbiAgICAgICAgX3BbN10gPSBlMSAqIGQyICsgZjEgKiBoMiArIGcxICogbDIgKyBoMSAqIHAyIDtcblxuICAgICAgICBfcFs4XSA9IGkxICogYTIgKyBqMSAqIGUyICsgazEgKiBpMiArIGwxICogbTIgO1xuICAgICAgICBfcFs5XSA9IGkxICogYjIgKyBqMSAqIGYyICsgazEgKiBqMiArIGwxICogbjIgO1xuICAgICAgICBfcFsxMF0gPSBpMSAqIGMyICsgajEgKiBnMiArIGsxICogazIgKyBsMSAqIG8yIDtcbiAgICAgICAgX3BbMTFdID0gaTEgKiBkMiArIGoxICogaDIgKyBrMSAqIGwyICsgbDEgKiBwMiA7XG5cbiAgICAgICAgX3BbMTJdID0gbTEgKiBhMiArIG4xICogZTIgKyBvMSAqIGkyICsgcDEgKiBtMiA7XG4gICAgICAgIF9wWzEzXSA9IG0xICogYjIgKyBuMSAqIGYyICsgbzEgKiBqMiArIHAxICogbjIgO1xuICAgICAgICBfcFsxNF0gPSBtMSAqIGMyICsgbjEgKiBnMiArIG8xICogazIgKyBwMSAqIG8yIDtcbiAgICAgICAgX3BbMTVdID0gbTEgKiBkMiArIG4xICogaDIgKyBvMSAqIGwyICsgcDEgKiBwMiA7XG5cbiAgICAgICAgdGhpcy5faWRlbnRpdHlDYWxjdWxhdGVkID0gZmFsc2U7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGlzSWRlbnRpdHkoKSB7XG4gICAgICAgIGlmKCF0aGlzLl9pZGVudGl0eUNhbGN1bGF0ZWQpe1xuICAgICAgICAgICAgdGhpcy5faWRlbnRpdHkgPSAhKHRoaXMucHJvcHNbMF0gIT09IDEgfHwgdGhpcy5wcm9wc1sxXSAhPT0gMCB8fCB0aGlzLnByb3BzWzJdICE9PSAwIHx8IHRoaXMucHJvcHNbM10gIT09IDAgfHwgdGhpcy5wcm9wc1s0XSAhPT0gMCB8fCB0aGlzLnByb3BzWzVdICE9PSAxIHx8IHRoaXMucHJvcHNbNl0gIT09IDAgfHwgdGhpcy5wcm9wc1s3XSAhPT0gMCB8fCB0aGlzLnByb3BzWzhdICE9PSAwIHx8IHRoaXMucHJvcHNbOV0gIT09IDAgfHwgdGhpcy5wcm9wc1sxMF0gIT09IDEgfHwgdGhpcy5wcm9wc1sxMV0gIT09IDAgfHwgdGhpcy5wcm9wc1sxMl0gIT09IDAgfHwgdGhpcy5wcm9wc1sxM10gIT09IDAgfHwgdGhpcy5wcm9wc1sxNF0gIT09IDAgfHwgdGhpcy5wcm9wc1sxNV0gIT09IDEpO1xuICAgICAgICAgICAgdGhpcy5faWRlbnRpdHlDYWxjdWxhdGVkID0gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5faWRlbnRpdHk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZXF1YWxzKG1hdHIpe1xuICAgICAgICB2YXIgaSA9IDA7XG4gICAgICAgIHdoaWxlIChpIDwgMTYpIHtcbiAgICAgICAgICAgIGlmKG1hdHIucHJvcHNbaV0gIT09IHRoaXMucHJvcHNbaV0pIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpKz0xO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNsb25lKG1hdHIpe1xuICAgICAgICB2YXIgaTtcbiAgICAgICAgZm9yKGk9MDtpPDE2O2krPTEpe1xuICAgICAgICAgICAgbWF0ci5wcm9wc1tpXSA9IHRoaXMucHJvcHNbaV07XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBjbG9uZUZyb21Qcm9wcyhwcm9wcyl7XG4gICAgICAgIHZhciBpO1xuICAgICAgICBmb3IoaT0wO2k8MTY7aSs9MSl7XG4gICAgICAgICAgICB0aGlzLnByb3BzW2ldID0gcHJvcHNbaV07XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBhcHBseVRvUG9pbnQoeCwgeSwgeikge1xuXG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICB4OiB4ICogdGhpcy5wcm9wc1swXSArIHkgKiB0aGlzLnByb3BzWzRdICsgeiAqIHRoaXMucHJvcHNbOF0gKyB0aGlzLnByb3BzWzEyXSxcbiAgICAgICAgICAgIHk6IHggKiB0aGlzLnByb3BzWzFdICsgeSAqIHRoaXMucHJvcHNbNV0gKyB6ICogdGhpcy5wcm9wc1s5XSArIHRoaXMucHJvcHNbMTNdLFxuICAgICAgICAgICAgejogeCAqIHRoaXMucHJvcHNbMl0gKyB5ICogdGhpcy5wcm9wc1s2XSArIHogKiB0aGlzLnByb3BzWzEwXSArIHRoaXMucHJvcHNbMTRdXG4gICAgICAgIH07XG4gICAgICAgIC8qcmV0dXJuIHtcbiAgICAgICAgIHg6IHggKiBtZS5hICsgeSAqIG1lLmMgKyBtZS5lLFxuICAgICAgICAgeTogeCAqIG1lLmIgKyB5ICogbWUuZCArIG1lLmZcbiAgICAgICAgIH07Ki9cbiAgICB9XG4gICAgZnVuY3Rpb24gYXBwbHlUb1goeCwgeSwgeikge1xuICAgICAgICByZXR1cm4geCAqIHRoaXMucHJvcHNbMF0gKyB5ICogdGhpcy5wcm9wc1s0XSArIHogKiB0aGlzLnByb3BzWzhdICsgdGhpcy5wcm9wc1sxMl07XG4gICAgfVxuICAgIGZ1bmN0aW9uIGFwcGx5VG9ZKHgsIHksIHopIHtcbiAgICAgICAgcmV0dXJuIHggKiB0aGlzLnByb3BzWzFdICsgeSAqIHRoaXMucHJvcHNbNV0gKyB6ICogdGhpcy5wcm9wc1s5XSArIHRoaXMucHJvcHNbMTNdO1xuICAgIH1cbiAgICBmdW5jdGlvbiBhcHBseVRvWih4LCB5LCB6KSB7XG4gICAgICAgIHJldHVybiB4ICogdGhpcy5wcm9wc1syXSArIHkgKiB0aGlzLnByb3BzWzZdICsgeiAqIHRoaXMucHJvcHNbMTBdICsgdGhpcy5wcm9wc1sxNF07XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaW52ZXJzZVBvaW50KHB0KSB7XG4gICAgICAgIHZhciBkZXRlcm1pbmFudCA9IHRoaXMucHJvcHNbMF0gKiB0aGlzLnByb3BzWzVdIC0gdGhpcy5wcm9wc1sxXSAqIHRoaXMucHJvcHNbNF07XG4gICAgICAgIHZhciBhID0gdGhpcy5wcm9wc1s1XS9kZXRlcm1pbmFudDtcbiAgICAgICAgdmFyIGIgPSAtIHRoaXMucHJvcHNbMV0vZGV0ZXJtaW5hbnQ7XG4gICAgICAgIHZhciBjID0gLSB0aGlzLnByb3BzWzRdL2RldGVybWluYW50O1xuICAgICAgICB2YXIgZCA9IHRoaXMucHJvcHNbMF0vZGV0ZXJtaW5hbnQ7XG4gICAgICAgIHZhciBlID0gKHRoaXMucHJvcHNbNF0gKiB0aGlzLnByb3BzWzEzXSAtIHRoaXMucHJvcHNbNV0gKiB0aGlzLnByb3BzWzEyXSkvZGV0ZXJtaW5hbnQ7XG4gICAgICAgIHZhciBmID0gLSAodGhpcy5wcm9wc1swXSAqIHRoaXMucHJvcHNbMTNdIC0gdGhpcy5wcm9wc1sxXSAqIHRoaXMucHJvcHNbMTJdKS9kZXRlcm1pbmFudDtcbiAgICAgICAgcmV0dXJuIFtwdFswXSAqIGEgKyBwdFsxXSAqIGMgKyBlLCBwdFswXSAqIGIgKyBwdFsxXSAqIGQgKyBmLCAwXTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBpbnZlcnNlUG9pbnRzKHB0cyl7XG4gICAgICAgIHZhciBpLCBsZW4gPSBwdHMubGVuZ3RoLCByZXRQdHMgPSBbXTtcbiAgICAgICAgZm9yKGk9MDtpPGxlbjtpKz0xKXtcbiAgICAgICAgICAgIHJldFB0c1tpXSA9IGludmVyc2VQb2ludChwdHNbaV0pO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXRQdHM7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gYXBwbHlUb1RyaXBsZVBvaW50cyhwdDEsIHB0MiwgcHQzKSB7XG4gICAgICAgIHZhciBhcnIgPSBjcmVhdGVUeXBlZEFycmF5KCdmbG9hdDMyJywgNik7XG4gICAgICAgIGlmKHRoaXMuaXNJZGVudGl0eSgpKSB7XG4gICAgICAgICAgICBhcnJbMF0gPSBwdDFbMF07XG4gICAgICAgICAgICBhcnJbMV0gPSBwdDFbMV07XG4gICAgICAgICAgICBhcnJbMl0gPSBwdDJbMF07XG4gICAgICAgICAgICBhcnJbM10gPSBwdDJbMV07XG4gICAgICAgICAgICBhcnJbNF0gPSBwdDNbMF07XG4gICAgICAgICAgICBhcnJbNV0gPSBwdDNbMV07XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB2YXIgcDAgPSB0aGlzLnByb3BzWzBdLCBwMSA9IHRoaXMucHJvcHNbMV0sIHA0ID0gdGhpcy5wcm9wc1s0XSwgcDUgPSB0aGlzLnByb3BzWzVdLCBwMTIgPSB0aGlzLnByb3BzWzEyXSwgcDEzID0gdGhpcy5wcm9wc1sxM107XG4gICAgICAgICAgICBhcnJbMF0gPSBwdDFbMF0gKiBwMCArIHB0MVsxXSAqIHA0ICsgcDEyO1xuICAgICAgICAgICAgYXJyWzFdID0gcHQxWzBdICogcDEgKyBwdDFbMV0gKiBwNSArIHAxMztcbiAgICAgICAgICAgIGFyclsyXSA9IHB0MlswXSAqIHAwICsgcHQyWzFdICogcDQgKyBwMTI7XG4gICAgICAgICAgICBhcnJbM10gPSBwdDJbMF0gKiBwMSArIHB0MlsxXSAqIHA1ICsgcDEzO1xuICAgICAgICAgICAgYXJyWzRdID0gcHQzWzBdICogcDAgKyBwdDNbMV0gKiBwNCArIHAxMjtcbiAgICAgICAgICAgIGFycls1XSA9IHB0M1swXSAqIHAxICsgcHQzWzFdICogcDUgKyBwMTM7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGFycjtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBhcHBseVRvUG9pbnRBcnJheSh4LHkseil7XG4gICAgICAgIHZhciBhcnI7XG4gICAgICAgIGlmKHRoaXMuaXNJZGVudGl0eSgpKSB7XG4gICAgICAgICAgICBhcnIgPSBbeCx5LHpdO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgYXJyID0gW3ggKiB0aGlzLnByb3BzWzBdICsgeSAqIHRoaXMucHJvcHNbNF0gKyB6ICogdGhpcy5wcm9wc1s4XSArIHRoaXMucHJvcHNbMTJdLHggKiB0aGlzLnByb3BzWzFdICsgeSAqIHRoaXMucHJvcHNbNV0gKyB6ICogdGhpcy5wcm9wc1s5XSArIHRoaXMucHJvcHNbMTNdLHggKiB0aGlzLnByb3BzWzJdICsgeSAqIHRoaXMucHJvcHNbNl0gKyB6ICogdGhpcy5wcm9wc1sxMF0gKyB0aGlzLnByb3BzWzE0XV07XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGFycjtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBhcHBseVRvUG9pbnRTdHJpbmdpZmllZCh4LCB5KSB7XG4gICAgICAgIGlmKHRoaXMuaXNJZGVudGl0eSgpKSB7XG4gICAgICAgICAgICByZXR1cm4geCArICcsJyArIHk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuICh4ICogdGhpcy5wcm9wc1swXSArIHkgKiB0aGlzLnByb3BzWzRdICsgdGhpcy5wcm9wc1sxMl0pKycsJysoeCAqIHRoaXMucHJvcHNbMV0gKyB5ICogdGhpcy5wcm9wc1s1XSArIHRoaXMucHJvcHNbMTNdKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiB0b0NTUygpIHtcbiAgICAgICAgLy9Eb2Vzbid0IG1ha2UgbXVjaCBzZW5zZSB0byBhZGQgdGhpcyBvcHRpbWl6YXRpb24uIElmIGl0IGlzIGFuIGlkZW50aXR5IG1hdHJpeCwgaXQncyB2ZXJ5IGxpa2VseSB0aGlzIHdpbGwgZ2V0IGNhbGxlZCBvbmx5IG9uY2Ugc2luY2UgaXQgd29uJ3QgYmUga2V5ZnJhbWVkLlxuICAgICAgICAvKmlmKHRoaXMuaXNJZGVudGl0eSgpKSB7XG4gICAgICAgICAgICByZXR1cm4gJyc7XG4gICAgICAgIH0qL1xuICAgICAgICB2YXIgaSA9IDA7XG4gICAgICAgIHZhciBwcm9wcyA9IHRoaXMucHJvcHM7XG4gICAgICAgIHZhciBjc3NWYWx1ZSA9ICdtYXRyaXgzZCgnO1xuICAgICAgICB2YXIgdiA9IDEwMDAwO1xuICAgICAgICB3aGlsZShpPDE2KXtcbiAgICAgICAgICAgIGNzc1ZhbHVlICs9IF9ybmQocHJvcHNbaV0qdikvdjtcbiAgICAgICAgICAgIGNzc1ZhbHVlICs9IGkgPT09IDE1ID8gJyknOicsJztcbiAgICAgICAgICAgIGkgKz0gMTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gY3NzVmFsdWU7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gdG8yZENTUygpIHtcbiAgICAgICAgLy9Eb2Vzbid0IG1ha2UgbXVjaCBzZW5zZSB0byBhZGQgdGhpcyBvcHRpbWl6YXRpb24uIElmIGl0IGlzIGFuIGlkZW50aXR5IG1hdHJpeCwgaXQncyB2ZXJ5IGxpa2VseSB0aGlzIHdpbGwgZ2V0IGNhbGxlZCBvbmx5IG9uY2Ugc2luY2UgaXQgd29uJ3QgYmUga2V5ZnJhbWVkLlxuICAgICAgICAvKmlmKHRoaXMuaXNJZGVudGl0eSgpKSB7XG4gICAgICAgICAgICByZXR1cm4gJyc7XG4gICAgICAgIH0qL1xuICAgICAgICB2YXIgdiA9IDEwMDAwO1xuICAgICAgICB2YXIgcHJvcHMgPSB0aGlzLnByb3BzO1xuICAgICAgICByZXR1cm4gXCJtYXRyaXgoXCIgKyBfcm5kKHByb3BzWzBdKnYpL3YgKyAnLCcgKyBfcm5kKHByb3BzWzFdKnYpL3YgKyAnLCcgKyBfcm5kKHByb3BzWzRdKnYpL3YgKyAnLCcgKyBfcm5kKHByb3BzWzVdKnYpL3YgKyAnLCcgKyBfcm5kKHByb3BzWzEyXSp2KS92ICsgJywnICsgX3JuZChwcm9wc1sxM10qdikvdiArIFwiKVwiO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIE1hdHJpeEluc3RhbmNlKCl7XG4gICAgICAgIHRoaXMucmVzZXQgPSByZXNldDtcbiAgICAgICAgdGhpcy5yb3RhdGUgPSByb3RhdGU7XG4gICAgICAgIHRoaXMucm90YXRlWCA9IHJvdGF0ZVg7XG4gICAgICAgIHRoaXMucm90YXRlWSA9IHJvdGF0ZVk7XG4gICAgICAgIHRoaXMucm90YXRlWiA9IHJvdGF0ZVo7XG4gICAgICAgIHRoaXMuc2tldyA9IHNrZXc7XG4gICAgICAgIHRoaXMuc2tld0Zyb21BeGlzID0gc2tld0Zyb21BeGlzO1xuICAgICAgICB0aGlzLnNoZWFyID0gc2hlYXI7XG4gICAgICAgIHRoaXMuc2NhbGUgPSBzY2FsZTtcbiAgICAgICAgdGhpcy5zZXRUcmFuc2Zvcm0gPSBzZXRUcmFuc2Zvcm07XG4gICAgICAgIHRoaXMudHJhbnNsYXRlID0gdHJhbnNsYXRlO1xuICAgICAgICB0aGlzLnRyYW5zZm9ybSA9IHRyYW5zZm9ybTtcbiAgICAgICAgdGhpcy5hcHBseVRvUG9pbnQgPSBhcHBseVRvUG9pbnQ7XG4gICAgICAgIHRoaXMuYXBwbHlUb1ggPSBhcHBseVRvWDtcbiAgICAgICAgdGhpcy5hcHBseVRvWSA9IGFwcGx5VG9ZO1xuICAgICAgICB0aGlzLmFwcGx5VG9aID0gYXBwbHlUb1o7XG4gICAgICAgIHRoaXMuYXBwbHlUb1BvaW50QXJyYXkgPSBhcHBseVRvUG9pbnRBcnJheTtcbiAgICAgICAgdGhpcy5hcHBseVRvVHJpcGxlUG9pbnRzID0gYXBwbHlUb1RyaXBsZVBvaW50cztcbiAgICAgICAgdGhpcy5hcHBseVRvUG9pbnRTdHJpbmdpZmllZCA9IGFwcGx5VG9Qb2ludFN0cmluZ2lmaWVkO1xuICAgICAgICB0aGlzLnRvQ1NTID0gdG9DU1M7XG4gICAgICAgIHRoaXMudG8yZENTUyA9IHRvMmRDU1M7XG4gICAgICAgIHRoaXMuY2xvbmUgPSBjbG9uZTtcbiAgICAgICAgdGhpcy5jbG9uZUZyb21Qcm9wcyA9IGNsb25lRnJvbVByb3BzO1xuICAgICAgICB0aGlzLmVxdWFscyA9IGVxdWFscztcbiAgICAgICAgdGhpcy5pbnZlcnNlUG9pbnRzID0gaW52ZXJzZVBvaW50cztcbiAgICAgICAgdGhpcy5pbnZlcnNlUG9pbnQgPSBpbnZlcnNlUG9pbnQ7XG4gICAgICAgIHRoaXMuX3QgPSB0aGlzLnRyYW5zZm9ybTtcbiAgICAgICAgdGhpcy5pc0lkZW50aXR5ID0gaXNJZGVudGl0eTtcbiAgICAgICAgdGhpcy5faWRlbnRpdHkgPSB0cnVlO1xuICAgICAgICB0aGlzLl9pZGVudGl0eUNhbGN1bGF0ZWQgPSBmYWxzZTtcblxuICAgICAgICB0aGlzLnByb3BzID0gY3JlYXRlVHlwZWRBcnJheSgnZmxvYXQzMicsIDE2KTtcbiAgICAgICAgdGhpcy5yZXNldCgpO1xuICAgIH07XG5cbiAgICByZXR1cm4gZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiBuZXcgTWF0cml4SW5zdGFuY2UoKVxuICAgIH1cbn0oKSk7XG5cbm1vZHVsZS5leHBvcnRzID0gTWF0cml4OyIsInZhciBjcmVhdGVUeXBlZEFycmF5ID0gKGZ1bmN0aW9uKCl7XG5cdGZ1bmN0aW9uIGNyZWF0ZVJlZ3VsYXJBcnJheSh0eXBlLCBsZW4pe1xuXHRcdHZhciBpID0gMCwgYXJyID0gW10sIHZhbHVlO1xuXHRcdHN3aXRjaCh0eXBlKSB7XG5cdFx0XHRjYXNlICdpbnQxNic6XG5cdFx0XHRjYXNlICd1aW50OGMnOlxuXHRcdFx0XHR2YWx1ZSA9IDE7XG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0dmFsdWUgPSAxLjE7XG5cdFx0XHRcdGJyZWFrO1xuXHRcdH1cblx0XHRmb3IoaSA9IDA7IGkgPCBsZW47IGkgKz0gMSkge1xuXHRcdFx0YXJyLnB1c2godmFsdWUpO1xuXHRcdH1cblx0XHRyZXR1cm4gYXJyO1xuXHR9XG5cdGZ1bmN0aW9uIGNyZWF0ZVR5cGVkQXJyYXkodHlwZSwgbGVuKXtcblx0XHRpZih0eXBlID09PSAnZmxvYXQzMicpIHtcblx0XHRcdHJldHVybiBuZXcgRmxvYXQzMkFycmF5KGxlbik7XG5cdFx0fSBlbHNlIGlmKHR5cGUgPT09ICdpbnQxNicpIHtcblx0XHRcdHJldHVybiBuZXcgSW50MTZBcnJheShsZW4pO1xuXHRcdH0gZWxzZSBpZih0eXBlID09PSAndWludDhjJykge1xuXHRcdFx0cmV0dXJuIG5ldyBVaW50OENsYW1wZWRBcnJheShsZW4pO1xuXHRcdH1cblx0fVxuXHRpZih0eXBlb2YgVWludDhDbGFtcGVkQXJyYXkgPT09ICdmdW5jdGlvbicgJiYgdHlwZW9mIEZsb2F0MzJBcnJheSA9PT0gJ2Z1bmN0aW9uJykge1xuXHRcdHJldHVybiBjcmVhdGVUeXBlZEFycmF5O1xuXHR9IGVsc2Uge1xuXHRcdHJldHVybiBjcmVhdGVSZWd1bGFyQXJyYXk7XG5cdH1cbn0oKSk7XG5cbm1vZHVsZS5leHBvcnRzID0gY3JlYXRlVHlwZWRBcnJheTtcbiIsInZhciBBbmltYXRpb25JdGVtID0gcmVxdWlyZSgnLi9hbmltYXRpb24vQW5pbWF0aW9uSXRlbScpO1xuXG5mdW5jdGlvbiBjcmVhdGVBbmltYXRpb25BcGkoYW5pbSkge1xuXHRyZXR1cm4gT2JqZWN0LmFzc2lnbih7fSwgQW5pbWF0aW9uSXRlbShhbmltKSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0ge1xuXHRjcmVhdGVBbmltYXRpb25BcGkgOiBjcmVhdGVBbmltYXRpb25BcGlcbn0iLCJ2YXIga2V5UGF0aEJ1aWxkZXIgPSByZXF1aXJlKCcuLi9oZWxwZXJzL2tleVBhdGhCdWlsZGVyJyk7XG52YXIgbGF5ZXJfdHlwZXMgPSByZXF1aXJlKCcuLi9lbnVtcy9sYXllcl90eXBlcycpO1xuXG5mdW5jdGlvbiBLZXlQYXRoTGlzdChlbGVtZW50cywgbm9kZV90eXBlKSB7XG5cblx0ZnVuY3Rpb24gX2dldExlbmd0aCgpIHtcblx0XHRyZXR1cm4gZWxlbWVudHMubGVuZ3RoO1xuXHR9XG5cblx0ZnVuY3Rpb24gX2ZpbHRlckxheWVyQnlUeXBlKGVsZW1lbnRzLCB0eXBlKSB7XG5cdFx0cmV0dXJuIGVsZW1lbnRzLmZpbHRlcihmdW5jdGlvbihlbGVtZW50KSB7XG5cdFx0XHRyZXR1cm4gZWxlbWVudC5nZXRUYXJnZXRMYXllcigpLmRhdGEudHkgPT09IGxheWVyX3R5cGVzW3R5cGVdO1xuXHRcdH0pO1xuXHR9XG5cblx0ZnVuY3Rpb24gX2ZpbHRlckxheWVyQnlOYW1lKGVsZW1lbnRzLCBuYW1lKSB7XG5cdFx0cmV0dXJuIGVsZW1lbnRzLmZpbHRlcihmdW5jdGlvbihlbGVtZW50KSB7XG5cdFx0XHRyZXR1cm4gZWxlbWVudC5nZXRUYXJnZXRMYXllcigpLmRhdGEubm0gPT09IG5hbWU7XG5cdFx0fSk7XG5cdH1cblxuXHRmdW5jdGlvbiBfZmlsdGVyTGF5ZXJCeVByb3BlcnR5KGVsZW1lbnRzLCBuYW1lKSB7XG5cdFx0cmV0dXJuIGVsZW1lbnRzLmZpbHRlcihmdW5jdGlvbihlbGVtZW50KSB7XG5cdFx0XHRpZihlbGVtZW50Lmhhc1Byb3BlcnR5KG5hbWUpKSB7XG5cdFx0XHRcdHJldHVybiBlbGVtZW50LmdldFByb3BlcnR5KG5hbWUpO1xuXHRcdFx0fVxuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdH0pO1xuXHR9XG5cblx0ZnVuY3Rpb24gZ2V0TGF5ZXJzQnlUeXBlKHNlbGVjdG9yKSB7XG5cdFx0cmV0dXJuIEtleVBhdGhMaXN0KF9maWx0ZXJMYXllckJ5VHlwZShlbGVtZW50cywgc2VsZWN0b3IpLCAnbGF5ZXInKTtcblx0fVxuXG5cdGZ1bmN0aW9uIGdldExheWVyc0J5TmFtZShzZWxlY3Rvcikge1xuXHRcdHJldHVybiBLZXlQYXRoTGlzdChfZmlsdGVyTGF5ZXJCeU5hbWUoZWxlbWVudHMsIHNlbGVjdG9yKSwgJ2xheWVyJyk7XG5cdH1cblxuXHRmdW5jdGlvbiBnZXRQcm9wZXJ0aWVzQnlTZWxlY3RvcihzZWxlY3Rvcikge1xuXHRcdHJldHVybiBLZXlQYXRoTGlzdChlbGVtZW50cy5maWx0ZXIoZnVuY3Rpb24oZWxlbWVudCkge1xuXHRcdFx0cmV0dXJuIGVsZW1lbnQuaGFzUHJvcGVydHkoc2VsZWN0b3IpO1xuXHRcdH0pLm1hcChmdW5jdGlvbihlbGVtZW50KSB7XG5cdFx0XHRyZXR1cm4gZWxlbWVudC5nZXRQcm9wZXJ0eShzZWxlY3Rvcik7XG5cdFx0fSksICdwcm9wZXJ0eScpO1xuXHR9XG5cblx0ZnVuY3Rpb24gZ2V0TGF5ZXJQcm9wZXJ0eShzZWxlY3Rvcikge1xuXHRcdHZhciBsYXllcnMgPSBfZmlsdGVyTGF5ZXJCeVByb3BlcnR5KGVsZW1lbnRzLCBzZWxlY3Rvcik7XG5cdFx0dmFyIHByb3BlcnRpZXMgPSBsYXllcnMubWFwKGZ1bmN0aW9uKGVsZW1lbnQpe1xuXHRcdFx0cmV0dXJuIGVsZW1lbnQuZ2V0UHJvcGVydHkoc2VsZWN0b3IpO1xuXHRcdH0pXG5cdFx0cmV0dXJuIEtleVBhdGhMaXN0KHByb3BlcnRpZXMsICdwcm9wZXJ0eScpO1xuXHR9XG5cblx0ZnVuY3Rpb24gZ2V0S2V5UGF0aChwcm9wZXJ0eVBhdGgpIHtcblx0XHR2YXIga2V5UGF0aERhdGEgPSBrZXlQYXRoQnVpbGRlcihwcm9wZXJ0eVBhdGgpO1xuXHRcdHZhciBzZWxlY3RvciA9IGtleVBhdGhEYXRhLnNlbGVjdG9yO1xuXHRcdHZhciBub2Rlc0J5TmFtZSwgbm9kZXNCeVR5cGUsIHNlbGVjdGVkTm9kZXM7XG5cdFx0aWYgKG5vZGVfdHlwZSA9PT0gJ3JlbmRlcmVyJyB8fCBub2RlX3R5cGUgPT09ICdsYXllcicpIHtcblx0XHRcdG5vZGVzQnlOYW1lID0gZ2V0TGF5ZXJzQnlOYW1lKHNlbGVjdG9yKTtcblx0XHRcdG5vZGVzQnlUeXBlID0gZ2V0TGF5ZXJzQnlUeXBlKHNlbGVjdG9yKTtcblx0XHRcdGlmIChub2Rlc0J5TmFtZS5sZW5ndGggPT09IDAgJiYgbm9kZXNCeVR5cGUubGVuZ3RoID09PSAwKSB7XG5cdFx0XHRcdHNlbGVjdGVkTm9kZXMgPSBnZXRMYXllclByb3BlcnR5KHNlbGVjdG9yKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHNlbGVjdGVkTm9kZXMgPSBub2Rlc0J5TmFtZS5jb25jYXQobm9kZXNCeVR5cGUpO1xuXHRcdFx0fVxuXHRcdFx0aWYgKGtleVBhdGhEYXRhLnByb3BlcnR5UGF0aCkge1xuXHRcdFx0XHRyZXR1cm4gc2VsZWN0ZWROb2Rlcy5nZXRLZXlQYXRoKGtleVBhdGhEYXRhLnByb3BlcnR5UGF0aCk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRyZXR1cm4gc2VsZWN0ZWROb2Rlcztcblx0XHRcdH1cblx0XHR9IGVsc2UgaWYobm9kZV90eXBlID09PSAncHJvcGVydHknKSB7XG5cdFx0XHRzZWxlY3RlZE5vZGVzID0gZ2V0UHJvcGVydGllc0J5U2VsZWN0b3Ioc2VsZWN0b3IpO1xuXHRcdFx0aWYgKGtleVBhdGhEYXRhLnByb3BlcnR5UGF0aCkge1xuXHRcdFx0XHRyZXR1cm4gc2VsZWN0ZWROb2Rlcy5nZXRLZXlQYXRoKGtleVBhdGhEYXRhLnByb3BlcnR5UGF0aCk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRyZXR1cm4gc2VsZWN0ZWROb2Rlcztcblx0XHRcdH1cblx0XHR9XG5cdH1cblxuXHRmdW5jdGlvbiBjb25jYXQobm9kZXMpIHtcblx0XHR2YXIgbm9kZXNFbGVtZW50cyA9IG5vZGVzLmdldEVsZW1lbnRzKCk7XG5cdFx0cmV0dXJuIEtleVBhdGhMaXN0KGVsZW1lbnRzLmNvbmNhdChub2Rlc0VsZW1lbnRzKSwgbm9kZV90eXBlKTtcblx0fVxuXG5cdGZ1bmN0aW9uIGdldEVsZW1lbnRzKCkge1xuXHRcdHJldHVybiBlbGVtZW50cztcblx0fVxuXG5cdGZ1bmN0aW9uIGdldFByb3BlcnR5QXRJbmRleChpbmRleCkge1xuXHRcdHJldHVybiBlbGVtZW50c1tpbmRleF07XG5cdH1cblxuXHR2YXIgbWV0aG9kcyA9IHtcblx0XHRnZXRLZXlQYXRoOiBnZXRLZXlQYXRoLFxuXHRcdGNvbmNhdDogY29uY2F0LFxuXHRcdGdldEVsZW1lbnRzOiBnZXRFbGVtZW50cyxcblx0XHRnZXRQcm9wZXJ0eUF0SW5kZXg6IGdldFByb3BlcnR5QXRJbmRleFxuXHR9XG5cblx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG1ldGhvZHMsICdsZW5ndGgnLCB7XG5cdFx0Z2V0OiBfZ2V0TGVuZ3RoXG5cdH0pO1xuXG5cdHJldHVybiBtZXRob2RzO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IEtleVBhdGhMaXN0OyIsInZhciBrZXlfcGF0aF9zZXBhcmF0b3IgPSByZXF1aXJlKCcuLi9lbnVtcy9rZXlfcGF0aF9zZXBhcmF0b3InKTtcbnZhciBwcm9wZXJ0eV9uYW1lcyA9IHJlcXVpcmUoJy4uL2VudW1zL3Byb3BlcnR5X25hbWVzJyk7XG5cbmZ1bmN0aW9uIEtleVBhdGhOb2RlKHN0YXRlKSB7XG5cblx0ZnVuY3Rpb24gZ2V0UHJvcGVydHlCeVBhdGgoc2VsZWN0b3IsIHByb3BlcnR5UGF0aCkge1xuXHRcdHZhciBpbnN0YW5jZVByb3BlcnRpZXMgPSBzdGF0ZS5wcm9wZXJ0aWVzIHx8IFtdO1xuXHRcdHZhciBpID0gMCwgbGVuID0gaW5zdGFuY2VQcm9wZXJ0aWVzLmxlbmd0aDtcblx0XHR3aGlsZShpIDwgbGVuKSB7XG5cdFx0XHRpZihpbnN0YW5jZVByb3BlcnRpZXNbaV0ubmFtZSA9PT0gc2VsZWN0b3IpIHtcblx0XHRcdFx0cmV0dXJuIGluc3RhbmNlUHJvcGVydGllc1tpXS52YWx1ZTtcblx0XHRcdH1cblx0XHRcdGkgKz0gMTtcblx0XHR9XG5cdFx0cmV0dXJuIG51bGw7XG5cblx0fVxuXG5cdGZ1bmN0aW9uIGhhc1Byb3BlcnR5KHNlbGVjdG9yKSB7XG5cdFx0cmV0dXJuICEhZ2V0UHJvcGVydHlCeVBhdGgoc2VsZWN0b3IpO1xuXHR9XG5cblx0ZnVuY3Rpb24gZ2V0UHJvcGVydHkoc2VsZWN0b3IpIHtcblx0XHRyZXR1cm4gZ2V0UHJvcGVydHlCeVBhdGgoc2VsZWN0b3IpO1xuXHR9XG5cblx0ZnVuY3Rpb24gZnJvbUtleXBhdGhMYXllclBvaW50KHBvaW50KSB7XG5cdFx0cmV0dXJuIHN0YXRlLnBhcmVudC5mcm9tS2V5cGF0aExheWVyUG9pbnQocG9pbnQpO1xuXHR9XG5cblx0ZnVuY3Rpb24gdG9LZXlwYXRoTGF5ZXJQb2ludChwb2ludCkge1xuXHRcdHJldHVybiBzdGF0ZS5wYXJlbnQudG9LZXlwYXRoTGF5ZXJQb2ludChwb2ludCk7XG5cdH1cblxuXHR2YXIgbWV0aG9kcyA9IHtcblx0XHRoYXNQcm9wZXJ0eTogaGFzUHJvcGVydHksXG5cdFx0Z2V0UHJvcGVydHk6IGdldFByb3BlcnR5LFxuXHRcdGZyb21LZXlwYXRoTGF5ZXJQb2ludDogZnJvbUtleXBhdGhMYXllclBvaW50LFxuXHRcdHRvS2V5cGF0aExheWVyUG9pbnQ6IHRvS2V5cGF0aExheWVyUG9pbnRcblx0fVxuXHRyZXR1cm4gbWV0aG9kcztcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBLZXlQYXRoTm9kZTsiLCJ2YXIgS2V5UGF0aE5vZGUgPSByZXF1aXJlKCcuLi9rZXlfcGF0aC9LZXlQYXRoTm9kZScpO1xudmFyIFRyYW5zZm9ybSA9IHJlcXVpcmUoJy4vdHJhbnNmb3JtL1RyYW5zZm9ybScpO1xudmFyIEVmZmVjdHMgPSByZXF1aXJlKCcuL2VmZmVjdHMvRWZmZWN0cycpO1xudmFyIE1hdHJpeCA9IHJlcXVpcmUoJy4uL2hlbHBlcnMvdHJhbnNmb3JtYXRpb25NYXRyaXgnKTtcblxuZnVuY3Rpb24gTGF5ZXJCYXNlKHN0YXRlKSB7XG5cblx0dmFyIHRyYW5zZm9ybSA9IFRyYW5zZm9ybShzdGF0ZS5lbGVtZW50LmZpbmFsVHJhbnNmb3JtLm1Qcm9wLCBzdGF0ZSk7XG5cdHZhciBlZmZlY3RzID0gRWZmZWN0cyhzdGF0ZS5lbGVtZW50LmVmZmVjdHNNYW5hZ2VyLmVmZmVjdEVsZW1lbnRzIHx8IFtdLCBzdGF0ZSk7XG5cblx0ZnVuY3Rpb24gX2J1aWxkUHJvcGVydHlNYXAoKSB7XG5cdFx0c3RhdGUucHJvcGVydGllcy5wdXNoKHtcblx0XHRcdG5hbWU6ICd0cmFuc2Zvcm0nLFxuXHRcdFx0dmFsdWU6IHRyYW5zZm9ybVxuXHRcdH0se1xuXHRcdFx0bmFtZTogJ1RyYW5zZm9ybScsXG5cdFx0XHR2YWx1ZTogdHJhbnNmb3JtXG5cdFx0fSx7XG5cdFx0XHRuYW1lOiAnRWZmZWN0cycsXG5cdFx0XHR2YWx1ZTogZWZmZWN0c1xuXHRcdH0se1xuXHRcdFx0bmFtZTogJ2VmZmVjdHMnLFxuXHRcdFx0dmFsdWU6IGVmZmVjdHNcblx0XHR9KVxuXHR9XG5cbiAgICBmdW5jdGlvbiBnZXRFbGVtZW50VG9Qb2ludChwb2ludCkge1xuICAgIH1cblxuXHRmdW5jdGlvbiB0b0tleXBhdGhMYXllclBvaW50KHBvaW50KSB7XG5cdFx0dmFyIGVsZW1lbnQgPSBzdGF0ZS5lbGVtZW50O1xuICAgIFx0aWYoc3RhdGUucGFyZW50LnRvS2V5cGF0aExheWVyUG9pbnQpIHtcbiAgICAgICAgXHRwb2ludCA9IHN0YXRlLnBhcmVudC50b0tleXBhdGhMYXllclBvaW50KHBvaW50KTtcbiAgICAgICAgfVxuICAgIFx0dmFyIHRvV29ybGRNYXQgPSBNYXRyaXgoKTtcbiAgICAgICAgdmFyIHRyYW5zZm9ybU1hdCA9IHN0YXRlLmdldFByb3BlcnR5KCdUcmFuc2Zvcm0nKS5nZXRUYXJnZXRUcmFuc2Zvcm0oKTtcbiAgICAgICAgdHJhbnNmb3JtTWF0LmFwcGx5VG9NYXRyaXgodG9Xb3JsZE1hdCk7XG4gICAgICAgIGlmKGVsZW1lbnQuaGllcmFyY2h5ICYmIGVsZW1lbnQuaGllcmFyY2h5Lmxlbmd0aCl7XG4gICAgICAgICAgICB2YXIgaSwgbGVuID0gZWxlbWVudC5oaWVyYXJjaHkubGVuZ3RoO1xuICAgICAgICAgICAgZm9yKGk9MDtpPGxlbjtpKz0xKXtcbiAgICAgICAgICAgICAgICBlbGVtZW50LmhpZXJhcmNoeVtpXS5maW5hbFRyYW5zZm9ybS5tUHJvcC5hcHBseVRvTWF0cml4KHRvV29ybGRNYXQpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0b1dvcmxkTWF0LmludmVyc2VQb2ludChwb2ludCk7XG5cdH1cblxuXHRmdW5jdGlvbiBmcm9tS2V5cGF0aExheWVyUG9pbnQocG9pbnQpIHtcblx0XHR2YXIgZWxlbWVudCA9IHN0YXRlLmVsZW1lbnQ7XG5cdFx0dmFyIHRvV29ybGRNYXQgPSBNYXRyaXgoKTtcbiAgICAgICAgdmFyIHRyYW5zZm9ybU1hdCA9IHN0YXRlLmdldFByb3BlcnR5KCdUcmFuc2Zvcm0nKS5nZXRUYXJnZXRUcmFuc2Zvcm0oKTtcbiAgICAgICAgdHJhbnNmb3JtTWF0LmFwcGx5VG9NYXRyaXgodG9Xb3JsZE1hdCk7XG4gICAgICAgIGlmKGVsZW1lbnQuaGllcmFyY2h5ICYmIGVsZW1lbnQuaGllcmFyY2h5Lmxlbmd0aCl7XG4gICAgICAgICAgICB2YXIgaSwgbGVuID0gZWxlbWVudC5oaWVyYXJjaHkubGVuZ3RoO1xuICAgICAgICAgICAgZm9yKGk9MDtpPGxlbjtpKz0xKXtcbiAgICAgICAgICAgICAgICBlbGVtZW50LmhpZXJhcmNoeVtpXS5maW5hbFRyYW5zZm9ybS5tUHJvcC5hcHBseVRvTWF0cml4KHRvV29ybGRNYXQpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHBvaW50ID0gdG9Xb3JsZE1hdC5hcHBseVRvUG9pbnRBcnJheShwb2ludFswXSxwb2ludFsxXSxwb2ludFsyXXx8MCk7XG4gICAgICAgIGlmKHN0YXRlLnBhcmVudC5mcm9tS2V5cGF0aExheWVyUG9pbnQpIHtcbiAgICAgICAgXHRyZXR1cm4gc3RhdGUucGFyZW50LmZyb21LZXlwYXRoTGF5ZXJQb2ludChwb2ludCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgIFx0cmV0dXJuIHBvaW50O1xuICAgICAgICB9XG5cdH1cblxuXHRmdW5jdGlvbiBnZXRUYXJnZXRMYXllcigpIHtcblx0XHRyZXR1cm4gc3RhdGUuZWxlbWVudDtcblx0fVxuXG5cdHZhciBtZXRob2RzID0ge1xuXHRcdGdldFRhcmdldExheWVyOiBnZXRUYXJnZXRMYXllcixcblx0XHR0b0tleXBhdGhMYXllclBvaW50OiB0b0tleXBhdGhMYXllclBvaW50LFxuXHRcdGZyb21LZXlwYXRoTGF5ZXJQb2ludDogZnJvbUtleXBhdGhMYXllclBvaW50XG5cdH1cblxuXHRfYnVpbGRQcm9wZXJ0eU1hcCgpO1xuXG5cdHJldHVybiBPYmplY3QuYXNzaWduKHN0YXRlLCBLZXlQYXRoTm9kZShzdGF0ZSksIG1ldGhvZHMpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IExheWVyQmFzZTsiLCJ2YXIgbGF5ZXJfdHlwZXMgPSByZXF1aXJlKCcuLi9lbnVtcy9sYXllcl90eXBlcycpO1xudmFyIGxheWVyX2FwaSA9IHJlcXVpcmUoJy4uL2hlbHBlcnMvbGF5ZXJBUElCdWlsZGVyJyk7XG5cbmZ1bmN0aW9uIExheWVyTGlzdChlbGVtZW50cykge1xuXG5cdGZ1bmN0aW9uIF9nZXRMZW5ndGgoKSB7XG5cdFx0cmV0dXJuIGVsZW1lbnRzLmxlbmd0aDtcblx0fVxuXG5cdGZ1bmN0aW9uIF9maWx0ZXJMYXllckJ5VHlwZShlbGVtZW50cywgdHlwZSkge1xuXHRcdHJldHVybiBlbGVtZW50cy5maWx0ZXIoZnVuY3Rpb24oZWxlbWVudCkge1xuXHRcdFx0cmV0dXJuIGVsZW1lbnQuZGF0YS50eSA9PT0gbGF5ZXJfdHlwZXNbdHlwZV07XG5cdFx0fSk7XG5cdH1cblxuXHRmdW5jdGlvbiBfZmlsdGVyTGF5ZXJCeU5hbWUoZWxlbWVudHMsIG5hbWUpIHtcblx0XHRyZXR1cm4gZWxlbWVudHMuZmlsdGVyKGZ1bmN0aW9uKGVsZW1lbnQpIHtcblx0XHRcdHJldHVybiBlbGVtZW50LmRhdGEubm0gPT09IG5hbWU7XG5cdFx0fSk7XG5cdH1cblxuXHRmdW5jdGlvbiBnZXRMYXllcnMoKSB7XG5cdFx0IHJldHVybiBMYXllckxpc3QoZWxlbWVudHMpO1xuXHR9XG5cblx0ZnVuY3Rpb24gZ2V0TGF5ZXJzQnlUeXBlKHR5cGUpIHtcblx0XHR2YXIgZWxlbWVudHNMaXN0ID0gX2ZpbHRlckxheWVyQnlUeXBlKGVsZW1lbnRzLCB0eXBlKTtcblx0XHRyZXR1cm4gTGF5ZXJMaXN0KGVsZW1lbnRzTGlzdCk7XG5cdH1cblxuXHRmdW5jdGlvbiBnZXRMYXllcnNCeU5hbWUodHlwZSkge1xuXHRcdHZhciBlbGVtZW50c0xpc3QgPSBfZmlsdGVyTGF5ZXJCeU5hbWUoZWxlbWVudHMsIHR5cGUpO1xuXHRcdHJldHVybiBMYXllckxpc3QoZWxlbWVudHNMaXN0KTtcblx0fVxuXG5cdGZ1bmN0aW9uIGxheWVyKGluZGV4KSB7XG5cdFx0aWYgKGluZGV4ID49IGVsZW1lbnRzLmxlbmd0aCkge1xuXHRcdFx0cmV0dXJuIFtdO1xuXHRcdH1cblx0XHRyZXR1cm4gbGF5ZXJfYXBpKGVsZW1lbnRzW3BhcnNlSW50KGluZGV4KV0pO1xuXHR9XG5cblx0ZnVuY3Rpb24gYWRkSXRlcmF0YWJsZU1ldGhvZHMoaXRlcmF0YWJsZU1ldGhvZHMsIGxpc3QpIHtcblx0XHRpdGVyYXRhYmxlTWV0aG9kcy5yZWR1Y2UoZnVuY3Rpb24oYWNjdW11bGF0b3IsIHZhbHVlKXtcblx0XHRcdHZhciBfdmFsdWUgPSB2YWx1ZTtcblx0XHRcdGFjY3VtdWxhdG9yW3ZhbHVlXSA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHR2YXIgX2FyZ3VtZW50cyA9IGFyZ3VtZW50cztcblx0XHRcdFx0cmV0dXJuIGVsZW1lbnRzLm1hcChmdW5jdGlvbihlbGVtZW50KXtcblx0XHRcdFx0XHR2YXIgbGF5ZXIgPSBsYXllcl9hcGkoZWxlbWVudCk7XG5cdFx0XHRcdFx0aWYobGF5ZXJbX3ZhbHVlXSkge1xuXHRcdFx0XHRcdFx0cmV0dXJuIGxheWVyW192YWx1ZV0uYXBwbHkobnVsbCwgX2FyZ3VtZW50cyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdHJldHVybiBudWxsO1xuXHRcdFx0XHR9KTtcblx0XHRcdH1cblx0XHRcdHJldHVybiBhY2N1bXVsYXRvcjtcblx0XHR9LCBtZXRob2RzKTtcblx0fVxuXG5cdGZ1bmN0aW9uIGdldFRhcmdldEVsZW1lbnRzKCkge1xuXHRcdHJldHVybiBlbGVtZW50cztcblx0fVxuXG5cdGZ1bmN0aW9uIGNvbmNhdChsaXN0KSB7XG5cdFx0cmV0dXJuIGVsZW1lbnRzLmNvbmNhdChsaXN0LmdldFRhcmdldEVsZW1lbnRzKCkpO1xuXHR9XG5cblx0dmFyIG1ldGhvZHMgPSB7XG5cdFx0Z2V0TGF5ZXJzOiBnZXRMYXllcnMsXG5cdFx0Z2V0TGF5ZXJzQnlUeXBlOiBnZXRMYXllcnNCeVR5cGUsXG5cdFx0Z2V0TGF5ZXJzQnlOYW1lOiBnZXRMYXllcnNCeU5hbWUsXG5cdFx0bGF5ZXI6IGxheWVyLFxuXHRcdGNvbmNhdDogY29uY2F0LFxuXHRcdGdldFRhcmdldEVsZW1lbnRzOiBnZXRUYXJnZXRFbGVtZW50c1xuXHR9O1xuXG5cdGFkZEl0ZXJhdGFibGVNZXRob2RzKFsnc2V0VHJhbnNsYXRlJywgJ2dldFR5cGUnLCAnZ2V0RHVyYXRpb24nXSk7XG5cdGFkZEl0ZXJhdGFibGVNZXRob2RzKFsnc2V0VGV4dCcsICdnZXRUZXh0JywgJ3NldERvY3VtZW50RGF0YScsICdjYW5SZXNpemVGb250JywgJ3NldE1pbmltdW1Gb250U2l6ZSddKTtcblxuXHRPYmplY3QuZGVmaW5lUHJvcGVydHkobWV0aG9kcywgJ2xlbmd0aCcsIHtcblx0XHRnZXQ6IF9nZXRMZW5ndGhcblx0fSk7XG5cdHJldHVybiBtZXRob2RzO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IExheWVyTGlzdDsiLCJ2YXIgS2V5UGF0aE5vZGUgPSByZXF1aXJlKCcuLi8uLi9rZXlfcGF0aC9LZXlQYXRoTm9kZScpO1xudmFyIFByb3BlcnR5ID0gcmVxdWlyZSgnLi4vLi4vcHJvcGVydHkvUHJvcGVydHknKTtcblxuZnVuY3Rpb24gQ2FtZXJhKGVsZW1lbnQsIHBhcmVudCkge1xuXG5cdHZhciBpbnN0YW5jZSA9IHt9O1xuXG5cdHZhciBzdGF0ZSA9IHtcblx0XHRlbGVtZW50OiBlbGVtZW50LFxuXHRcdHBhcmVudDogcGFyZW50LFxuXHRcdHByb3BlcnRpZXM6IF9idWlsZFByb3BlcnR5TWFwKClcblx0fVxuXG5cdGZ1bmN0aW9uIF9idWlsZFByb3BlcnR5TWFwKCkge1xuXHRcdHJldHVybiBbXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdQb2ludCBvZiBJbnRlcmVzdCcsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShlbGVtZW50LmEsIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdab29tJyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KGVsZW1lbnQucGUsIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdQb3NpdGlvbicsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShlbGVtZW50LnAsIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdYIFJvdGF0aW9uJyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KGVsZW1lbnQucngsIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdZIFJvdGF0aW9uJyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KGVsZW1lbnQucnksIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdaIFJvdGF0aW9uJyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KGVsZW1lbnQucnosIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdPcmllbnRhdGlvbicsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShlbGVtZW50Lm9yLCBwYXJlbnQpXG5cdFx0XHR9XG5cdFx0XVxuXHR9XG5cblx0ZnVuY3Rpb24gZ2V0VGFyZ2V0TGF5ZXIoKSB7XG5cdFx0cmV0dXJuIHN0YXRlLmVsZW1lbnQ7XG5cdH1cblxuXHR2YXIgbWV0aG9kcyA9IHtcblx0XHRnZXRUYXJnZXRMYXllcjogZ2V0VGFyZ2V0TGF5ZXJcblx0fVxuXG5cdHJldHVybiBPYmplY3QuYXNzaWduKGluc3RhbmNlLCBLZXlQYXRoTm9kZShzdGF0ZSksIG1ldGhvZHMpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IENhbWVyYTsiLCJ2YXIgS2V5UGF0aExpc3QgPSByZXF1aXJlKCcuLi8uLi9rZXlfcGF0aC9LZXlQYXRoTGlzdCcpO1xudmFyIExheWVyQmFzZSA9IHJlcXVpcmUoJy4uL0xheWVyQmFzZScpO1xudmFyIGxheWVyX2FwaSA9IHJlcXVpcmUoJy4uLy4uL2hlbHBlcnMvbGF5ZXJBUElCdWlsZGVyJyk7XG52YXIgUHJvcGVydHkgPSByZXF1aXJlKCcuLi8uLi9wcm9wZXJ0eS9Qcm9wZXJ0eScpO1xudmFyIFRpbWVSZW1hcCA9IHJlcXVpcmUoJy4vVGltZVJlbWFwJyk7XG5cbmZ1bmN0aW9uIENvbXBvc2l0aW9uKGVsZW1lbnQsIHBhcmVudCkge1xuXG5cdHZhciBpbnN0YW5jZSA9IHt9O1xuXG5cdHZhciBzdGF0ZSA9IHtcblx0XHRlbGVtZW50OiBlbGVtZW50LFxuXHRcdHBhcmVudDogcGFyZW50LFxuXHRcdHByb3BlcnRpZXM6IF9idWlsZFByb3BlcnR5TWFwKClcblx0fVxuXG5cdGZ1bmN0aW9uIGJ1aWxkTGF5ZXJBcGkobGF5ZXIsIGluZGV4KSB7XG5cdFx0dmFyIF9sYXllckFwaSA9IG51bGw7XG5cdFx0dmFyIG9iID0ge1xuXHRcdFx0bmFtZTogbGF5ZXIubm1cblx0XHR9XG5cblx0XHRmdW5jdGlvbiBnZXRMYXllckFwaSgpIHtcblx0XHRcdGlmKCFfbGF5ZXJBcGkpIHtcblx0XHRcdFx0X2xheWVyQXBpID0gbGF5ZXJfYXBpKGVsZW1lbnQuZWxlbWVudHNbaW5kZXhdLCBzdGF0ZSlcblx0XHRcdH1cblx0XHRcdHJldHVybiBfbGF5ZXJBcGlcblx0XHR9XG5cblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkob2IsICd2YWx1ZScsIHtcblx0XHRcdGdldCA6IGdldExheWVyQXBpXG5cdFx0fSlcblx0XHRyZXR1cm4gb2I7XG5cdH1cblxuXHRcblx0ZnVuY3Rpb24gX2J1aWxkUHJvcGVydHlNYXAoKSB7XG5cdFx0dmFyIGNvbXBvc2l0aW9uTGF5ZXJzID0gZWxlbWVudC5sYXllcnMubWFwKGJ1aWxkTGF5ZXJBcGkpXG5cdFx0cmV0dXJuIFtcblx0XHRcdHtcblx0XHRcdFx0bmFtZTogJ1RpbWUgUmVtYXAnLFxuXHRcdFx0XHR2YWx1ZTogVGltZVJlbWFwKGVsZW1lbnQudG0pXG5cdFx0XHR9XG5cdFx0XS5jb25jYXQoY29tcG9zaXRpb25MYXllcnMpXG5cdH1cblxuXHR2YXIgbWV0aG9kcyA9IHtcblx0fVxuXG5cdHJldHVybiBPYmplY3QuYXNzaWduKGluc3RhbmNlLCBMYXllckJhc2Uoc3RhdGUpLCBLZXlQYXRoTGlzdChzdGF0ZS5lbGVtZW50cywgJ2xheWVyJyksIG1ldGhvZHMpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvc2l0aW9uOyIsInZhciBLZXlQYXRoTm9kZSA9IHJlcXVpcmUoJy4uLy4uL2tleV9wYXRoL0tleVBhdGhOb2RlJyk7XG52YXIgVmFsdWVQcm9wZXJ0eSA9IHJlcXVpcmUoJy4uLy4uL3Byb3BlcnR5L1ZhbHVlUHJvcGVydHknKTtcblxuZnVuY3Rpb24gVGltZVJlbWFwKHByb3BlcnR5LCBwYXJlbnQpIHtcblx0dmFyIHN0YXRlID0ge1xuXHRcdHByb3BlcnR5OiBwcm9wZXJ0eSxcblx0XHRwYXJlbnQ6IHBhcmVudFxuXHR9XG5cblx0dmFyIF9pc0NhbGxiYWNrQWRkZWQgPSBmYWxzZTtcblx0dmFyIGN1cnJlbnRTZWdtZW50SW5pdCA9IDA7XG5cdHZhciBjdXJyZW50U2VnbWVudEVuZCA9IDA7XG5cdHZhciBwcmV2aW91c1RpbWUgPSAwLCBjdXJyZW50VGltZSA9IDA7XG5cdHZhciBpbml0VGltZSA9IDA7XG5cdHZhciBfbG9vcCA9IHRydWU7XG5cdHZhciBfbG9vcENvdW50ID0gMDtcblx0dmFyIF9zcGVlZCA9IDE7XG5cdHZhciBfcGF1c2VkID0gZmFsc2U7XG5cdHZhciBfaXNEZWJ1Z2dpbmcgPSBmYWxzZTtcblx0dmFyIHF1ZXVlZFNlZ21lbnRzID0gW107XG5cdHZhciBfZGVzdHJveUZ1bmN0aW9uO1xuXHR2YXIgZW50ZXJGcmFtZUNhbGxiYWNrID0gbnVsbDtcblx0dmFyIGVudGVyRnJhbWVFdmVudCA9IHtcblx0XHR0aW1lOiAtMVxuXHR9XG5cblx0ZnVuY3Rpb24gcGxheVNlZ21lbnQoaW5pdCwgZW5kLCBjbGVhcikge1xuXHRcdF9wYXVzZWQgPSBmYWxzZTtcblx0XHRpZihjbGVhcikge1xuXHRcdFx0Y2xlYXJRdWV1ZSgpO1xuXHRcdFx0Y3VycmVudFRpbWUgPSBpbml0O1xuXHRcdH1cblx0XHRpZihfaXNEZWJ1Z2dpbmcpIHtcblx0XHRcdGNvbnNvbGUubG9nKGluaXQsIGVuZCk7XG5cdFx0fVxuXHRcdF9sb29wQ291bnQgPSAwO1xuXHRcdHByZXZpb3VzVGltZSA9IERhdGUubm93KCk7XG5cdFx0Y3VycmVudFNlZ21lbnRJbml0ID0gaW5pdDtcblx0XHRjdXJyZW50U2VnbWVudEVuZCA9IGVuZDtcblx0XHRhZGRDYWxsYmFjaygpO1xuXHR9XG5cblx0ZnVuY3Rpb24gcGxheVF1ZXVlZFNlZ21lbnQoKSB7XG5cdFx0dmFyIG5ld1NlZ21lbnQgPSBxdWV1ZWRTZWdtZW50cy5zaGlmdCgpO1xuXHRcdHBsYXlTZWdtZW50KG5ld1NlZ21lbnRbMF0sIG5ld1NlZ21lbnRbMV0pO1xuXHR9XG5cblx0ZnVuY3Rpb24gcXVldWVTZWdtZW50KGluaXQsIGVuZCkge1xuXHRcdHF1ZXVlZFNlZ21lbnRzLnB1c2goW2luaXQsIGVuZF0pO1xuXHR9XG5cblx0ZnVuY3Rpb24gY2xlYXJRdWV1ZSgpIHtcblx0XHRxdWV1ZWRTZWdtZW50cy5sZW5ndGggPSAwO1xuXHR9XG5cblx0ZnVuY3Rpb24gX3NlZ21lbnRQbGF5ZXIoY3VycmVudFZhbHVlKSB7XG5cdFx0aWYoY3VycmVudFNlZ21lbnRJbml0ID09PSBjdXJyZW50U2VnbWVudEVuZCkge1xuXHRcdFx0Y3VycmVudFRpbWUgPSBjdXJyZW50U2VnbWVudEluaXQ7XG5cdFx0fSBlbHNlIGlmKCFfcGF1c2VkKSB7XG5cdFx0XHR2YXIgbm93VGltZSA9IERhdGUubm93KCk7XG5cdFx0XHR2YXIgZWxhcHNlZFRpbWUgPSBfc3BlZWQgKiAobm93VGltZSAtIHByZXZpb3VzVGltZSkgLyAxMDAwO1xuXHRcdFx0cHJldmlvdXNUaW1lID0gbm93VGltZTtcblx0XHRcdGlmKGN1cnJlbnRTZWdtZW50SW5pdCA8IGN1cnJlbnRTZWdtZW50RW5kKSB7XG5cdFx0XHRcdGN1cnJlbnRUaW1lICs9IGVsYXBzZWRUaW1lO1xuXHRcdFx0XHRpZihjdXJyZW50VGltZSA+IGN1cnJlbnRTZWdtZW50RW5kKSB7XG5cdFx0XHRcdFx0X2xvb3BDb3VudCArPSAxO1xuXHRcdFx0XHRcdGlmKHF1ZXVlZFNlZ21lbnRzLmxlbmd0aCkge1xuXHRcdFx0XHRcdFx0cGxheVF1ZXVlZFNlZ21lbnQoKTtcblx0XHRcdFx0XHR9IGVsc2UgaWYoIV9sb29wKSB7XG5cdFx0XHRcdFx0XHRjdXJyZW50VGltZSA9IGN1cnJlbnRTZWdtZW50RW5kO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHQvKmN1cnJlbnRUaW1lIC09IE1hdGguZmxvb3IoY3VycmVudFRpbWUgLyAoY3VycmVudFNlZ21lbnRFbmQgLSBjdXJyZW50U2VnbWVudEluaXQpKSAqIChjdXJyZW50U2VnbWVudEVuZCAtIGN1cnJlbnRTZWdtZW50SW5pdCk7XG5cdFx0XHRcdFx0XHRjdXJyZW50VGltZSA9IGN1cnJlbnRTZWdtZW50SW5pdCArIGN1cnJlbnRUaW1lOyovXG5cdFx0XHRcdFx0XHRjdXJyZW50VGltZSA9IGN1cnJlbnRUaW1lICUgKGN1cnJlbnRTZWdtZW50RW5kIC0gY3VycmVudFNlZ21lbnRJbml0KTtcblx0XHRcdFx0XHRcdC8vY3VycmVudFRpbWUgPSBjdXJyZW50U2VnbWVudEluaXQgKyAoY3VycmVudFRpbWUpO1xuXHRcdFx0XHRcdFx0Ly9jdXJyZW50VGltZSA9IGN1cnJlbnRTZWdtZW50SW5pdCArIChjdXJyZW50VGltZSAtIGN1cnJlbnRTZWdtZW50RW5kKTtcblx0XHRcdFx0XHRcdCAvL2NvbnNvbGUubG9nKCdDVDogJywgY3VycmVudFRpbWUpIFxuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0Y3VycmVudFRpbWUgLT0gZWxhcHNlZFRpbWU7XG5cdFx0XHRcdGlmKGN1cnJlbnRUaW1lIDwgY3VycmVudFNlZ21lbnRFbmQpIHtcblx0XHRcdFx0XHRfbG9vcENvdW50ICs9IDE7XG5cdFx0XHRcdFx0aWYocXVldWVkU2VnbWVudHMubGVuZ3RoKSB7XG5cdFx0XHRcdFx0XHRwbGF5UXVldWVkU2VnbWVudCgpO1xuXHRcdFx0XHRcdH0gZWxzZSBpZighX2xvb3ApIHtcblx0XHRcdFx0XHRcdGN1cnJlbnRUaW1lID0gY3VycmVudFNlZ21lbnRFbmQ7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdGN1cnJlbnRUaW1lID0gY3VycmVudFNlZ21lbnRJbml0IC0gKGN1cnJlbnRTZWdtZW50RW5kIC0gY3VycmVudFRpbWUpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdFx0aWYoX2lzRGVidWdnaW5nKSB7XG5cdFx0XHRcdGNvbnNvbGUubG9nKGN1cnJlbnRUaW1lKVxuXHRcdFx0fVxuXHRcdH1cblx0XHRpZihpbnN0YW5jZS5vbkVudGVyRnJhbWUgJiYgZW50ZXJGcmFtZUV2ZW50LnRpbWUgIT09IGN1cnJlbnRUaW1lKSB7XG5cdFx0XHRlbnRlckZyYW1lRXZlbnQudGltZSA9IGN1cnJlbnRUaW1lO1xuXHRcdFx0aW5zdGFuY2Uub25FbnRlckZyYW1lKGVudGVyRnJhbWVFdmVudCk7XG5cdFx0fVxuXHRcdHJldHVybiBjdXJyZW50VGltZTtcblx0fVxuXG5cdGZ1bmN0aW9uIGFkZENhbGxiYWNrKCkge1xuXHRcdGlmKCFfaXNDYWxsYmFja0FkZGVkKSB7XG5cdFx0XHRfaXNDYWxsYmFja0FkZGVkID0gdHJ1ZTtcblx0XHRcdF9kZXN0cm95RnVuY3Rpb24gPSBpbnN0YW5jZS5zZXRWYWx1ZShfc2VnbWVudFBsYXllciwgX2lzRGVidWdnaW5nKVxuXHRcdH1cblx0fVxuXG5cdGZ1bmN0aW9uIHBsYXlUbyhlbmQsIGNsZWFyKSB7XG5cdFx0X3BhdXNlZCA9IGZhbHNlO1xuXHRcdGlmKGNsZWFyKSB7XG5cdFx0XHRjbGVhclF1ZXVlKCk7XG5cdFx0fVxuXHRcdGFkZENhbGxiYWNrKCk7XG5cdFx0Y3VycmVudFNlZ21lbnRFbmQgPSBlbmQ7XG5cdH1cblxuXHRmdW5jdGlvbiBnZXRDdXJyZW50VGltZSgpIHtcblx0XHRpZihfaXNDYWxsYmFja0FkZGVkKSB7XG5cdFx0XHRyZXR1cm4gY3VycmVudFRpbWU7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHJldHVybiBwcm9wZXJ0eS52IC8gcHJvcGVydHkubXVsdDtcblx0XHR9XG5cdH1cblxuXHRmdW5jdGlvbiBzZXRMb29wKGZsYWcpIHtcblx0XHRfbG9vcCA9IGZsYWc7XG5cdH1cblxuXHRmdW5jdGlvbiBzZXRTcGVlZCh2YWx1ZSkge1xuXHRcdF9zcGVlZCA9IHZhbHVlO1xuXHR9XG5cblx0ZnVuY3Rpb24gc2V0RGVidWdnaW5nKGZsYWcpIHtcblx0XHRfaXNEZWJ1Z2dpbmcgPSBmbGFnO1xuXHR9XG5cblx0ZnVuY3Rpb24gcGF1c2UoKSB7XG5cdFx0X3BhdXNlZCA9IHRydWU7XG5cdH1cblxuXHRmdW5jdGlvbiBkZXN0cm95KCkge1xuXHRcdGlmKF9kZXN0cm95RnVuY3Rpb24pIHtcblx0XHRcdF9kZXN0cm95RnVuY3Rpb24oKTtcblx0XHRcdHN0YXRlLnByb3BlcnR5ID0gbnVsbDtcblx0XHRcdHN0YXRlLnBhcmVudCA9IG51bGw7XG5cdFx0fVxuXHR9XG5cblx0dmFyIG1ldGhvZHMgPSB7XG5cdFx0cGxheVNlZ21lbnQ6IHBsYXlTZWdtZW50LFxuXHRcdHBsYXlUbzogcGxheVRvLFxuXHRcdHF1ZXVlU2VnbWVudDogcXVldWVTZWdtZW50LFxuXHRcdGNsZWFyUXVldWU6IGNsZWFyUXVldWUsXG5cdFx0c2V0TG9vcDogc2V0TG9vcCxcblx0XHRzZXRTcGVlZDogc2V0U3BlZWQsXG5cdFx0cGF1c2U6IHBhdXNlLFxuXHRcdHNldERlYnVnZ2luZzogc2V0RGVidWdnaW5nLFxuXHRcdGdldEN1cnJlbnRUaW1lOiBnZXRDdXJyZW50VGltZSxcblx0XHRvbkVudGVyRnJhbWU6IG51bGwsXG5cdFx0ZGVzdHJveTogZGVzdHJveVxuXHR9XG5cblx0dmFyIGluc3RhbmNlID0ge31cblxuXHRyZXR1cm4gT2JqZWN0LmFzc2lnbihpbnN0YW5jZSwgbWV0aG9kcywgVmFsdWVQcm9wZXJ0eShzdGF0ZSksIEtleVBhdGhOb2RlKHN0YXRlKSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gVGltZVJlbWFwOyIsInZhciBQcm9wZXJ0eSA9IHJlcXVpcmUoJy4uLy4uL3Byb3BlcnR5L1Byb3BlcnR5Jyk7XG5cbmZ1bmN0aW9uIEVmZmVjdEVsZW1lbnQoZWZmZWN0LCBwYXJlbnQpIHtcblxuXHRyZXR1cm4gUHJvcGVydHkoZWZmZWN0LnAsIHBhcmVudCk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gRWZmZWN0RWxlbWVudDsiLCJ2YXIgS2V5UGF0aE5vZGUgPSByZXF1aXJlKCcuLi8uLi9rZXlfcGF0aC9LZXlQYXRoTm9kZScpO1xudmFyIFByb3BlcnR5ID0gcmVxdWlyZSgnLi4vLi4vcHJvcGVydHkvUHJvcGVydHknKTtcbnZhciBFZmZlY3RFbGVtZW50ID0gcmVxdWlyZSgnLi9FZmZlY3RFbGVtZW50Jyk7XG5cbmZ1bmN0aW9uIEVmZmVjdHMoZWZmZWN0cywgcGFyZW50KSB7XG5cblx0dmFyIHN0YXRlID0ge1xuXHRcdHBhcmVudDogcGFyZW50LFxuXHRcdHByb3BlcnRpZXM6IGJ1aWxkUHJvcGVydGllcygpXG5cdH1cblxuXHRmdW5jdGlvbiBnZXRWYWx1ZShlZmZlY3REYXRhLCBpbmRleCkge1xuXHRcdHZhciBubSA9IGVmZmVjdERhdGEuZGF0YSA/IGVmZmVjdERhdGEuZGF0YS5ubSA6IGluZGV4LnRvU3RyaW5nKCk7XG5cdFx0dmFyIGVmZmVjdEVsZW1lbnQgPSBlZmZlY3REYXRhLmRhdGEgPyBFZmZlY3RzKGVmZmVjdERhdGEuZWZmZWN0RWxlbWVudHMsIHBhcmVudCkgOiBQcm9wZXJ0eShlZmZlY3REYXRhLnAsIHBhcmVudCk7XG5cdFx0cmV0dXJuIHtcblx0XHRcdG5hbWU6IG5tLFxuXHRcdFx0dmFsdWU6IGVmZmVjdEVsZW1lbnRcblx0XHR9XG5cdH1cblxuXHRmdW5jdGlvbiBidWlsZFByb3BlcnRpZXMoKSB7XG5cdFx0dmFyIGksIGxlbiA9IGVmZmVjdHMubGVuZ3RoO1xuXHRcdHZhciBhcnIgPSBbXTtcblx0XHRmb3IgKGkgPSAwOyBpIDwgbGVuOyBpICs9IDEpIHtcblx0XHRcdGFyci5wdXNoKGdldFZhbHVlKGVmZmVjdHNbaV0sIGkpKTtcblx0XHR9XG5cdFx0cmV0dXJuIGFycjtcblx0fVxuXG5cdHZhciBtZXRob2RzID0ge1xuXHR9XG5cblx0cmV0dXJuIE9iamVjdC5hc3NpZ24obWV0aG9kcywgS2V5UGF0aE5vZGUoc3RhdGUpKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBFZmZlY3RzOyIsInZhciBMYXllckJhc2UgPSByZXF1aXJlKCcuLi9MYXllckJhc2UnKTtcblxuZnVuY3Rpb24gSW1hZ2UoZWxlbWVudCkge1xuXHR2YXIgc3RhdGUgPSB7XG5cdFx0ZWxlbWVudDogZWxlbWVudCxcblx0XHRwcm9wZXJ0aWVzOiBbXVxuXHR9XG5cblx0dmFyIG1ldGhvZHMgPSB7XG5cdH1cblxuXHRyZXR1cm4gT2JqZWN0LmFzc2lnbih7fSwgTGF5ZXJCYXNlKHN0YXRlKSwgbWV0aG9kcyk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gSW1hZ2U7IiwidmFyIExheWVyQmFzZSA9IHJlcXVpcmUoJy4uL0xheWVyQmFzZScpO1xuXG5mdW5jdGlvbiBOdWxsRWxlbWVudChlbGVtZW50LCBwYXJlbnQpIHtcblxuXHR2YXIgaW5zdGFuY2UgPSB7fTtcblxuXHR2YXIgc3RhdGUgPSB7XG5cdFx0ZWxlbWVudDogZWxlbWVudCxcblx0XHRwYXJlbnQ6IHBhcmVudCxcblx0XHRwcm9wZXJ0aWVzOiBfYnVpbGRQcm9wZXJ0eU1hcCgpXG5cdH1cblxuXHRmdW5jdGlvbiBfYnVpbGRQcm9wZXJ0eU1hcCgpIHtcblx0XHRyZXR1cm4gW1xuXHRcdF1cblx0fVxuXG5cdHZhciBtZXRob2RzID0ge1xuXHR9XG5cblx0cmV0dXJuIE9iamVjdC5hc3NpZ24oaW5zdGFuY2UsIExheWVyQmFzZShzdGF0ZSksIG1ldGhvZHMpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IE51bGxFbGVtZW50OyIsInZhciBMYXllckJhc2UgPSByZXF1aXJlKCcuLi9MYXllckJhc2UnKTtcbnZhciBTaGFwZUNvbnRlbnRzID0gcmVxdWlyZSgnLi9TaGFwZUNvbnRlbnRzJyk7XG5cbmZ1bmN0aW9uIFNoYXBlKGVsZW1lbnQsIHBhcmVudCkge1xuXG5cdHZhciBzdGF0ZSA9IHtcblx0XHRwcm9wZXJ0aWVzOiBbXSxcblx0XHRwYXJlbnQ6IHBhcmVudCxcblx0XHRlbGVtZW50OiBlbGVtZW50XG5cdH1cblx0dmFyIHNoYXBlQ29udGVudHMgPSBTaGFwZUNvbnRlbnRzKGVsZW1lbnQuZGF0YS5zaGFwZXMsIGVsZW1lbnQuaXRlbXNEYXRhLCBzdGF0ZSk7XG5cblx0XG5cblx0ZnVuY3Rpb24gX2J1aWxkUHJvcGVydHlNYXAoKSB7XG5cdFx0c3RhdGUucHJvcGVydGllcy5wdXNoKFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnQ29udGVudHMnLFxuXHRcdFx0XHR2YWx1ZTogc2hhcGVDb250ZW50c1xuXHRcdFx0fVxuXHRcdClcblx0fVxuXG5cdHZhciBtZXRob2RzID0ge1xuXHR9XG5cblx0X2J1aWxkUHJvcGVydHlNYXAoKTtcblxuXHRyZXR1cm4gT2JqZWN0LmFzc2lnbihzdGF0ZSwgTGF5ZXJCYXNlKHN0YXRlKSwgbWV0aG9kcyk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gU2hhcGU7IiwidmFyIEtleVBhdGhOb2RlID0gcmVxdWlyZSgnLi4vLi4va2V5X3BhdGgvS2V5UGF0aE5vZGUnKTtcbnZhciBQcm9wZXJ0eSA9IHJlcXVpcmUoJy4uLy4uL3Byb3BlcnR5L1Byb3BlcnR5Jyk7XG52YXIgU2hhcGVSZWN0YW5nbGUgPSByZXF1aXJlKCcuL1NoYXBlUmVjdGFuZ2xlJyk7XG52YXIgU2hhcGVGaWxsID0gcmVxdWlyZSgnLi9TaGFwZUZpbGwnKTtcbnZhciBTaGFwZVN0cm9rZSA9IHJlcXVpcmUoJy4vU2hhcGVTdHJva2UnKTtcbnZhciBTaGFwZUVsbGlwc2UgPSByZXF1aXJlKCcuL1NoYXBlRWxsaXBzZScpO1xudmFyIFNoYXBlR3JhZGllbnRGaWxsID0gcmVxdWlyZSgnLi9TaGFwZUdyYWRpZW50RmlsbCcpO1xudmFyIFNoYXBlR3JhZGllbnRTdHJva2UgPSByZXF1aXJlKCcuL1NoYXBlR3JhZGllbnRTdHJva2UnKTtcbnZhciBTaGFwZVRyaW1QYXRocyA9IHJlcXVpcmUoJy4vU2hhcGVUcmltUGF0aHMnKTtcbnZhciBTaGFwZVJlcGVhdGVyID0gcmVxdWlyZSgnLi9TaGFwZVJlcGVhdGVyJyk7XG52YXIgU2hhcGVQb2x5c3RhciA9IHJlcXVpcmUoJy4vU2hhcGVQb2x5c3RhcicpO1xudmFyIFNoYXBlUm91bmRDb3JuZXJzID0gcmVxdWlyZSgnLi9TaGFwZVJvdW5kQ29ybmVycycpO1xudmFyIFNoYXBlUGF0aCA9IHJlcXVpcmUoJy4vU2hhcGVQYXRoJyk7XG52YXIgVHJhbnNmb3JtID0gcmVxdWlyZSgnLi4vdHJhbnNmb3JtL1RyYW5zZm9ybScpO1xudmFyIE1hdHJpeCA9IHJlcXVpcmUoJy4uLy4uL2hlbHBlcnMvdHJhbnNmb3JtYXRpb25NYXRyaXgnKTtcblxuZnVuY3Rpb24gU2hhcGVDb250ZW50cyhzaGFwZXNEYXRhLCBzaGFwZXMsIHBhcmVudCkge1xuXHR2YXIgc3RhdGUgPSB7XG5cdFx0cHJvcGVydGllczogX2J1aWxkUHJvcGVydHlNYXAoKSxcblx0XHRwYXJlbnQ6IHBhcmVudFxuXHR9XG5cblx0dmFyIGNhY2hlZFNoYXBlUHJvcGVydGllcyA9IFtdO1xuXG5cdGZ1bmN0aW9uIGJ1aWxkU2hhcGVPYmplY3Qoc2hhcGUsIGluZGV4KSB7XG5cdFx0dmFyIG9iID0ge1xuXHRcdFx0bmFtZTogc2hhcGUubm1cblx0XHR9XG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG9iLCAndmFsdWUnLCB7XG5cdFx0ICAgZ2V0KCkge1xuXHRcdCAgIFx0aWYoY2FjaGVkU2hhcGVQcm9wZXJ0aWVzW2luZGV4XSkge1xuXHRcdCAgIFx0XHRyZXR1cm4gY2FjaGVkU2hhcGVQcm9wZXJ0aWVzW2luZGV4XTtcblx0XHQgICBcdH0gZWxzZSB7XG5cdFx0ICAgXHRcdHZhciBwcm9wZXJ0eTtcblx0XHQgICBcdH1cblx0ICAgXHRcdGlmKHNoYXBlLnR5ID09PSAnZ3InKSB7XG5cdCAgIFx0XHRcdHByb3BlcnR5ID0gU2hhcGVDb250ZW50cyhzaGFwZXNEYXRhW2luZGV4XS5pdCwgc2hhcGVzW2luZGV4XS5pdCwgc3RhdGUpO1xuXHQgICBcdFx0fSBlbHNlIGlmKHNoYXBlLnR5ID09PSAncmMnKSB7XG5cdCAgIFx0XHRcdHByb3BlcnR5ID0gU2hhcGVSZWN0YW5nbGUoc2hhcGVzW2luZGV4XSwgc3RhdGUpO1xuXHQgICBcdFx0fSBlbHNlIGlmKHNoYXBlLnR5ID09PSAnZWwnKSB7XG5cdCAgIFx0XHRcdHByb3BlcnR5ID0gU2hhcGVFbGxpcHNlKHNoYXBlc1tpbmRleF0sIHN0YXRlKTtcblx0ICAgXHRcdH0gZWxzZSBpZihzaGFwZS50eSA9PT0gJ2ZsJykge1xuXHQgICBcdFx0XHRwcm9wZXJ0eSA9IFNoYXBlRmlsbChzaGFwZXNbaW5kZXhdLCBzdGF0ZSk7XG5cdCAgIFx0XHR9IGVsc2UgaWYoc2hhcGUudHkgPT09ICdzdCcpIHtcblx0ICAgXHRcdFx0cHJvcGVydHkgPSBTaGFwZVN0cm9rZShzaGFwZXNbaW5kZXhdLCBzdGF0ZSk7XG5cdCAgIFx0XHR9IGVsc2UgaWYoc2hhcGUudHkgPT09ICdnZicpIHtcblx0ICAgXHRcdFx0cHJvcGVydHkgPSBTaGFwZUdyYWRpZW50RmlsbChzaGFwZXNbaW5kZXhdLCBzdGF0ZSk7XG5cdCAgIFx0XHR9IGVsc2UgaWYoc2hhcGUudHkgPT09ICdncycpIHtcblx0ICAgXHRcdFx0cHJvcGVydHkgPSBTaGFwZUdyYWRpZW50U3Ryb2tlKHNoYXBlc1tpbmRleF0sIHN0YXRlKTtcblx0ICAgXHRcdH0gZWxzZSBpZihzaGFwZS50eSA9PT0gJ3RtJykge1xuXHQgICBcdFx0XHRwcm9wZXJ0eSA9IFNoYXBlVHJpbVBhdGhzKHNoYXBlc1tpbmRleF0sIHN0YXRlKTtcblx0ICAgXHRcdH0gZWxzZSBpZihzaGFwZS50eSA9PT0gJ3JwJykge1xuXHQgICBcdFx0XHRwcm9wZXJ0eSA9IFNoYXBlUmVwZWF0ZXIoc2hhcGVzW2luZGV4XSwgc3RhdGUpO1xuXHQgICBcdFx0fSBlbHNlIGlmKHNoYXBlLnR5ID09PSAnc3InKSB7XG5cdCAgIFx0XHRcdHByb3BlcnR5ID0gU2hhcGVQb2x5c3RhcihzaGFwZXNbaW5kZXhdLCBzdGF0ZSk7XG5cdCAgIFx0XHR9IGVsc2UgaWYoc2hhcGUudHkgPT09ICdyZCcpIHtcblx0ICAgXHRcdFx0cHJvcGVydHkgPSBTaGFwZVJvdW5kQ29ybmVycyhzaGFwZXNbaW5kZXhdLCBzdGF0ZSk7XG5cdCAgIFx0XHR9IGVsc2UgaWYoc2hhcGUudHkgPT09ICdzaCcpIHtcblx0ICAgXHRcdFx0cHJvcGVydHkgPSBTaGFwZVBhdGgoc2hhcGVzW2luZGV4XSwgc3RhdGUpO1xuXHQgICBcdFx0fSBlbHNlIGlmKHNoYXBlLnR5ID09PSAndHInKSB7XG5cdCAgIFx0XHRcdHByb3BlcnR5ID0gVHJhbnNmb3JtKHNoYXBlc1tpbmRleF0udHJhbnNmb3JtLm1Qcm9wcywgc3RhdGUpO1xuXHQgICBcdFx0fSBlbHNlIHtcblx0ICAgXHRcdFx0Y29uc29sZS5sb2coc2hhcGUudHkpO1xuXHQgICBcdFx0fVxuXHQgICBcdFx0Y2FjaGVkU2hhcGVQcm9wZXJ0aWVzW2luZGV4XSA9IHByb3BlcnR5O1xuXHQgICBcdFx0cmV0dXJuIHByb3BlcnR5O1xuXHRcdCAgIH1cblx0XHR9KTtcblx0XHRyZXR1cm4gb2Jcblx0fVxuXG5cdGZ1bmN0aW9uIF9idWlsZFByb3BlcnR5TWFwKCkge1xuXHRcdHJldHVybiBzaGFwZXNEYXRhLm1hcChmdW5jdGlvbihzaGFwZSwgaW5kZXgpIHtcblx0XHRcdHJldHVybiBidWlsZFNoYXBlT2JqZWN0KHNoYXBlLCBpbmRleClcblx0XHR9KTtcblx0fVxuXG5cdGZ1bmN0aW9uIGZyb21LZXlwYXRoTGF5ZXJQb2ludChwb2ludCkge1xuXHRcdGlmKHN0YXRlLmhhc1Byb3BlcnR5KCdUcmFuc2Zvcm0nKSkge1xuICAgIFx0XHR2YXIgdG9Xb3JsZE1hdCA9IE1hdHJpeCgpO1xuICAgICAgICBcdHZhciB0cmFuc2Zvcm1NYXQgPSBzdGF0ZS5nZXRQcm9wZXJ0eSgnVHJhbnNmb3JtJykuZ2V0VGFyZ2V0VHJhbnNmb3JtKCk7XG5cdFx0XHR0cmFuc2Zvcm1NYXQuYXBwbHlUb01hdHJpeCh0b1dvcmxkTWF0KTtcbiAgICAgICAgXHRwb2ludCA9IHRvV29ybGRNYXQuYXBwbHlUb1BvaW50QXJyYXkocG9pbnRbMF0scG9pbnRbMV0scG9pbnRbMl18fDApO1xuXHRcdH1cblx0XHRyZXR1cm4gc3RhdGUucGFyZW50LmZyb21LZXlwYXRoTGF5ZXJQb2ludChwb2ludCk7XG5cdH1cblxuXHRmdW5jdGlvbiB0b0tleXBhdGhMYXllclBvaW50KHBvaW50KSB7XG5cdFx0cG9pbnQgPSBzdGF0ZS5wYXJlbnQudG9LZXlwYXRoTGF5ZXJQb2ludChwb2ludCk7XG5cdFx0aWYoc3RhdGUuaGFzUHJvcGVydHkoJ1RyYW5zZm9ybScpKSB7XG4gICAgXHRcdHZhciB0b1dvcmxkTWF0ID0gTWF0cml4KCk7XG4gICAgICAgIFx0dmFyIHRyYW5zZm9ybU1hdCA9IHN0YXRlLmdldFByb3BlcnR5KCdUcmFuc2Zvcm0nKS5nZXRUYXJnZXRUcmFuc2Zvcm0oKTtcblx0XHRcdHRyYW5zZm9ybU1hdC5hcHBseVRvTWF0cml4KHRvV29ybGRNYXQpO1xuICAgICAgICBcdHBvaW50ID0gdG9Xb3JsZE1hdC5pbnZlcnNlUG9pbnQocG9pbnQpO1xuXHRcdH1cblx0XHRyZXR1cm4gcG9pbnQ7XG5cdH1cblxuXHR2YXIgbWV0aG9kcyA9IHtcblx0XHRmcm9tS2V5cGF0aExheWVyUG9pbnQ6IGZyb21LZXlwYXRoTGF5ZXJQb2ludCxcblx0XHR0b0tleXBhdGhMYXllclBvaW50OiB0b0tleXBhdGhMYXllclBvaW50XG5cdH1cblxuXHQvL3N0YXRlLnByb3BlcnRpZXMgPSBfYnVpbGRQcm9wZXJ0eU1hcCgpO1xuXG5cdHJldHVybiBPYmplY3QuYXNzaWduKHN0YXRlLCBLZXlQYXRoTm9kZShzdGF0ZSksIG1ldGhvZHMpXG59XG5cbm1vZHVsZS5leHBvcnRzID0gU2hhcGVDb250ZW50czsiLCJ2YXIgS2V5UGF0aE5vZGUgPSByZXF1aXJlKCcuLi8uLi9rZXlfcGF0aC9LZXlQYXRoTm9kZScpO1xudmFyIFByb3BlcnR5ID0gcmVxdWlyZSgnLi4vLi4vcHJvcGVydHkvUHJvcGVydHknKTtcblxuZnVuY3Rpb24gU2hhcGVFbGxpcHNlKGVsZW1lbnQsIHBhcmVudCkge1xuXG5cdHZhciBzdGF0ZSA9IHtcblx0XHRwYXJlbnQ6IHBhcmVudCxcblx0XHRwcm9wZXJ0aWVzOiBfYnVpbGRQcm9wZXJ0eU1hcCgpXG5cdH1cblxuXHRmdW5jdGlvbiBfYnVpbGRQcm9wZXJ0eU1hcCgpIHtcblx0XHRyZXR1cm4gW1xuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnU2l6ZScsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShlbGVtZW50LnNoLnMsIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdQb3NpdGlvbicsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShlbGVtZW50LnNoLnAsIHBhcmVudClcblx0XHRcdH1cblx0XHRdXG5cdH1cblxuXHR2YXIgbWV0aG9kcyA9IHtcblx0fVxuXG5cdHJldHVybiBPYmplY3QuYXNzaWduKG1ldGhvZHMsIEtleVBhdGhOb2RlKHN0YXRlKSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gU2hhcGVFbGxpcHNlOyIsInZhciBLZXlQYXRoTm9kZSA9IHJlcXVpcmUoJy4uLy4uL2tleV9wYXRoL0tleVBhdGhOb2RlJyk7XG52YXIgUHJvcGVydHkgPSByZXF1aXJlKCcuLi8uLi9wcm9wZXJ0eS9Qcm9wZXJ0eScpO1xuXG5mdW5jdGlvbiBTaGFwZUZpbGwoZWxlbWVudCwgcGFyZW50KSB7XG5cblx0dmFyIHN0YXRlID0ge1xuXHRcdHBhcmVudDogcGFyZW50LFxuXHRcdHByb3BlcnRpZXM6IF9idWlsZFByb3BlcnR5TWFwKClcblx0fVxuXG5cdGZ1bmN0aW9uIF9idWlsZFByb3BlcnR5TWFwKCkge1xuXHRcdHJldHVybiBbXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdDb2xvcicsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShlbGVtZW50LmMsIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdPcGFjaXR5Jyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KGVsZW1lbnQubywgcGFyZW50KVxuXHRcdFx0fVxuXHRcdF1cblx0fVxuXG5cdHZhciBtZXRob2RzID0ge1xuXHR9XG5cblx0cmV0dXJuIE9iamVjdC5hc3NpZ24obWV0aG9kcywgS2V5UGF0aE5vZGUoc3RhdGUpKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBTaGFwZUZpbGw7IiwidmFyIEtleVBhdGhOb2RlID0gcmVxdWlyZSgnLi4vLi4va2V5X3BhdGgvS2V5UGF0aE5vZGUnKTtcbnZhciBQcm9wZXJ0eSA9IHJlcXVpcmUoJy4uLy4uL3Byb3BlcnR5L1Byb3BlcnR5Jyk7XG5cbmZ1bmN0aW9uIFNoYXBlR3JhZGllbnRGaWxsKGVsZW1lbnQsIHBhcmVudCkge1xuXG5cdHZhciBzdGF0ZSA9IHtcblx0XHRwYXJlbnQ6IHBhcmVudCxcblx0XHRwcm9wZXJ0aWVzOiBfYnVpbGRQcm9wZXJ0eU1hcCgpXG5cdH1cblxuXHRmdW5jdGlvbiBfYnVpbGRQcm9wZXJ0eU1hcCgpIHtcblx0XHRyZXR1cm4gW1xuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnU3RhcnQgUG9pbnQnLFxuXHRcdFx0XHR2YWx1ZTogUHJvcGVydHkoZWxlbWVudC5zLCBwYXJlbnQpXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnRW5kIFBvaW50Jyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KGVsZW1lbnQucywgcGFyZW50KVxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0bmFtZTogJ09wYWNpdHknLFxuXHRcdFx0XHR2YWx1ZTogUHJvcGVydHkoZWxlbWVudC5vLCBwYXJlbnQpXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnSGlnaGxpZ2h0IExlbmd0aCcsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShlbGVtZW50LmgsIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdIaWdobGlnaHQgQW5nbGUnLFxuXHRcdFx0XHR2YWx1ZTogUHJvcGVydHkoZWxlbWVudC5hLCBwYXJlbnQpXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnQ29sb3JzJyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KGVsZW1lbnQuZy5wcm9wLCBwYXJlbnQpXG5cdFx0XHR9XG5cdFx0XVxuXHR9XG5cblx0dmFyIG1ldGhvZHMgPSB7XG5cdH1cblxuXHRyZXR1cm4gT2JqZWN0LmFzc2lnbihtZXRob2RzLCBLZXlQYXRoTm9kZShzdGF0ZSkpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IFNoYXBlR3JhZGllbnRGaWxsOyIsInZhciBLZXlQYXRoTm9kZSA9IHJlcXVpcmUoJy4uLy4uL2tleV9wYXRoL0tleVBhdGhOb2RlJyk7XG52YXIgUHJvcGVydHkgPSByZXF1aXJlKCcuLi8uLi9wcm9wZXJ0eS9Qcm9wZXJ0eScpO1xuXG5mdW5jdGlvbiBTaGFwZUdyYWRpZW50U3Ryb2tlKGVsZW1lbnQsIHBhcmVudCkge1xuXG5cdHZhciBzdGF0ZSA9IHtcblx0XHRwYXJlbnQ6IHBhcmVudCxcblx0XHRwcm9wZXJ0aWVzOiBfYnVpbGRQcm9wZXJ0eU1hcCgpXG5cdH1cblxuXHRmdW5jdGlvbiBfYnVpbGRQcm9wZXJ0eU1hcCgpIHtcblx0XHRyZXR1cm4gW1xuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnU3RhcnQgUG9pbnQnLFxuXHRcdFx0XHR2YWx1ZTogUHJvcGVydHkoZWxlbWVudC5zLCBwYXJlbnQpXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnRW5kIFBvaW50Jyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KGVsZW1lbnQuZSwgcGFyZW50KVxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0bmFtZTogJ09wYWNpdHknLFxuXHRcdFx0XHR2YWx1ZTogUHJvcGVydHkoZWxlbWVudC5vLCBwYXJlbnQpXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnSGlnaGxpZ2h0IExlbmd0aCcsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShlbGVtZW50LmgsIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdIaWdobGlnaHQgQW5nbGUnLFxuXHRcdFx0XHR2YWx1ZTogUHJvcGVydHkoZWxlbWVudC5hLCBwYXJlbnQpXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnQ29sb3JzJyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KGVsZW1lbnQuZy5wcm9wLCBwYXJlbnQpXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnU3Ryb2tlIFdpZHRoJyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KGVsZW1lbnQudywgcGFyZW50KVxuXHRcdFx0fVxuXHRcdF1cblx0fVxuXG5cdHZhciBtZXRob2RzID0ge1xuXHR9XG5cblx0cmV0dXJuIE9iamVjdC5hc3NpZ24obWV0aG9kcywgS2V5UGF0aE5vZGUoc3RhdGUpKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBTaGFwZUdyYWRpZW50U3Ryb2tlOyIsInZhciBLZXlQYXRoTm9kZSA9IHJlcXVpcmUoJy4uLy4uL2tleV9wYXRoL0tleVBhdGhOb2RlJyk7XG52YXIgUHJvcGVydHkgPSByZXF1aXJlKCcuLi8uLi9wcm9wZXJ0eS9Qcm9wZXJ0eScpO1xuXG5mdW5jdGlvbiBTaGFwZVBhdGgoZWxlbWVudCwgcGFyZW50KSB7XG5cblx0dmFyIHN0YXRlID0ge1xuXHRcdHBhcmVudDogcGFyZW50LFxuXHRcdHByb3BlcnRpZXM6IF9idWlsZFByb3BlcnR5TWFwKClcblx0fVxuXG5cdGZ1bmN0aW9uIHNldFBhdGgodmFsdWUpIHtcblx0XHRQcm9wZXJ0eShlbGVtZW50LnNoKS5zZXRWYWx1ZSh2YWx1ZSk7XG5cdH1cblxuXHRmdW5jdGlvbiBfYnVpbGRQcm9wZXJ0eU1hcCgpIHtcblx0XHRyZXR1cm4gW1xuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAncGF0aCcsXG5cdFx0XHRcdHZhbHVlOlByb3BlcnR5KGVsZW1lbnQuc2gsIHBhcmVudClcblx0XHRcdH1cblx0XHRdXG5cdH1cblxuXHR2YXIgbWV0aG9kcyA9IHtcblx0fVxuXG5cdHJldHVybiBPYmplY3QuYXNzaWduKG1ldGhvZHMsIEtleVBhdGhOb2RlKHN0YXRlKSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gU2hhcGVQYXRoOyIsInZhciBLZXlQYXRoTm9kZSA9IHJlcXVpcmUoJy4uLy4uL2tleV9wYXRoL0tleVBhdGhOb2RlJyk7XG52YXIgUHJvcGVydHkgPSByZXF1aXJlKCcuLi8uLi9wcm9wZXJ0eS9Qcm9wZXJ0eScpO1xuXG5mdW5jdGlvbiBTaGFwZVBvbHlzdGFyKGVsZW1lbnQsIHBhcmVudCkge1xuXG5cdHZhciBzdGF0ZSA9IHtcblx0XHRwYXJlbnQ6IHBhcmVudCxcblx0XHRwcm9wZXJ0aWVzOiBfYnVpbGRQcm9wZXJ0eU1hcCgpXG5cdH1cblxuXHRmdW5jdGlvbiBfYnVpbGRQcm9wZXJ0eU1hcCgpIHtcblx0XHRyZXR1cm4gW1xuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnUG9pbnRzJyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KGVsZW1lbnQuc2gucHQsIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdQb3NpdGlvbicsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShlbGVtZW50LnNoLnAsIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdSb3RhdGlvbicsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShlbGVtZW50LnNoLnIsIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdJbm5lciBSYWRpdXMnLFxuXHRcdFx0XHR2YWx1ZTogUHJvcGVydHkoZWxlbWVudC5zaC5pciwgcGFyZW50KVxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0bmFtZTogJ091dGVyIFJhZGl1cycsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShlbGVtZW50LnNoLm9yLCBwYXJlbnQpXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnSW5uZXIgUm91bmRuZXNzJyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KGVsZW1lbnQuc2guaXMsIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdPdXRlciBSb3VuZG5lc3MnLFxuXHRcdFx0XHR2YWx1ZTogUHJvcGVydHkoZWxlbWVudC5zaC5vcywgcGFyZW50KVxuXHRcdFx0fVxuXHRcdF1cblx0fVxuXG5cdHZhciBtZXRob2RzID0ge1xuXHR9XG5cblx0cmV0dXJuIE9iamVjdC5hc3NpZ24obWV0aG9kcywgS2V5UGF0aE5vZGUoc3RhdGUpKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBTaGFwZVBvbHlzdGFyOyIsInZhciBLZXlQYXRoTm9kZSA9IHJlcXVpcmUoJy4uLy4uL2tleV9wYXRoL0tleVBhdGhOb2RlJyk7XG52YXIgUHJvcGVydHkgPSByZXF1aXJlKCcuLi8uLi9wcm9wZXJ0eS9Qcm9wZXJ0eScpO1xuXG5mdW5jdGlvbiBTaGFwZVJlY3RhbmdsZShlbGVtZW50LCBwYXJlbnQpIHtcblxuXHR2YXIgc3RhdGUgPSB7XG5cdFx0cGFyZW50OiBwYXJlbnQsXG5cdFx0cHJvcGVydGllczogX2J1aWxkUHJvcGVydHlNYXAoKVxuXHR9XG5cblx0ZnVuY3Rpb24gX2J1aWxkUHJvcGVydHlNYXAoKSB7XG5cdFx0cmV0dXJuIFtcblx0XHRcdHtcblx0XHRcdFx0bmFtZTogJ1NpemUnLFxuXHRcdFx0XHR2YWx1ZTogUHJvcGVydHkoZWxlbWVudC5zaC5zLCBwYXJlbnQpXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnUG9zaXRpb24nLFxuXHRcdFx0XHR2YWx1ZTogUHJvcGVydHkoZWxlbWVudC5zaC5wLCBwYXJlbnQpXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnUm91bmRuZXNzJyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KGVsZW1lbnQuc2guciwgcGFyZW50KVxuXHRcdFx0fVxuXHRcdF1cblx0fVxuXG5cdHZhciBtZXRob2RzID0ge1xuXHR9XG5cblx0cmV0dXJuIE9iamVjdC5hc3NpZ24obWV0aG9kcywgS2V5UGF0aE5vZGUoc3RhdGUpKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBTaGFwZVJlY3RhbmdsZTsiLCJ2YXIgS2V5UGF0aE5vZGUgPSByZXF1aXJlKCcuLi8uLi9rZXlfcGF0aC9LZXlQYXRoTm9kZScpO1xudmFyIFByb3BlcnR5ID0gcmVxdWlyZSgnLi4vLi4vcHJvcGVydHkvUHJvcGVydHknKTtcbnZhciBUcmFuc2Zvcm0gPSByZXF1aXJlKCcuLi90cmFuc2Zvcm0vVHJhbnNmb3JtJyk7XG5cbmZ1bmN0aW9uIFNoYXBlUmVwZWF0ZXIoZWxlbWVudCwgcGFyZW50KSB7XG5cblx0dmFyIHN0YXRlID0ge1xuXHRcdHBhcmVudDogcGFyZW50LFxuXHRcdHByb3BlcnRpZXM6IF9idWlsZFByb3BlcnR5TWFwKClcblx0fVxuXG5cdGZ1bmN0aW9uIF9idWlsZFByb3BlcnR5TWFwKCkge1xuXHRcdHJldHVybiBbXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdDb3BpZXMnLFxuXHRcdFx0XHR2YWx1ZTogUHJvcGVydHkoZWxlbWVudC5jLCBwYXJlbnQpXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnT2Zmc2V0Jyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KGVsZW1lbnQubywgcGFyZW50KVxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0bmFtZTogJ1RyYW5zZm9ybScsXG5cdFx0XHRcdHZhbHVlOiBUcmFuc2Zvcm0oZWxlbWVudC50ciwgcGFyZW50KVxuXHRcdFx0fVxuXHRcdF1cblx0fVxuXG5cdHZhciBtZXRob2RzID0ge1xuXHR9XG5cblx0cmV0dXJuIE9iamVjdC5hc3NpZ24obWV0aG9kcywgS2V5UGF0aE5vZGUoc3RhdGUpKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBTaGFwZVJlcGVhdGVyOyIsInZhciBLZXlQYXRoTm9kZSA9IHJlcXVpcmUoJy4uLy4uL2tleV9wYXRoL0tleVBhdGhOb2RlJyk7XG52YXIgUHJvcGVydHkgPSByZXF1aXJlKCcuLi8uLi9wcm9wZXJ0eS9Qcm9wZXJ0eScpO1xuXG5mdW5jdGlvbiBTaGFwZVJvdW5kQ29ybmVycyhlbGVtZW50LCBwYXJlbnQpIHtcblxuXHR2YXIgc3RhdGUgPSB7XG5cdFx0cGFyZW50OiBwYXJlbnQsXG5cdFx0cHJvcGVydGllczogX2J1aWxkUHJvcGVydHlNYXAoKVxuXHR9XG5cblx0ZnVuY3Rpb24gX2J1aWxkUHJvcGVydHlNYXAoKSB7XG5cdFx0cmV0dXJuIFtcblx0XHRcdHtcblx0XHRcdFx0bmFtZTogJ1JhZGl1cycsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShlbGVtZW50LnJkLCBwYXJlbnQpXG5cdFx0XHR9XG5cdFx0XVxuXHR9XG5cblx0dmFyIG1ldGhvZHMgPSB7XG5cdH1cblxuXHRyZXR1cm4gT2JqZWN0LmFzc2lnbihtZXRob2RzLCBLZXlQYXRoTm9kZShzdGF0ZSkpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IFNoYXBlUm91bmRDb3JuZXJzOyIsInZhciBLZXlQYXRoTm9kZSA9IHJlcXVpcmUoJy4uLy4uL2tleV9wYXRoL0tleVBhdGhOb2RlJyk7XG52YXIgUHJvcGVydHkgPSByZXF1aXJlKCcuLi8uLi9wcm9wZXJ0eS9Qcm9wZXJ0eScpO1xuXG5mdW5jdGlvbiBTaGFwZVN0cm9rZShlbGVtZW50LCBwYXJlbnQpIHtcblx0dmFyIHN0YXRlID0ge1xuXHRcdHBhcmVudDogcGFyZW50LFxuXHRcdHByb3BlcnRpZXM6IF9idWlsZFByb3BlcnR5TWFwKClcblx0fVxuXG5cdGZ1bmN0aW9uIF9idWlsZFByb3BlcnR5TWFwKCkge1xuXHRcdHJldHVybiBbXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdDb2xvcicsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShlbGVtZW50LmMsIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdTdHJva2UgV2lkdGgnLFxuXHRcdFx0XHR2YWx1ZTogUHJvcGVydHkoZWxlbWVudC53LCBwYXJlbnQpXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnT3BhY2l0eScsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShlbGVtZW50Lm8sIHBhcmVudClcblx0XHRcdH1cblx0XHRdXG5cdH1cblxuXHR2YXIgbWV0aG9kcyA9IHtcblx0fVxuXG5cdHJldHVybiBPYmplY3QuYXNzaWduKG1ldGhvZHMsIEtleVBhdGhOb2RlKHN0YXRlKSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gU2hhcGVTdHJva2UiLCJ2YXIgS2V5UGF0aE5vZGUgPSByZXF1aXJlKCcuLi8uLi9rZXlfcGF0aC9LZXlQYXRoTm9kZScpO1xudmFyIFByb3BlcnR5ID0gcmVxdWlyZSgnLi4vLi4vcHJvcGVydHkvUHJvcGVydHknKTtcblxuZnVuY3Rpb24gU2hhcGVUcmltUGF0aHMoZWxlbWVudCwgcGFyZW50KSB7XG5cblx0dmFyIHN0YXRlID0ge1xuXHRcdHBhcmVudDogcGFyZW50LFxuXHRcdHByb3BlcnRpZXM6IF9idWlsZFByb3BlcnR5TWFwKClcblx0fVxuXG5cdGZ1bmN0aW9uIF9idWlsZFByb3BlcnR5TWFwKCkge1xuXHRcdHJldHVybiBbXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdTdGFydCcsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShlbGVtZW50LnMsIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdFbmQnLFxuXHRcdFx0XHR2YWx1ZTogUHJvcGVydHkoZWxlbWVudC5lLCBwYXJlbnQpXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnT2Zmc2V0Jyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KGVsZW1lbnQubywgcGFyZW50KVxuXHRcdFx0fVxuXHRcdF1cblx0fVxuXG5cdHZhciBtZXRob2RzID0ge1xuXHR9XG5cblx0cmV0dXJuIE9iamVjdC5hc3NpZ24obWV0aG9kcywgS2V5UGF0aE5vZGUoc3RhdGUpKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBTaGFwZVRyaW1QYXRoczsiLCJ2YXIgTGF5ZXJCYXNlID0gcmVxdWlyZSgnLi4vTGF5ZXJCYXNlJyk7XG5cbmZ1bmN0aW9uIFNvbGlkKGVsZW1lbnQsIHBhcmVudCkge1xuXG5cdHZhciBzdGF0ZSA9IHtcblx0XHRlbGVtZW50OiBlbGVtZW50LFxuXHRcdHBhcmVudDogcGFyZW50LFxuXHRcdHByb3BlcnRpZXM6IF9idWlsZFByb3BlcnR5TWFwKClcblx0fVxuXG5cdGZ1bmN0aW9uIF9idWlsZFByb3BlcnR5TWFwKCkge1xuXHRcdHJldHVybiBbXG5cdFx0XVxuXHR9XG5cblx0dmFyIG1ldGhvZHMgPSB7XG5cdH1cblxuXHRyZXR1cm4gT2JqZWN0LmFzc2lnbih7fSwgTGF5ZXJCYXNlKHN0YXRlKSwgbWV0aG9kcyk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gU29saWQ7IiwidmFyIEtleVBhdGhOb2RlID0gcmVxdWlyZSgnLi4vLi4va2V5X3BhdGgvS2V5UGF0aE5vZGUnKTtcbnZhciBQcm9wZXJ0eSA9IHJlcXVpcmUoJy4uLy4uL3Byb3BlcnR5L1Byb3BlcnR5Jyk7XG52YXIgVGV4dEFuaW1hdG9yID0gcmVxdWlyZSgnLi9UZXh0QW5pbWF0b3InKTtcblxuZnVuY3Rpb24gVGV4dChlbGVtZW50LCBwYXJlbnQpIHtcblxuXHR2YXIgaW5zdGFuY2UgPSB7fVxuXG5cdHZhciBzdGF0ZSA9IHtcblx0XHRlbGVtZW50OiBlbGVtZW50LFxuXHRcdHBhcmVudDogcGFyZW50LFxuXHRcdHByb3BlcnRpZXM6IF9idWlsZFByb3BlcnR5TWFwKClcblx0fVxuXG5cdGZ1bmN0aW9uIHNldERvY3VtZW50RGF0YShfZnVuY3Rpb24pIHtcblx0XHR2YXIgcHJldmlvdXNWYWx1ZTtcblx0XHRzZXRJbnRlcnZhbChmdW5jdGlvbigpIHtcblx0XHRcdHZhciBuZXdWYWx1ZSA9IF9mdW5jdGlvbihlbGVtZW50LnRleHRQcm9wZXJ0eS5jdXJyZW50RGF0YSk7XG5cdFx0XHRpZiAocHJldmlvdXNWYWx1ZSAhPT0gbmV3VmFsdWUpIHtcblx0XHRcdFx0ZWxlbWVudC51cGRhdGVEb2N1bWVudERhdGEobmV3VmFsdWUpXG5cdFx0XHR9XG5cdFx0fSwgNTAwKVxuXHRcdGNvbnNvbGUubG9nKGVsZW1lbnQpXG5cdH1cblxuXHRmdW5jdGlvbiBhZGRBbmltYXRvcnMoKSB7XG5cdFx0dmFyIGFuaW1hdG9yUHJvcGVydGllcyA9IFtdO1xuXHRcdHZhciBhbmltYXRvcnMgPSBlbGVtZW50LnRleHRBbmltYXRvci5fYW5pbWF0b3JzRGF0YTtcblx0XHR2YXIgaSwgbGVuID0gYW5pbWF0b3JzLmxlbmd0aDtcblx0XHR2YXIgdGV4dEFuaW1hdG9yO1xuXHRcdGZvciAoaSA9IDA7IGkgPCBsZW47IGkgKz0gMSkge1xuXHRcdFx0dGV4dEFuaW1hdG9yID0gVGV4dEFuaW1hdG9yKGFuaW1hdG9yc1tpXSlcblx0XHRcdGFuaW1hdG9yUHJvcGVydGllcy5wdXNoKHtcblx0XHRcdFx0bmFtZTogZWxlbWVudC50ZXh0QW5pbWF0b3IuX3RleHREYXRhLmFbaV0ubm0gfHwgJ0FuaW1hdG9yICcgKyAoaSsxKSwgLy9GYWxsYmFjayBmb3Igb2xkIGFuaW1hdGlvbnNcblx0XHRcdFx0dmFsdWU6IHRleHRBbmltYXRvclxuXHRcdFx0fSlcblx0XHR9XG5cdFx0cmV0dXJuIGFuaW1hdG9yUHJvcGVydGllcztcblx0fVxuXG5cdGZ1bmN0aW9uIF9idWlsZFByb3BlcnR5TWFwKCkge1xuXHRcdHJldHVybiBbXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6J1NvdXJjZScsXG5cdFx0XHRcdHZhbHVlOiB7XG5cdFx0XHRcdFx0c2V0VmFsdWU6IHNldERvY3VtZW50RGF0YVxuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0XS5jb25jYXQoYWRkQW5pbWF0b3JzKCkpXG5cdH1cblxuXHR2YXIgbWV0aG9kcyA9IHtcblx0fVxuXG5cdHJldHVybiBPYmplY3QuYXNzaWduKGluc3RhbmNlLCBtZXRob2RzLCBLZXlQYXRoTm9kZShzdGF0ZSkpO1xuXG59XG5cbm1vZHVsZS5leHBvcnRzID0gVGV4dDsiLCJ2YXIgS2V5UGF0aE5vZGUgPSByZXF1aXJlKCcuLi8uLi9rZXlfcGF0aC9LZXlQYXRoTm9kZScpO1xudmFyIFByb3BlcnR5ID0gcmVxdWlyZSgnLi4vLi4vcHJvcGVydHkvUHJvcGVydHknKTtcblxuZnVuY3Rpb24gVGV4dEFuaW1hdG9yKGFuaW1hdG9yKSB7XG5cblx0dmFyIGluc3RhbmNlID0ge31cblxuXHR2YXIgc3RhdGUgPSB7XG5cdFx0cHJvcGVydGllczogX2J1aWxkUHJvcGVydHlNYXAoKVxuXHR9XG5cblx0ZnVuY3Rpb24gc2V0QW5jaG9yUG9pbnQodmFsdWUpIHtcblx0XHRQcm9wZXJ0eShhbmltYXRvci5hLmEpLnNldFZhbHVlKHZhbHVlKTtcblx0fVxuXG5cdGZ1bmN0aW9uIHNldEZpbGxCcmlnaHRuZXNzKHZhbHVlKSB7XG5cdFx0UHJvcGVydHkoYW5pbWF0b3IuYS5mYikuc2V0VmFsdWUodmFsdWUpO1xuXHR9XG5cblx0ZnVuY3Rpb24gc2V0RmlsbENvbG9yKHZhbHVlKSB7XG5cdFx0UHJvcGVydHkoYW5pbWF0b3IuYS5mYykuc2V0VmFsdWUodmFsdWUpO1xuXHR9XG5cblx0ZnVuY3Rpb24gc2V0RmlsbEh1ZSh2YWx1ZSkge1xuXHRcdFByb3BlcnR5KGFuaW1hdG9yLmEuZmgpLnNldFZhbHVlKHZhbHVlKTtcblx0fVxuXG5cdGZ1bmN0aW9uIHNldEZpbGxTYXR1cmF0aW9uKHZhbHVlKSB7XG5cdFx0UHJvcGVydHkoYW5pbWF0b3IuYS5mcykuc2V0VmFsdWUodmFsdWUpO1xuXHR9XG5cblx0ZnVuY3Rpb24gc2V0RmlsbE9wYWNpdHkodmFsdWUpIHtcblx0XHRQcm9wZXJ0eShhbmltYXRvci5hLmZvKS5zZXRWYWx1ZSh2YWx1ZSk7XG5cdH1cblxuXHRmdW5jdGlvbiBzZXRPcGFjaXR5KHZhbHVlKSB7XG5cdFx0UHJvcGVydHkoYW5pbWF0b3IuYS5vKS5zZXRWYWx1ZSh2YWx1ZSk7XG5cdH1cblxuXHRmdW5jdGlvbiBzZXRQb3NpdGlvbih2YWx1ZSkge1xuXHRcdFByb3BlcnR5KGFuaW1hdG9yLmEucCkuc2V0VmFsdWUodmFsdWUpO1xuXHR9XG5cblx0ZnVuY3Rpb24gc2V0Um90YXRpb24odmFsdWUpIHtcblx0XHRQcm9wZXJ0eShhbmltYXRvci5hLnIpLnNldFZhbHVlKHZhbHVlKTtcblx0fVxuXG5cdGZ1bmN0aW9uIHNldFJvdGF0aW9uWCh2YWx1ZSkge1xuXHRcdFByb3BlcnR5KGFuaW1hdG9yLmEucngpLnNldFZhbHVlKHZhbHVlKTtcblx0fVxuXG5cdGZ1bmN0aW9uIHNldFJvdGF0aW9uWSh2YWx1ZSkge1xuXHRcdFByb3BlcnR5KGFuaW1hdG9yLmEucnkpLnNldFZhbHVlKHZhbHVlKTtcblx0fVxuXG5cdGZ1bmN0aW9uIHNldFNjYWxlKHZhbHVlKSB7XG5cdFx0UHJvcGVydHkoYW5pbWF0b3IuYS5zKS5zZXRWYWx1ZSh2YWx1ZSk7XG5cdH1cblxuXHRmdW5jdGlvbiBzZXRTa2V3QXhpcyh2YWx1ZSkge1xuXHRcdFByb3BlcnR5KGFuaW1hdG9yLmEuc2EpLnNldFZhbHVlKHZhbHVlKTtcblx0fVxuXG5cdGZ1bmN0aW9uIHNldFN0cm9rZUNvbG9yKHZhbHVlKSB7XG5cdFx0UHJvcGVydHkoYW5pbWF0b3IuYS5zYykuc2V0VmFsdWUodmFsdWUpO1xuXHR9XG5cblx0ZnVuY3Rpb24gc2V0U2tldyh2YWx1ZSkge1xuXHRcdFByb3BlcnR5KGFuaW1hdG9yLmEuc2spLnNldFZhbHVlKHZhbHVlKTtcblx0fVxuXG5cdGZ1bmN0aW9uIHNldFN0cm9rZU9wYWNpdHkodmFsdWUpIHtcblx0XHRQcm9wZXJ0eShhbmltYXRvci5hLnNvKS5zZXRWYWx1ZSh2YWx1ZSk7XG5cdH1cblxuXHRmdW5jdGlvbiBzZXRTdHJva2VXaWR0aCh2YWx1ZSkge1xuXHRcdFByb3BlcnR5KGFuaW1hdG9yLmEuc3cpLnNldFZhbHVlKHZhbHVlKTtcblx0fVxuXG5cdGZ1bmN0aW9uIHNldFN0cm9rZUJyaWdodG5lc3ModmFsdWUpIHtcblx0XHRQcm9wZXJ0eShhbmltYXRvci5hLnNiKS5zZXRWYWx1ZSh2YWx1ZSk7XG5cdH1cblxuXHRmdW5jdGlvbiBzZXRTdHJva2VIdWUodmFsdWUpIHtcblx0XHRQcm9wZXJ0eShhbmltYXRvci5hLnNoKS5zZXRWYWx1ZSh2YWx1ZSk7XG5cdH1cblxuXHRmdW5jdGlvbiBzZXRTdHJva2VTYXR1cmF0aW9uKHZhbHVlKSB7XG5cdFx0UHJvcGVydHkoYW5pbWF0b3IuYS5zcykuc2V0VmFsdWUodmFsdWUpO1xuXHR9XG5cblx0ZnVuY3Rpb24gc2V0VHJhY2tpbmdBbW91bnQodmFsdWUpIHtcblx0XHRQcm9wZXJ0eShhbmltYXRvci5hLnQpLnNldFZhbHVlKHZhbHVlKTtcblx0fVxuXG5cdGZ1bmN0aW9uIF9idWlsZFByb3BlcnR5TWFwKCkge1xuXHRcdHJldHVybiBbXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6J0FuY2hvciBQb2ludCcsXG5cdFx0XHRcdHZhbHVlOiB7XG5cdFx0XHRcdFx0c2V0VmFsdWU6IHNldEFuY2hvclBvaW50XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6J0ZpbGwgQnJpZ2h0bmVzcycsXG5cdFx0XHRcdHZhbHVlOiB7XG5cdFx0XHRcdFx0c2V0VmFsdWU6IHNldEZpbGxCcmlnaHRuZXNzXG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6J0ZpbGwgQ29sb3InLFxuXHRcdFx0XHR2YWx1ZToge1xuXHRcdFx0XHRcdHNldFZhbHVlOiBzZXRGaWxsQ29sb3Jcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0bmFtZTonRmlsbCBIdWUnLFxuXHRcdFx0XHR2YWx1ZToge1xuXHRcdFx0XHRcdHNldFZhbHVlOiBzZXRGaWxsSHVlXG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6J0ZpbGwgU2F0dXJhdGlvbicsXG5cdFx0XHRcdHZhbHVlOiB7XG5cdFx0XHRcdFx0c2V0VmFsdWU6IHNldEZpbGxTYXR1cmF0aW9uXG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6J0ZpbGwgT3BhY2l0eScsXG5cdFx0XHRcdHZhbHVlOiB7XG5cdFx0XHRcdFx0c2V0VmFsdWU6IHNldEZpbGxPcGFjaXR5XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6J09wYWNpdHknLFxuXHRcdFx0XHR2YWx1ZToge1xuXHRcdFx0XHRcdHNldFZhbHVlOiBzZXRPcGFjaXR5XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6J1Bvc2l0aW9uJyxcblx0XHRcdFx0dmFsdWU6IHtcblx0XHRcdFx0XHRzZXRWYWx1ZTogc2V0UG9zaXRpb25cblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0bmFtZTonUm90YXRpb24gWCcsXG5cdFx0XHRcdHZhbHVlOiB7XG5cdFx0XHRcdFx0c2V0VmFsdWU6IHNldFJvdGF0aW9uWFxuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOidSb3RhdGlvbiBZJyxcblx0XHRcdFx0dmFsdWU6IHtcblx0XHRcdFx0XHRzZXRWYWx1ZTogc2V0Um90YXRpb25ZXG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6J1NjYWxlJyxcblx0XHRcdFx0dmFsdWU6IHtcblx0XHRcdFx0XHRzZXRWYWx1ZTogc2V0U2NhbGVcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0bmFtZTonU2tldyBBeGlzJyxcblx0XHRcdFx0dmFsdWU6IHtcblx0XHRcdFx0XHRzZXRWYWx1ZTogc2V0U2tld0F4aXNcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0bmFtZTonU3Ryb2tlIENvbG9yJyxcblx0XHRcdFx0dmFsdWU6IHtcblx0XHRcdFx0XHRzZXRWYWx1ZTogc2V0U3Ryb2tlQ29sb3Jcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0bmFtZTonU2tldycsXG5cdFx0XHRcdHZhbHVlOiB7XG5cdFx0XHRcdFx0c2V0VmFsdWU6IHNldFNrZXdcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0bmFtZTonU3Ryb2tlIFdpZHRoJyxcblx0XHRcdFx0dmFsdWU6IHtcblx0XHRcdFx0XHRzZXRWYWx1ZTogc2V0U3Ryb2tlV2lkdGhcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0bmFtZTonVHJhY2tpbmcgQW1vdW50Jyxcblx0XHRcdFx0dmFsdWU6IHtcblx0XHRcdFx0XHRzZXRWYWx1ZTogc2V0VHJhY2tpbmdBbW91bnRcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0bmFtZTonU3Ryb2tlIE9wYWNpdHknLFxuXHRcdFx0XHR2YWx1ZToge1xuXHRcdFx0XHRcdHNldFZhbHVlOiBzZXRTdHJva2VPcGFjaXR5XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6J1N0cm9rZSBCcmlnaHRuZXNzJyxcblx0XHRcdFx0dmFsdWU6IHtcblx0XHRcdFx0XHRzZXRWYWx1ZTogc2V0U3Ryb2tlQnJpZ2h0bmVzc1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOidTdHJva2UgU2F0dXJhdGlvbicsXG5cdFx0XHRcdHZhbHVlOiB7XG5cdFx0XHRcdFx0c2V0VmFsdWU6IHNldFN0cm9rZVNhdHVyYXRpb25cblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0bmFtZTonU3Ryb2tlIEh1ZScsXG5cdFx0XHRcdHZhbHVlOiB7XG5cdFx0XHRcdFx0c2V0VmFsdWU6IHNldFN0cm9rZUh1ZVxuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0XG5cdFx0XVxuXHR9XG5cblx0dmFyIG1ldGhvZHMgPSB7XG5cdH1cblxuXHRyZXR1cm4gT2JqZWN0LmFzc2lnbihpbnN0YW5jZSwgbWV0aG9kcywgS2V5UGF0aE5vZGUoc3RhdGUpKTtcblxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IFRleHRBbmltYXRvcjsiLCJ2YXIgTGF5ZXJCYXNlID0gcmVxdWlyZSgnLi4vTGF5ZXJCYXNlJyk7XG52YXIgVGV4dCA9IHJlcXVpcmUoJy4vVGV4dCcpO1xuXG5mdW5jdGlvbiBUZXh0RWxlbWVudChlbGVtZW50KSB7XG5cblx0dmFyIGluc3RhbmNlID0ge307XG5cblx0dmFyIFRleHRQcm9wZXJ0eSA9IFRleHQoZWxlbWVudCk7XG5cdHZhciBzdGF0ZSA9IHtcblx0XHRlbGVtZW50OiBlbGVtZW50LFxuXHRcdHByb3BlcnRpZXM6IF9idWlsZFByb3BlcnR5TWFwKClcblx0fVxuXG5cdGZ1bmN0aW9uIF9idWlsZFByb3BlcnR5TWFwKCkge1xuXHRcdHJldHVybiBbXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICd0ZXh0Jyxcblx0XHRcdFx0dmFsdWU6IFRleHRQcm9wZXJ0eVxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0bmFtZTogJ1RleHQnLFxuXHRcdFx0XHR2YWx1ZTogVGV4dFByb3BlcnR5XG5cdFx0XHR9XG5cdFx0XVxuXHR9XG5cblx0ZnVuY3Rpb24gZ2V0VGV4dCgpIHtcblx0XHRyZXR1cm4gZWxlbWVudC50ZXh0UHJvcGVydHkuY3VycmVudERhdGEudDtcblx0fVxuXG5cdGZ1bmN0aW9uIHNldFRleHQodmFsdWUsIGluZGV4KSB7XG5cdFx0c2V0RG9jdW1lbnREYXRhKHt0OiB2YWx1ZX0sIGluZGV4KTtcblx0fVxuXG5cdGZ1bmN0aW9uIHNldERvY3VtZW50RGF0YShkYXRhLCBpbmRleCkge1xuXHRcdHJldHVybiBlbGVtZW50LnVwZGF0ZURvY3VtZW50RGF0YShkYXRhLCBpbmRleCk7XG5cdH1cblx0XG5cdGZ1bmN0aW9uIGNhblJlc2l6ZUZvbnQoX2NhblJlc2l6ZSkge1xuXHRcdHJldHVybiBlbGVtZW50LmNhblJlc2l6ZUZvbnQoX2NhblJlc2l6ZSk7XG5cdH1cblxuXHRmdW5jdGlvbiBzZXRNaW5pbXVtRm9udFNpemUoX2ZvbnRTaXplKSB7XG5cdFx0cmV0dXJuIGVsZW1lbnQuc2V0TWluaW11bUZvbnRTaXplKF9mb250U2l6ZSk7XG5cdH1cblxuXHR2YXIgbWV0aG9kcyA9IHtcblx0XHRnZXRUZXh0OiBnZXRUZXh0LFxuXHRcdHNldFRleHQ6IHNldFRleHQsXG5cdFx0Y2FuUmVzaXplRm9udDogY2FuUmVzaXplRm9udCxcblx0XHRzZXREb2N1bWVudERhdGE6IHNldERvY3VtZW50RGF0YSxcblx0XHRzZXRNaW5pbXVtRm9udFNpemU6IHNldE1pbmltdW1Gb250U2l6ZVxuXHR9XG5cblx0cmV0dXJuIE9iamVjdC5hc3NpZ24oaW5zdGFuY2UsIExheWVyQmFzZShzdGF0ZSksIG1ldGhvZHMpO1xuXG59XG5cbm1vZHVsZS5leHBvcnRzID0gVGV4dEVsZW1lbnQ7IiwidmFyIEtleVBhdGhOb2RlID0gcmVxdWlyZSgnLi4vLi4va2V5X3BhdGgvS2V5UGF0aE5vZGUnKTtcbnZhciBQcm9wZXJ0eSA9IHJlcXVpcmUoJy4uLy4uL3Byb3BlcnR5L1Byb3BlcnR5Jyk7XG5cbmZ1bmN0aW9uIFRyYW5zZm9ybShwcm9wcywgcGFyZW50KSB7XG5cdHZhciBzdGF0ZSA9IHtcblx0XHRwcm9wZXJ0aWVzOiBfYnVpbGRQcm9wZXJ0eU1hcCgpXG5cdH1cblxuXHRmdW5jdGlvbiBfYnVpbGRQcm9wZXJ0eU1hcCgpIHtcblx0XHRyZXR1cm4gW1xuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnQW5jaG9yIFBvaW50Jyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KHByb3BzLmEsIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdQb2ludCBvZiBJbnRlcmVzdCcsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShwcm9wcy5hLCBwYXJlbnQpXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnUG9zaXRpb24nLFxuXHRcdFx0XHR2YWx1ZTogUHJvcGVydHkocHJvcHMucCwgcGFyZW50KVxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0bmFtZTogJ1NjYWxlJyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KHByb3BzLnMsIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdSb3RhdGlvbicsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShwcm9wcy5yLCBwYXJlbnQpXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnWCBQb3NpdGlvbicsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShwcm9wcy5weCwgcGFyZW50KVxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0bmFtZTogJ1kgUG9zaXRpb24nLFxuXHRcdFx0XHR2YWx1ZTogUHJvcGVydHkocHJvcHMucHksIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdaIFBvc2l0aW9uJyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KHByb3BzLnB6LCBwYXJlbnQpXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnWCBSb3RhdGlvbicsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShwcm9wcy5yeCwgcGFyZW50KVxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0bmFtZTogJ1kgUm90YXRpb24nLFxuXHRcdFx0XHR2YWx1ZTogUHJvcGVydHkocHJvcHMucnksIHBhcmVudClcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdG5hbWU6ICdaIFJvdGF0aW9uJyxcblx0XHRcdFx0dmFsdWU6IFByb3BlcnR5KHByb3BzLnJ6LCBwYXJlbnQpXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRuYW1lOiAnT3BhY2l0eScsXG5cdFx0XHRcdHZhbHVlOiBQcm9wZXJ0eShwcm9wcy5vLCBwYXJlbnQpXG5cdFx0XHR9XG5cdFx0XVxuXHR9XG5cblx0ZnVuY3Rpb24gZ2V0VGFyZ2V0VHJhbnNmb3JtKCkge1xuXHRcdHJldHVybiBwcm9wcztcblx0fVxuXG5cdHZhciBtZXRob2RzID0ge1xuXHRcdGdldFRhcmdldFRyYW5zZm9ybTogZ2V0VGFyZ2V0VHJhbnNmb3JtXG5cdH1cblxuXHRyZXR1cm4gT2JqZWN0LmFzc2lnbihtZXRob2RzLCBLZXlQYXRoTm9kZShzdGF0ZSkpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IFRyYW5zZm9ybTsiLCJ2YXIgS2V5UGF0aE5vZGUgPSByZXF1aXJlKCcuLi9rZXlfcGF0aC9LZXlQYXRoTm9kZScpO1xudmFyIFZhbHVlUHJvcGVydHkgPSByZXF1aXJlKCcuL1ZhbHVlUHJvcGVydHknKTtcblxuZnVuY3Rpb24gUHJvcGVydHkocHJvcGVydHksIHBhcmVudCkge1xuXHR2YXIgc3RhdGUgPSB7XG5cdFx0cHJvcGVydHk6IHByb3BlcnR5LFxuXHRcdHBhcmVudDogcGFyZW50XG5cdH1cblxuXHRmdW5jdGlvbiBkZXN0cm95KCkge1xuXHRcdHN0YXRlLnByb3BlcnR5ID0gbnVsbDtcblx0XHRzdGF0ZS5wYXJlbnQgPSBudWxsO1xuXHR9XG5cblx0dmFyIG1ldGhvZHMgPSB7XG5cdFx0ZGVzdHJveTogZGVzdHJveVxuXHR9XG5cblx0cmV0dXJuIE9iamVjdC5hc3NpZ24oe30sIG1ldGhvZHMsIFZhbHVlUHJvcGVydHkoc3RhdGUpLCBLZXlQYXRoTm9kZShzdGF0ZSkpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IFByb3BlcnR5OyIsImZ1bmN0aW9uIFZhbHVlUHJvcGVydHkoc3RhdGUpIHtcblx0XG5cdGZ1bmN0aW9uIHNldFZhbHVlKHZhbHVlKSB7XG5cdFx0dmFyIHByb3BlcnR5ID0gc3RhdGUucHJvcGVydHk7XG5cdFx0aWYoIXByb3BlcnR5IHx8ICFwcm9wZXJ0eS5hZGRFZmZlY3QpIHtcblx0XHRcdHJldHVybjtcblx0XHR9XG5cdFx0aWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ2Z1bmN0aW9uJykge1xuXHRcdFx0cmV0dXJuIHByb3BlcnR5LmFkZEVmZmVjdCh2YWx1ZSk7XG5cdFx0fSBlbHNlIGlmIChwcm9wZXJ0eS5wcm9wVHlwZSA9PT0gJ211bHRpZGltZW5zaW9uYWwnICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUubGVuZ3RoID09PSAyKSB7XG5cdFx0XHRyZXR1cm4gcHJvcGVydHkuYWRkRWZmZWN0KGZ1bmN0aW9uKCl7cmV0dXJuIHZhbHVlfSk7XG5cdFx0fSBlbHNlIGlmIChwcm9wZXJ0eS5wcm9wVHlwZSA9PT0gJ3VuaWRpbWVuc2lvbmFsJyAmJiB0eXBlb2YgdmFsdWUgPT09ICdudW1iZXInKSB7XG5cdFx0XHRyZXR1cm4gcHJvcGVydHkuYWRkRWZmZWN0KGZ1bmN0aW9uKCl7cmV0dXJuIHZhbHVlfSk7XG5cdFx0fVxuXHR9XG5cblx0ZnVuY3Rpb24gZ2V0VmFsdWUoKSB7XG5cdFx0cmV0dXJuIHN0YXRlLnByb3BlcnR5LnY7XG5cdH1cblxuXHR2YXIgbWV0aG9kcyA9IHtcblx0XHRzZXRWYWx1ZTogc2V0VmFsdWUsXG5cdFx0Z2V0VmFsdWU6IGdldFZhbHVlXG5cdH1cblxuXHRyZXR1cm4gbWV0aG9kcztcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBWYWx1ZVByb3BlcnR5OyIsInZhciBMYXllckxpc3QgPSByZXF1aXJlKCcuLi9sYXllci9MYXllckxpc3QnKTtcbnZhciBLZXlQYXRoTGlzdCA9IHJlcXVpcmUoJy4uL2tleV9wYXRoL0tleVBhdGhMaXN0Jyk7XG5cbmZ1bmN0aW9uIFJlbmRlcmVyKHN0YXRlKSB7XG5cblx0c3RhdGUuX3R5cGUgPSAncmVuZGVyZXInO1xuXG5cdGZ1bmN0aW9uIGdldFJlbmRlcmVyVHlwZSgpIHtcblx0XHRyZXR1cm4gc3RhdGUuYW5pbWF0aW9uLmFuaW1UeXBlO1xuXHR9XG5cblx0cmV0dXJuIE9iamVjdC5hc3NpZ24oe1xuXHRcdGdldFJlbmRlcmVyVHlwZTogZ2V0UmVuZGVyZXJUeXBlXG5cdH0sIExheWVyTGlzdChzdGF0ZS5lbGVtZW50cyksIEtleVBhdGhMaXN0KHN0YXRlLmVsZW1lbnRzLCAncmVuZGVyZXInKSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gUmVuZGVyZXI7Il19
